import os
from utils.robot_env import RobotEnv
from utils.yaml_parser import load_yaml


class ANYmalEnv(RobotEnv):
    def __init__(self, robot_config):
        super().__init__(robot_config)


        #########################################################################################
        from utils.state_estimator import StateEstimator
        self.state_estimator = StateEstimator(yaml_path=robot_config)

        #########################################################################################
        from utils.controller.state_machine import StateMachine, LegCoordinator
        configs = load_yaml(robot_config)
        self.step_state_machine = StateMachine(configs["step_duration"], self.configs["sim_rate"])
        self.mpc_state_machine = StateMachine(configs["mpc_duration"], self.configs["sim_rate"])
        self.leg_coodinator = LegCoordinator(yaml_path=robot_config)


        #########################################################################################
        from utils.planner.balance_planner import BalancePlanner
        self.balance_planner = BalancePlanner(yaml_path=robot_config)

        #########################################################################################
        from utils.planner.walk_planner import WalkPlanner
        self.walk_planner = WalkPlanner(yaml_path=robot_config)

        #########################################################################################
        from utils.controller.whole_body_controller import WholeBodyController
        self.whole_body_controller = WholeBodyController(yaml_path=robot_config)

        #########################################################################################
        from utils.robot import Robot
        self.robot = Robot(robot_config)

    def step(self):

        self.state_estimator.estimate_from_observation(base_position=self.robot.getBaseLinkPosition(),
                                                       base_quaternion=self.robot.getBaseLinkQuaternion(),
                                                       actuated_joint_position=self.robot.getActuatedJointPositions(),
                                                       base_twist_linear=self.robot.getBaseVelocityLinear(),
                                                       base_twist_angular=self.robot.getBaseVelocityAngular(),
                                                       actuated_joint_velocity=self.robot.getActuatedJointVelocities())

        self.step_state_machine(self.sim_count)
        self.mpc_state_machine(self.sim_count)
        self.leg_coodinator(self.step_state_machine)

        print(self.state_estimator.com_pos)


        # self.robot.homing_control()
        # self.robot.debug_control()
        # self.robot.inverse_kinematic_control()
        # self.robot.zero_torque_control()
        # self.robot.gravity_compensation_control(self.state_estimator)
        # self.robot.gravity_compensation_PD(self.state_estimator)



        # high_level_command = self.balance_planner(self.state_estimator)
        high_level_command = self.walk_planner(self.mpc_state_machine, self.step_state_machine, self.leg_coodinator, self.state_estimator)

        tau_n = self.whole_body_controller.compute_joint_torques(self.state_estimator, high_level_command)

        self.robot.setActuatedJointTorques(tau_n)



        super().step()

    def debug(self):
        super().debug()
        self.robot.debug(self.keys)
        # self.state_estimator.debug()


if __name__ == '__main__':


    robot_config = os.getcwd() + "/anymal_config.yaml"

    anymal_env = ANYmalEnv(robot_config)

    anymal_env.reset()

    while True:
        anymal_env.render()

        observation = anymal_env.step()

        anymal_env.debug()
