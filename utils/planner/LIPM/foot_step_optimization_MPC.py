#!/usr/bin/env python
import numpy as np
import matplotlib.pyplot as plt
from numpy import sqrt, sinh, cosh
from scipy.linalg import block_diag
from scipy.interpolate import UnivariateSpline, interp1d
from scipy.optimize import minimize
from utils.yaml_parser import load_yaml

class LIPM:
    def __init__(self, yaml_path):
        self.configs = load_yaml(yaml_path)


        self.T = self.configs["step_duration"]
        self.z0 = self.configs["com_height"]
        self.g = self.configs["gravity"]
        self.n = self.configs["optimize_steps"]


        self.Tc = sqrt(self.z0 / self.g)
        self.C = cosh(self.T / self.Tc)
        self.S = sinh(self.T / self.Tc)

        self.AT, self.BT = self.A(self.T), self.B(self.T)

        self.An, self.Bn = self.MPC(self.n)


    def A(self, t):
        A = np.array([[cosh(t / self.Tc), self.Tc * sinh(t / self.Tc)], [sinh(t / self.Tc) / self.Tc, cosh(t / self.Tc)]])
        return A

    def B(self, t):
        B = np.array([1 - cosh(t / self.Tc), -sinh(t / self.Tc) / self.Tc])
        return B

    def MPC(self, n):

        A, B = self.A(self.T), self.B(self.T).reshape(2,1)

        A_n = []
        for i in range(self.n):
            A_n.append(np.linalg.matrix_power(A, i+1))
        A_n = np.vstack(A_n)

        B_n = []
        for i in range(self.n):
            row = []
            for j in range(self.n):
                if i >= j:
                    row.append(np.linalg.matrix_power(A, i-j).dot(B))
                else:
                    row.append(np.zeros_like(B))
            row = np.hstack(row)
            B_n.append(row)
        B_n = np.vstack(B_n)

        return A_n, B_n

    def simulate_one_step_1D(self, x, xd, px, t):
        x_t = (x - px) * cosh(t / self.Tc) + self.Tc * xd * sinh(t / self.Tc) + px
        xd_t = (x - px) / self.Tc * sinh(t / self.Tc) + xd * cosh(t / self.Tc)
        xdd_t = (x - px) / (self.Tc * self.Tc) * cosh(t / self.Tc) + xd / self.Tc * sinh(t / self.Tc)
        return x_t, xd_t, xdd_t



class FootstepPlanner:
    def __init__(self, yaml_path):
        self.configs = load_yaml(yaml_path)
        self.model = LIPM(yaml_path)
        self.xd_ref, self.yd_ref = self.configs["des_walking_velocity"][0], self.configs["des_walking_velocity"][1]
        self.T = self.configs["step_duration"]
        self.n = self.configs["optimize_steps"]
        self.z0 = self.configs["com_height"]

    def get_com_state(self, t_from_start):
        x_t, xd_t, xdd_t = self.model.simulate_one_step_1D(self.X0[0], self.X0[1], self.px0, t_from_start)
        y_t, yd_t, ydd_t = self.model.simulate_one_step_1D(self.Y0[0], self.Y0[1], self.py0, t_from_start)
        com_pos_t = (x_t, y_t, self.z0)
        com_vel_t = (xd_t, yd_t, 0.0)
        com_acc_t = (xdd_t, ydd_t, 0.0)
        return com_pos_t, com_vel_t, com_acc_t

    def get_com_states(self, tt):
        x_t, xd_t, xdd_t = self.model.simulate_one_step_1D(self.X0[0], self.X0[1], self.px0, tt)
        y_t, yd_t, ydd_t = self.model.simulate_one_step_1D(self.Y0[0], self.Y0[1], self.py0, tt)
        com_pos_t = np.vstack((x_t, y_t, self.z0*np.ones(x_t.shape)))
        com_vel_t = np.vstack((xd_t, yd_t, 0.0*np.ones(xd_t.shape)))
        com_acc_t = np.vstack((xdd_t, ydd_t, 0.0*np.ones(xd_t.shape)))
        return com_pos_t, com_vel_t, com_acc_t


    def obj_fun_x(self, opt_px, *args_x):
        X0 = self.model.A(self.t_TD).dot(self.X0) + self.model.B(self.t_TD)*self.px0
        Xs = self.model.An.dot(X0) + self.model.Bn.dot(opt_px)
        Xs = Xs.reshape(self.n,2)

        # com velocity track objective
        ref_xd_obj = np.sum((Xs[:,1] - self.xd_ref)**2)
        ref_xd_obj = (Xs[-1, 1] - self.xd_ref) ** 2

        # minimize foot step objective
        min_px_diff_obj = np.sum(np.diff(opt_px)**2)

        return 1*ref_xd_obj + 1*min_px_diff_obj

    def obj_fun_y(self, opt_py, *args_y):
        Y0 = self.model.A(self.t_TD).dot(self.Y0) + self.model.B(self.t_TD)*(self.py0)
        Ys = self.model.An.dot(Y0) + self.model.Bn.dot(opt_py)
        Ys = Ys.reshape(self.n, 2)

        # com velocity track objective
        ref_yd_obj = np.sum((Ys[:,1] - self.yd_ref)**2)
        ref_yd_obj = (Ys[-1,1] - self.yd_ref)**2

        # minimize foot step objective
        min_py_diff_obj = np.sum(np.diff(opt_py) ** 2)

        return 1*ref_yd_obj + 1*min_py_diff_obj

    def constraint_first_step_in_x(self, px):

        # wrt ini foot position
        # return [0.3 - np.abs(self.px0 - px[0])]
        return [-(self.px0 - 0.3 - px[0]), self.px0 + 0.3 - px[0]]

        # wrt ini com
        # return [0.3 - np.abs(self.x0 - px[0])]

    def constraint_first_step_in_y(self, py):

        # wrt ini foot position
        # return [(self.py0 - py[0]) * self.cur_sup_foot - 0.15, 0.6 - (self.py0 - py[0]) * self.cur_sup_foot]

        # wrt ini com, this is better than ini foot position in lateral direction
        return [(self.y0 - py[0]) * self.cur_sup_foot - 0.15/2, 0.6 - (self.y0 - py[0]) * self.cur_sup_foot,
                (self.py0 - py[0]) * self.cur_sup_foot - 0.15, 1.0 - (self.py0 - py[0]) * self.cur_sup_foot]


    def optimize(self, direction):


        if direction=="x":
            p0_x = np.zeros(self.n)
            cons_x = [{'type': 'ineq', 'fun': self.constraint_first_step_in_x}]
            # results = minimize(self.obj_fun_x, p0_x, method='SLSQP', bounds=[], constraints=cons_x, args=[])
            results = minimize(self.obj_fun_x, p0_x, method='SLSQP', bounds=[], args=[])
            return results.x

        if direction=="y":
            p0_y = np.zeros(self.n)
            cons_y = [{'type': 'ineq', 'fun': self.constraint_first_step_in_y}]
            # results = minimize(self.obj_fun_y, p0_y, method='SLSQP', bounds=[], constraints=cons_y, args=[])
            results = minimize(self.obj_fun_y, p0_y, method='SLSQP', bounds=[], args=[])
            return results.x

    def plan(self,
             cur_com_pos,
             cur_com_vel,
             cur_sup_foot,
             cur_sup_foot_pos,
             T_remain):
        self.X0, self.Y0 = [cur_com_pos[0], cur_com_vel[0]], [cur_com_pos[1], cur_com_vel[1]]
        self.x0, self.y0 = cur_com_pos[0], cur_com_pos[1]
        self.px0, self.py0 = cur_sup_foot_pos[0], cur_sup_foot_pos[1]
        self.t_TD = T_remain
        self.cur_sup_foot = cur_sup_foot


        opt_step_positions_x = self.optimize(direction='x')
        opt_step_positions_y = self.optimize(direction='y')
        opt_step_positions = np.vstack((opt_step_positions_x,
                                        opt_step_positions_y,
                                        np.zeros(self.n))).T

        return opt_step_positions



class FootstepOptimizer:
    def __init__(self, ref_com_vel,
                 cur_com_pos,
                 cur_com_vel,
                 cur_sup_foot,
                 cur_sup_foot_pos,
                 remain_swing_duration,
                 norminal_swing_duration,
                 norminal_com_height,
                 optimize_steps=2,
                 inter_feet_distance=0.2,
                 g=9.81):
        self.v_ref = np.array(ref_com_vel[:2])
        self.xd_ref, self.yd_ref = ref_com_vel[0], ref_com_vel[1]
        self.x0, self.y0 = cur_com_pos[0], cur_com_pos[1]
        self.xd0, self.yd0 = cur_com_vel[0], cur_com_vel[1]
        self.cur_sup_foot = cur_sup_foot
        self.px0, self.py0 = cur_sup_foot_pos[0], cur_sup_foot_pos[1]
        self.T_remain = remain_swing_duration
        self.T = norminal_swing_duration
        self.z0 = norminal_com_height
        self.N = optimize_steps  # predict steps
        self.d = inter_feet_distance  # inter-feet clearance distance
        self.g = g

        self.Tc = sqrt(self.z0 / self.g)
        self.C = cosh(self.T / self.Tc)
        self.S = sinh(self.T / self.Tc)

        self.q0 = np.array([self.x0, self.xd0, self.y0, self.yd0])
        self.p0 = np.array([self.px0, self.py0])
        self.A0 = self.A_2D(self.T_remain)
        self.B0 = self.B_2D(self.T_remain)
        self.A = self.A_2D(self.T)
        self.B = self.B_2D(self.T)

        self.X0 = np.array([self.x0, self.xd0])
        self.Ax0 = self.A_1D(self.T_remain)
        self.Bx0 = self.B_1D(self.T_remain)
        self.Ax = self.A_1D(self.T)
        self.Bx = self.B_1D(self.T)

        self.Y0 = np.array([self.y0, self.yd0])
        self.Ay0 = self.A_1D(self.T_remain)
        self.By0 = self.B_1D(self.T_remain)
        self.Ay = self.A_1D(self.T)
        self.By = self.B_1D(self.T)

        self.p_des, self.v_des = self.cal_des_future_footsteps()
        self.px_des = self.p_des[:,0]
        self.py_des = self.p_des[:,1]

    def cal_des_future_footsteps(self):
        p_des = []
        v_des = []
        for i in range(1, self.N + 1):
            p_i = self.p0 + 2.0 * i * self.T * self.v_ref - np.mod(i, 2) * np.array([0, self.d * self.cur_sup_foot])
            p_des.append(p_i)
            v_i = [self.xd_ref, - self.d * self.cur_sup_foot]
            v_des.append(v_i)
        return np.array(p_des), np.array(v_des)

    def cal_px_des(self):
        px_des = []
        for i in range(1, self.N + 1):
            px_i = self.px0 + i * self.T * self.xd_ref
            px_des.append(px_i)
        return np.array(px_des)

    def cal_py_des(self):
        py_des = []
        for i in range(1, self.N + 1):
            py_i = self.py0 + i * self.T * self.yd_ref - np.mod(i,2) * self.d * self.cur_sup_foot
            py_des.append(py_i)
        return np.array(py_des)

    def cal_py_des_cross_walker(self):
        py_des = []
        for i in range(1, self.N + 1):
            py_i = self.py0 + i * self.T * self.yd_ref + np.mod(i,2) * self.d * self.cur_sup_foot
            py_des.append(py_i)
        return np.array(py_des)

    def cal_xd_des(self):
        xd_des = []
        for i in range(1, self.N + 1):
            xd_i = (self.C+1)/(self.Tc*self.S)*(self.T * self.xd_ref/2.0)
            xd_des.append(xd_i)
        return np.array(xd_des)

    def cal_yd_des(self):
        yd_des = []
        for i in range(1, self.N+1):
            yd_i = (self.C-1)/(self.Tc*self.S)*((self.T * self.yd_ref + (-1)**i * self.d * self.cur_sup_foot)/2.0)
            yd_des.append(yd_i)
        return np.array(yd_des)

    def simulate_one_step_1D(self, x, xd, px, t):
        x_t = (x - px) * cosh(t / self.Tc) + self.Tc * xd * sinh(t / self.Tc) + px
        xd_t = (x - px) / self.Tc * sinh(t / self.Tc) + xd * cosh(t / self.Tc)
        xdd_t = (x - px) / (self.Tc * self.Tc) * cosh(t / self.Tc) + xd / self.Tc * sinh(t / self.Tc)
        return x_t, xd_t, xdd_t

    def simulate_one_step_2D(self, com_pos, com_vel, foot_pos, t):
        x_t, xd_t, xdd_t = self.simulate_one_step_1D(com_pos[0], com_vel[0], foot_pos[0], t)
        y_t, yd_t, ydd_t = self.simulate_one_step_1D(com_pos[1], com_vel[1], foot_pos[1], t)
        com_pos_t = (x_t, y_t)
        com_vel_t = (xd_t, yd_t)
        return com_pos_t, com_vel_t

    def get_com_state(self, t_from_start):
        x_t, xd_t, xdd_t = self.simulate_one_step_1D(self.x0, self.xd0, self.px0, t_from_start)
        y_t, yd_t, ydd_t = self.simulate_one_step_1D(self.y0, self.yd0, self.py0, t_from_start)
        com_pos_t = (x_t, y_t, self.z0)
        com_vel_t = (xd_t, yd_t, 0.0)
        com_acc_t = (xdd_t, ydd_t, 0.0)
        return com_pos_t, com_vel_t, com_acc_t

    def get_com_state_tt(self, tt):
        x_t, xd_t, xdd_t = self.simulate_one_step_1D(self.x0, self.xd0, self.px0, tt)
        y_t, yd_t, ydd_t = self.simulate_one_step_1D(self.y0, self.yd0, self.py0, tt)
        com_pos_t = np.vstack((x_t, y_t, self.z0*np.ones(x_t.shape)))
        com_vel_t = np.vstack((xd_t, yd_t, 0.0*np.ones(xd_t.shape)))
        com_acc_t = np.vstack((xdd_t, ydd_t, 0.0*np.ones(xd_t.shape)))
        return com_pos_t, com_vel_t, com_acc_t

    def A_2D(self, t):
        Tc = self.Tc
        Ax = np.array([[cosh(t / Tc), Tc * sinh(t / Tc)], [sinh(t / Tc) / Tc, cosh(t / Tc)]])
        A = block_diag(Ax, Ax)
        return A

    def A_1D(self, t):
        Tc = self.Tc
        Ax = np.array([[cosh(t / Tc), Tc * sinh(t / Tc)], [sinh(t / Tc) / Tc, cosh(t / Tc)]])
        return Ax

    def B_2D(self, t):
        Tc = self.Tc
        Bx = np.array([[1 - cosh(t / Tc)], [-sinh(t / Tc) / Tc]])
        B = block_diag(Bx, Bx)
        return B

    def B_1D(self, t):
        Tc = self.Tc
        Bx = np.array([1 - cosh(t / Tc), -sinh(t / Tc) / Tc])
        return Bx

    def obj_fun(self, opt_var, *args):

        p = opt_var.reshape(self.N, 2)

        q1 = self.A0.dot(self.q0) + self.B0.dot(self.p0)
        q = [q1]
        for i in range(1, self.N + 1):
            q.append(self.A.dot(q[i - 1]) + self.B.dot(p[i - 1]))
        q = np.array(q)

        ref_vel_obj = np.sum((q[:, 1] - self.xd_ref) ** 2) + np.sum((q[:, 3] - self.yd_ref) ** 2)
        ref_footstep_obj = np.sum((opt_var - self.p_des.reshape(1, 2 * self.N)) ** 2)

        return 1 * ref_vel_obj + 1 * ref_footstep_obj


    def plan(self):
        opt_var0 = self.p_des.flatten()

        # sx_bnds = [-0.4, 0.4]
        # sy_bnds = [-0.4, 0.4]
        # px_bnds = px_n + sx_bnds
        # py_bnds = py_n + sy_bnds
        # Tsup_bnds = [0.399, 0.401]
        # bnds = (px_bnds, py_bnds, Tsup_bnds)

        # ub = opt_var0 + 0.1
        # lb = opt_var0 - 0.1
        # bnds = zip(lb,ub)
        #
        # bnds = [[self.px0-0.2, self.px0+0.2],
        #         [min(self.py0 - self.cur_sup_foot*0.12, self.py0 - self.cur_sup_foot*0.25), max(self.py0 - self.cur_sup_foot*0.12, self.py0 - self.cur_sup_foot*0.25)],
        #         [self.px0-0.4, self.px0+0.4],
        #         [self.py0-0.2, self.py0+0.2]]

        # bnds = [[self.px0 - 0.2, self.px0 + 0.2],
        #         [min(self.py0 - self.cur_sup_foot*0.1, self.py0 - self.cur_sup_foot*0.3), max(self.py0 - self.cur_sup_foot*0.1, self.py0 - self.cur_sup_foot*0.3)]]

        # print("opt_var0: ", opt_var0)
        # print("bnd: ", bnds)



        opt_res = minimize(self.obj_fun, opt_var0, method='SLSQP', bounds=[])


        return opt_res.x.reshape(-1,2)



    def obj_fun_x(self, opt_px, *args_x):
        X1 = self.Ax0.dot(self.X0) + self.Bx0*(self.px0)

        Xs = [X1]
        for i in range(1, self.N + 1):
            Xs.append(self.Ax.dot(Xs[i-1]) + self.Bx*opt_px[i-1])
        Xs = np.array(Xs)

        # com velocity track objective
        ref_xd_obj = np.sum((Xs[:,1] - self.xd_ref)**2)

        # reference foot placement track objective
        ref_px_obj = np.sum((opt_px-self.px_des)**2)

        # minimize foot step objective
        min_px_diff_obj = np.sum(np.diff(opt_px)**2)

        return 1*ref_xd_obj + 0*ref_px_obj + 1*min_px_diff_obj


    def obj_fun_y(self, opt_py, *args_y):

        Y1 = self.Ay0.dot(self.Y0) + self.By0*(self.py0)
        Ys = [Y1]
        for i in range(1, self.N + 1):
            Ys.append(self.Ay.dot(Ys[i-1]) + self.By*opt_py[i-1])
        Ys = np.array(Ys)

        # com velocity track objective
        ref_yd_obj = np.sum((Ys[:,1] - self.yd_ref)**2)

        # reference foot placement track objective
        ref_py_obj = np.sum((opt_py-self.py_des)**2)

        # minimize foot step objective
        min_py_diff_obj = np.sum(np.diff(opt_py) ** 2)

        return 1*ref_yd_obj + 0*ref_py_obj + 1*min_py_diff_obj


    def constraint_first_step_in_x(self, px):

        # wrt ini foot position
        # return [0.3 - np.abs(self.px0 - px[0])]
        return [-(self.px0 - 0.3 - px[0]), self.px0 + 0.3 - px[0]]

        # wrt ini com
        # return [0.3 - np.abs(self.x0 - px[0])]

    def constraint_first_step_in_y(self, py):

        # wrt ini foot position
        # return [(self.py0 - py[0]) * self.cur_sup_foot - 0.15, 0.6 - (self.py0 - py[0]) * self.cur_sup_foot]

        # wrt ini com, this is better than ini foot position in lateral direction
        return [(self.y0 - py[0]) * self.cur_sup_foot - 0.15/2, 0.6 - (self.y0 - py[0]) * self.cur_sup_foot,
                (self.py0 - py[0]) * self.cur_sup_foot - 0.15, 1.0 - (self.py0 - py[0]) * self.cur_sup_foot]


    def plan_x(self):
        # todo: step limits needed.
        opt_px0 = np.zeros_like(self.px_des)
        cons = [{'type': 'ineq', 'fun': self.constraint_first_step_in_x}]
        # opt_res_x = minimize(self.obj_fun_x, opt_px0, method='SLSQP', bounds=[], constraints=cons, args=[])
        opt_res_x = minimize(self.obj_fun_x, opt_px0, method='SLSQP', bounds=[], args=[])
        return opt_res_x


    def plan_y(self):
        # todo: step limits needed.
        opt_py0 = np.zeros_like(self.py_des)

        # bnds = [[min(self.py0 - self.cur_sup_foot*0.12, self.py0 - self.cur_sup_foot*0.25), max(self.py0 - self.cur_sup_foot*0.12, self.py0 - self.cur_sup_foot*0.25)]]
        # bnds = []
        # for i in range(1, self.N + 1):
        #     # if i == 1:
        #     bnds.append([min(self.py_des[i-1] + (-1)**i * self.cur_sup_foot*0.1, self.py_des[i-1] + (-1)**i * self.cur_sup_foot*1.0),
        #                      max(self.py_des[i-1] + (-1)**i * self.cur_sup_foot*0.1, self.py_des[i-1] + (-1)**i * self.cur_sup_foot*1.0)])
        #

        # print("py0: ", self.py0)
        # print("bnds: ", bnds)

        cons = [{'type': 'ineq', 'fun': self.constraint_first_step_in_y}]
        # opt_res_y = minimize(self.obj_fun_y, opt_py0, method='SLSQP', bounds=[], constraints=cons, args=[])
        opt_res_y = minimize(self.obj_fun_y, opt_py0, method='SLSQP', bounds=[], constraints=[], args=[])

        # print("opt_py: ", opt_res_y.x)
        return opt_res_y


    def plan_xy(self):

        # T_list = []
        # value_list = []
        # for T_remain in np.arange(0.3, 0.8, 0.01):
        #     opt_res_y_temp = self.plan_y(T_remain)
        #     print("T_remain: %.2f, fun_temp: %.3f" %(T_remain,  opt_res_y_temp.fun))
        #     T_list.append(T_remain)
        #     value_list.append(opt_res_y_temp.fun)
        # index = value_list.index(min(value_list))
        # T_remain_best = T_list[index]
        # print("index: ", index)
        # print("T_remian_best: ", T_remain_best)

        opt_res_x = self.plan_x()
        opt_res_y = self.plan_y()

        opt_ps = np.vstack((opt_res_x.x, opt_res_y.x)).T
        return opt_ps



    def plan_xyz(self):

        opt_res_x = self.plan_x()
        opt_res_y = self.plan_y()

        opt_ps = np.vstack((opt_res_x.x, opt_res_y.x, np.zeros(self.N))).T
        return opt_ps




# todo: decoupled version not finished.
# class FootstepOptimizerDecoupled:
#     def __init__(self, ref_com_vel, cur_com_pos, cur_com_vel, cur_sup_foot, cur_sup_foot_pos, remain_swing_duration,
#                  norminal_swing_duration, norminal_com_height, N=2, d=0.2, g=9.81):
#
#         self.xd_ref, self.yd_ref = ref_com_vel[0], ref_com_vel[1]
#         self.x0, self.y0 = cur_com_pos[0], cur_com_pos[1]
#         self.xd0, self.yd0 = cur_com_vel[0], cur_com_vel[1]
#         self.cur_sup_foot = cur_sup_foot
#         self.px0, self.py0 = cur_sup_foot_pos[0], cur_sup_foot_pos[1]
#         self.T_remain = remain_swing_duration
#         self.T = norminal_swing_duration
#         self.z0 = norminal_com_height
#         self.N = N
#         self.d = d  # inter-feet clearance distance
#         self.g = g
#
#         self.Tc = sqrt(self.z0 / self.g)
#         self.C = cosh(self.Tsup / self.Tc)
#         self.S = sinh(self.Tsup / self.Tc)
#
#         self.px_des = self.cal_px_des()
#         self.py_des = self.cal_py_des()
#         self.xd_des = self.cal_xd_des()
#         self.yd_des = self.cal_yd_des()
#
#         self.X0 = np.array([self.x0, self.xd0])
#
#
#     def cal_px_des(self):
#         px_des = []
#         for i in range(1, self.N + 1):
#             px_i = self.px0 + i * self.T * self.xd_ref
#             px_des.append(px_i)
#         return np.array(px_des)
#
#     def cal_py_des(self):
#         py_des = []
#         for i in range(1, self.N + 1):
#             py_i = self.py0 + i * self.T * self.yd_ref - np.mod(i,2) * self.d * self.cur_sup_foot
#             py_des.append(py_i)
#         return np.array(py_des)
#
#     def cal_xd_des(self):
#         xd_des = []
#         for i in range(1, self.N + 1):
#             xd_i = (self.C+1)/(self.Tc*self.S)*(self.T * self.xd_ref/2.0)
#             xd_des.append(xd_i)
#         return np.array(xd_des)
#
#     def cal_yd_des(self):
#         yd_des = []
#         for i in range(1, self.N+1):
#             yd_i = (self.C-1)/(self.Tc*self.S)*((self.T * self.yd_ref + (-1)**i * self.d * self.cur_sup_foot)/2.0)
#             yd_des.append(yd_i)
#         return np.array(yd_des)
#
#
#     def simulate_one_step_1D(self, x, xd, px, t):
#         x_t = (x - px) * cosh(t / self.Tc) + self.Tc * xd * sinh(t / self.Tc) + px
#         xd_t = (x - px) / self.Tc * sinh(t / self.Tc) + xd * cosh(t / self.Tc)
#         xdd_t = (x - px) / (self.Tc * self.Tc) * cosh(t / self.Tc) + xd / self.Tc * sinh(t / self.Tc)
#         return np.array([x_t, xd_t])
#
#     def simulate_one_step_1D_new(self, X, px, t):
#         x, xd = X[0], X[1]
#         x_t = (x - px) * cosh(t / self.Tc) + self.Tc * xd * sinh(t / self.Tc) + px
#         xd_t = (x - px) / self.Tc * sinh(t / self.Tc) + xd * cosh(t / self.Tc)
#         xdd_t = (x - px) / (self.Tc * self.Tc) * cosh(t / self.Tc) + xd / self.Tc * sinh(t / self.Tc)
#         return np.array([x_t, xd_t])
#
#
#     def obj_fun_x(self, opt_px, *args_x):
#
#         Xs = []
#         Xs.append(self.X0)
#         X1 = self.simulate_one_step_1D_new(self.X0, self.px0, self.T_remain)
#         Xs.append(X1)
#         for i in range(1, self.N + 1):
#             Xi = self.simulate_one_step_1D_new(Xs[-1], opt_px[i-1], self.T)
#             Xs.append(Xi)
#
#
#     def obj_fun_y(self, opt_var_y, *args_y):
#         pass
#
#     def plan(self):
#         opt_px0 = self.px_des
#         opt_res_x = minimize(self.obj_fun_x, opt_px0, method='SLSQP', bounds=[])
#




if __name__ == "__main__":
    footstep_optmizer = FootstepOptimizer(ref_com_vel=[0.0, 0.0],
                                          cur_com_pos=[0.07914, -0.01963],
                                          cur_com_vel=[0.04718, 0.13622],
                                          cur_sup_foot=2,
                                          cur_sup_foot_pos=[0.08258, 0.03864],
                                          remain_swing_duration=0.26,
                                          norminal_swing_duration=0.4,
                                          norminal_com_height=0.45,
                                          N=1,
                                          d=0.2,
                                          g=9.81)

    footsteps_nominal = footstep_optmizer.plan()
    print("footsteps_nominal: ", footsteps_nominal)
    footsteps_xy=footstep_optmizer.plan_xy()
    print("footsteps_xy: ", footsteps_xy)
