#!/usr/bin/env python
import numpy as np
import matplotlib.pyplot as plt
from scipy.optimize import minimize




class LinearInvertedPendulumModel:
    def __init__(self, X0, z, g=9.81):
        self.z, self.g = z, g
        self.Tc = np.sqrt(self.z / self.g)

        self.x0, self.xd0 = X0

    def __call__(self, t):
        t = np.asarray(t)
        x_t = self.x0 * np.cosh(t / self.Tc) + self.Tc * self.xd0 * np.sinh(t / self.Tc)
        xd_t = self.x0 / self.Tc * np.sinh(t / self.Tc) + self.xd0 * np.cosh(t / self.Tc)
        X_t = [x_t, xd_t]
        return np.asarray(X_t)


def objective_function(opt_x, *args):

    s1 = opt_x

    T, z, g, xs, vs, xe, ve = args

    lipm_ab = LinearInvertedPendulumModel(X0=[xs, vs], z=z)
    X_b = lipm_ab(T)

    lipm_bd = LinearInvertedPendulumModel(X0=[X_b[0]-s1, X_b[1]], z=z)
    X_d = lipm_bd(T)

    final_state_goal = (X_d[0] - xe)**2 + (X_d[1] - ve)**2


    return final_state_goal

def plan_one_step_automatically(T):

    bnds = [[0, 1]]


    opt_x0 = np.random.random(1)


    z, g = 0.8, 9.81
    xs, vs = -0.1, 0.4
    xe, ve = 0.0, 0.2

    res = minimize(objective_function, opt_x0, args=(T, z, g, xs, vs, xe, ve), method='SLSQP', bounds=bnds)



    # plotting
    s1 = res.x

    plot_one_step(s1, T, xs, vs, z)


    print("T: ", T, "s1: ", s1, "res.fun", res.fun, "res.nit: ", res.nit, "v=s/T: ", s1/T)
    # print(res)
    return res.x


def plot_one_step(s1, T, xs, vs, z):

    lipm_ab = LinearInvertedPendulumModel(X0=[xs, vs], z=z)
    tt_ab = np.linspace(0, T, num=100)
    XX_ab = lipm_ab(tt_ab)

    lipm_bd = LinearInvertedPendulumModel(X0=[XX_ab[0, -1] - s1, XX_ab[1, -1]], z=z)
    tt_bd = np.linspace(0, T, num=100)
    XX_bd = lipm_bd(tt_bd)


    t_b = T
    tt = np.concatenate((tt_ab, t_b + tt_bd))
    XX = np.hstack((XX_ab, XX_bd))


    plt.subplot(2, 1, 1)
    plt.plot(tt, XX[0], label='T:'+str(T))
    plt.grid(True)
    plt.legend(loc='best', prop={'size': 10})
    plt.subplot(2, 1, 2)
    plt.plot(tt, XX[1], label='T:'+str(T))
    plt.grid(True)
    plt.legend(loc='best', prop={'size': 10})






if __name__ == "__main__":
    print("[main]")

    plt.figure()


    plan_one_step_automatically(T=0.2)
    plan_one_step_automatically(T=0.3)
    plan_one_step_automatically(T=0.4)
    plan_one_step_automatically(T=0.5)
    plan_one_step_automatically(T=0.6)
    plan_one_step_automatically(T=0.7)
    plan_one_step_automatically(T=0.8)
    plan_one_step_automatically(T=0.9)
    plan_one_step_automatically(T=1.0)
    plan_one_step_automatically(T=1.1)
    plan_one_step_automatically(T=1.2)
    plt.show()