#!/usr/bin/env python
import numpy as np
import matplotlib.pyplot as plt
from scipy.optimize import minimize


class LinearInvertedPendulumModel:
    def __init__(self, X0, z, g=9.81):
        self.z, self.g = z, g
        self.Tc = np.sqrt(self.z / self.g)

        # initial state
        self.x0, self.xd0 = X0

        # current state
        self.x, self.xd = self.x0, self.xd0

    def __call__(self, t):
        x = self.x0 * np.cosh(t / self.Tc) + self.Tc * self.xd0 * np.sinh(t / self.Tc)
        xd = self.x0 / self.Tc * np.sinh(t / self.Tc) + self.xd0 * np.cosh(t / self.Tc)
        X = np.array([x, xd]).reshape((2, -1))
        self.x, self.xd = X[:, -1]  # update current state
        return X

    def simulate(self, t):
        x = self.x * np.cosh(t / self.Tc) + self.Tc * self.xd * np.sinh(t / self.Tc)
        xd = self.x / self.Tc * np.sinh(t / self.Tc) + self.xd * np.cosh(t / self.Tc)
        X = np.array([x, xd]).reshape((2, -1))
        self.x, self.xd = X[:, -1]  # update current state
        return X

    def set_state(self, X):
        self.x, self.xd = X


class Planner:
    def __init__(self, X0, z):
        self.X0 = X0
        self.z = z

    def objective_function(self, opt_x, *args):
        s1, s2 = opt_x

        T, remaining_time, Xe = args

        lipm = LinearInvertedPendulumModel(X0=self.X0, z=self.z)

        X_remaining = lipm.simulate(remaining_time)
        lipm.set_state(X=X_remaining[:, -1] - [s1, 0])  # switch leg
        X_2T = lipm.simulate(T)
        lipm.set_state(X=X_2T[:, -1] - [s2, 0])  # switch leg
        X_3T = lipm.simulate(T)

        kx, kv = 1, 1
        # kx, kv = 1, 1
        final_state_goal = kx*(X_3T[0, -1] - xe) ** 2 + kv*(X_3T[1, -1] - ve) ** 2

        return final_state_goal


    def plan_step_time_and_length(self, Xe, remaining_time):
        for T in np.linspace(0.1, 1, num=10):
            result = self.plan_step_length(Xe, T, remaining_time)
            print("T,s1,s2: ", result)


    def plan_step_length(self, Xe, T, remaining_time):

        N = 2  # number of steps
        bnds = [[-self.z/np.sqrt(2), self.z/np.sqrt(2)]]*N
        # bnds = [[-self.z, self.z]] * N
        opt_x0 = np.random.random(N)
        opt_results = minimize(self.objective_function, opt_x0, args=(T, remaining_time, Xe), method='SLSQP', bounds=bnds)
        s1, s2 = opt_results.x

        if opt_results.fun < 0.001:
            plt.figure()
            self.plot_steps(s1, s2, T, remaining_time, self.X0, self.z)
            plt.show()

        return (opt_results.success, opt_results.fun, T, s1, s2)

    def plot_steps(self, s1, s2, T, remaining_time, X0, z):

        lipm = LinearInvertedPendulumModel(X0=X0, z=z)
        tt_remaining = np.linspace(0, remaining_time, num=100)
        XX_remaining = lipm.simulate(tt_remaining)

        lipm.set_state(X=XX_remaining[:, -1] - [s1, 0])  # switch leg
        tt_T = np.linspace(0, T, num=100)
        XX_2T = lipm.simulate(tt_T)

        lipm.set_state(X=XX_2T[:, -1] - [s2, 0])  # switch leg
        XX_3T = lipm.simulate(tt_T)

        tt = np.hstack((tt_remaining, remaining_time+tt_T, remaining_time+T+tt_T))
        XX = np.hstack((XX_remaining, XX_2T, XX_3T))


        plt.subplot(2, 1, 1)
        plt.plot(tt, XX[0], label='T:' + str(T))
        plt.grid(True)
        plt.legend(loc='best', prop={'size': 10})
        plt.subplot(2, 1, 2)
        plt.plot(tt, XX[1], label='T:' + str(T))
        plt.grid(True)
        plt.legend(loc='best', prop={'size': 10})

#
# def objective_function(opt_x, *args):
#     s1, s2 = opt_x
#
#     T, z, g, xs, vs, xe, ve = args
#
#     lipm_ab = LinearInvertedPendulumModel(X0=[xs, vs], z=z)
#     X_b = lipm_ab(T)
#
#     lipm_bd = LinearInvertedPendulumModel(X0=[X_b[0] - s1, X_b[1]], z=z)  # '-' means forward step
#     X_d = lipm_bd(T)
#
#     lipm_de = LinearInvertedPendulumModel(X0=[X_d[0] - s2, X_d[1]], z=z)  # '-' means forward step
#     X_e = lipm_de(T)
#
#     final_state_goal = (X_e[0] - xe) ** 2 + (X_e[1] - ve) ** 2
#
#     return final_state_goal
#
#
# def step_planning_automatically(T):
#     z, g = 0.8, 9.81
#     xs, vs = 0.1, 0.3
#     xe, ve = -0.1, 0.0
#
#     bnds = [[-1, 1], [-1, 1]]
#     opt_x0 = np.random.random(2)
#     res = minimize(objective_function, opt_x0, args=(T, z, g, xs, vs, xe, ve), method='SLSQP', bounds=bnds)
#
#     # plotting
#     s1, s2 = res.x
#
#     plot_one_step(s1, s2, T, xs, vs, z)
#
#     print(
#         "T: ", T, "s1, s2: ", s1, s2, "res.fun", res.fun, "res.nit: ", res.nit, "(s1+s2)/(2*T): ", (s1 + s2) / (2 * T))
#     print(res)
#     return res.x
#
#
# def plot_one_step(s1, s2, T, xs, vs, z):
#     lipm_ab = LinearInvertedPendulumModel(X0=[xs, vs], z=z)
#     tt_ab = np.linspace(0, T, num=100)
#     XX_ab = lipm_ab(tt_ab)
#
#     lipm_bd = LinearInvertedPendulumModel(X0=[XX_ab[0, -1] - s1, XX_ab[1, -1]], z=z)
#     tt_bd = np.linspace(0, T, num=100)
#     XX_bd = lipm_bd(tt_bd)
#
#     lipm_de = LinearInvertedPendulumModel(X0=[XX_bd[0, -1] - s2, XX_bd[1, -1]], z=z)
#     tt_de = np.linspace(0, T, num=100)
#     XX_de = lipm_de(tt_de)
#
#     t_b, t_d = T, 2 * T
#     tt = np.concatenate((tt_ab, t_b + tt_bd, t_d + tt_de))
#     XX = np.hstack((XX_ab, XX_bd, XX_de))
#
#     plt.subplot(2, 1, 1)
#     plt.plot(tt, XX[0], label='T:' + str(T))
#     plt.grid(True)
#     plt.legend(loc='best', prop={'size': 10})
#     plt.subplot(2, 1, 2)
#     plt.plot(tt, XX[1], label='T:' + str(T))
#     plt.grid(True)
#     plt.legend(loc='best', prop={'size': 10})
#
#
# def plan_time_step():
#     plt.figure()
#     # step_planning_automatically(T=0.1)
#     # step_planning_automatically(T=0.2)
#     # step_planning_automatically(T=0.3)
#     step_planning_automatically(T=0.4)
#     # step_planning_automatically(T=0.5)
#     # step_planning_automatically(T=0.6)
#     # step_planning_automatically(T=0.7)
#     # step_planning_automatically(T=0.8)
#     plt.show()


if __name__ == "__main__":
    print("[main]")

    z = 0.8
    xs, vs = -0.1, 0.5
    xe, ve = -0.0, 0.3

    planner = Planner(X0=[xs, vs], z=z)

    planner.plan_step_time_and_length(Xe=[xe, ve], remaining_time=0.45)

