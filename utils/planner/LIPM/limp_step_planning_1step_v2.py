#!/usr/bin/env python
import numpy as np
import matplotlib.pyplot as plt
from scipy.optimize import minimize

from numpy import sqrt,sinh,cosh




class LinearInvertedPendulumModel:
    def __init__(self, X0, z, g=9.81):
        self.z, self.g = z, g
        self.Tc = np.sqrt(self.z / self.g)

        self.x0, self.xd0 = X0

    def __call__(self, t):
        t = np.asarray(t)
        x_t = self.x0 * np.cosh(t / self.Tc) + self.Tc * self.xd0 * np.sinh(t / self.Tc)
        xd_t = self.x0 / self.Tc * np.sinh(t / self.Tc) + self.xd0 * np.cosh(t / self.Tc)
        X_t = [x_t, xd_t]
        return np.asarray(X_t)


def f(tau, Tc):
    return np.array([[cosh(tau/Tc), Tc*sinh(tau/Tc)],
                     [1.0/Tc*sinh(tau/Tc), cosh(tau/Tc)]])


def obj_fun(opt_x, *args):
    tau_ab, tau_bc, s = opt_x
    z, g, X0, X1, tau0, s0, T0, dis_X = args
    Tc = sqrt(z / g)

    X_b = f(tau_ab, Tc).dot(X0)


    X1_opt = f(tau_ab, Tc).dot(f(tau_bc, Tc)).dot(X0) - f(tau_bc, Tc).dot(np.array([s, 0]))
    dX = X1 - X1_opt
    # print("X1_opt: ", X1_opt)

    final_state_goal = np.linalg.norm(dX)
    tau_goal = (tau_ab + tau_bc + tau0 - T0) ** 2

    final_state_and_mid_pos = final_state_goal + 100*(X_b[0]-s0/2.0)**2 # huan model

    swing_foot_vel = ((s-X0[0])/tau_ab)**2

    return 1.0*final_state_goal + 1.0*tau_goal + 0*(X_b[0]-s0/2.0)**2 + 0.0*(s-s0)**2 + 0.0*swing_foot_vel





if __name__ == "__main__":
    print("[main]")

    bnds = np.array([[0.1, 0.5], [0.25, 0.8], [-0.8, 0.8]])

    opt_x0 = np.random.random(3)

    opt_x0 = np.random.random(3)*(bnds[:,1]-bnds[:,0]) + bnds[:,0]

    opt_x0 = [0.25, 0.5, 0.0]


    z, g = 0.8, 9.81

    X000 = [0.0, 0.1]
    lipm = LinearInvertedPendulumModel(X0=X000, z=z)
    tt = np.linspace(0, 0.4, num=100)
    XX = lipm(tt)

    index = 0
    dis_X = [0.0, 0.0]
    X0 = XX[:,index] + dis_X
    tau0 = tt[index]

    # X0 = [0.00818908,  0.10403046+0.1]
    X1 = X000
    s0 = 0.2
    T0 = 0.8


    # opt_x0 = [T0/2-tau0, T0/2, s0]


    print("opt_x0: ", opt_x0, "X0: ", X0, "tau0: ", tau0)

    res = minimize(obj_fun, opt_x0, args=(z, g, X0, X1, tau0, s0, T0, dis_X), method='SLSQP', bounds=bnds, options={'maxiter': 200,'ftol': 1e-10})
    print(res)




