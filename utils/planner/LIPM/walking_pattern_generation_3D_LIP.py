#!/usr/bin/env python
import numpy as np
import matplotlib.pyplot as plt
from numpy import sqrt, sinh, cosh
from scipy.interpolate import UnivariateSpline, interp1d
from scipy.optimize import minimize

class WalkingPatternGenerator:
    def __init__(self, walk_parameters, ini_com_pos, ini_com_vel, ini_foot_placement, ini_support_leg=-1, Tsup=0.8, zc=0.8, g=9.81, Tsup_remain=0.8):
        """
        Chapter 4.3.3 of the Book: Kajita, Shuuji, et al. Introduction to humanoid robotics. Vol. 101. Springer Berlin Heidelberg, 2014.
        :param walk_parameters: (2,n) array, first_row: s_x, second_row: s_y, Chapter 4.3.3 -> 2 Walk Parameters
        :param ini_com_pos: (x,y)
        :param ini_com_vel: (xd,yd)
        :param ini_foot_placement: (px, py)
        :param Tsup: 0.8 by default
        :param zc: 0.8 by default
        :param g: 9.81 by default
        """
        self.walk_parameters = walk_parameters
        self.sx, self.sy = self.walk_parameters[0], self.walk_parameters[1]
        self.N = self.walk_parameters.shape[1] - 1

        self.ini_support_leg = ini_support_leg

        self.x0, self.y0 = ini_com_pos
        self.xd0, self.yd0 = ini_com_vel
        self.px0, self.py0 = ini_foot_placement
        self.Tsup = Tsup
        self.zc = zc
        self.g = g
        self.Tsup_remain = Tsup_remain

        self.Tc = sqrt(zc / g)
        self.C = cosh(Tsup / self.Tc)
        self.S = sinh(Tsup / self.Tc)

    def orbital_energy(self, x, xd):
        E = -self.g / (2.0 * self.zc) * x * x + xd * xd / 2.0
        return E

    def one_step(self, x, xd, px, t):
        # print("     pos rel in: %.5f, vel in: %.5f, Ei: %.5f" %((x-px), xd, self.orbital_energy(x=(x-px),xd=xd)))
        x_t = (x - px) * cosh(t / self.Tc) + self.Tc * xd * sinh(t / self.Tc) + px
        xd_t = (x - px) / self.Tc * sinh(t / self.Tc) + xd * cosh(t / self.Tc)
        # X = np.array([x, xd]).reshape((2, -1))
        # print("     pos rel out: %.5f, vel out: %.5f, Ef: %.5f" %(x_t-px,xd_t,self.orbital_energy(x=x_t-px,xd=xd_t)))
        return x_t, xd_t

    def one_step_with_acc(self, x, xd, px, t):
        x_t = (x - px) * cosh(t / self.Tc) + self.Tc * xd * sinh(t / self.Tc) + px
        xd_t = (x - px) / self.Tc * sinh(t / self.Tc) + xd * cosh(t / self.Tc)
        xdd_t = (x - px)/ (self.Tc*self.Tc) * cosh(t/self.Tc) + xd/self.Tc * sinh(t/self.Tc)
        return x_t, xd_t, xdd_t

    def plot_plan(self):
        plt.figure(figsize=(20, 10))
        plt.grid(which='both')
        plt.axis('equal')

        px_n, py_n = self.px0, self.py0

        for step in self.all_steps:
            Xs, Ys = step["Xs"], step["Ys"]
            px_star, py_star = step["foot_placement"]
            n = step["n"]

            px_n = px_n + self.sx[n - 1]
            py_n = py_n - (-1) ** n * self.sy[n - 1]


            plt.plot(Xs[0], Ys[0], 'k--')
            plt.plot(Xs[0][-1], Ys[0][-1], c='g', marker='*', markersize=10)
            plt.plot(px_n, py_n, c='k', marker='x', markersize=10, label='desired foot placement ' + str(n))


            plt.plot(px_star, py_star, c='r', marker='x', markersize=10)



    def plan(self, verbose=False, plot=False):
        """
        Chapter 4.3.3 -> Fig. 4.25 Algorithm of walking pattern generation based on 3D-LIP
        """

        if plot:
            plt.figure(figsize=(20, 10))
            plt.grid(which='both')
            plt.axis('equal')

        x_n_i, xd_n_i, px_n = self.x0, self.xd0, self.px0
        y_n_i, yd_n_i, py_n = self.y0, self.yd0, self.py0

        if plot:
            plt.plot(px_n, py_n, 'ko')

        px_star = self.px0
        py_star = self.py0

        # saving all trajectory and foot placement
        self.all_steps = []

        T, n = 0, 0
        while n < self.N:
            if verbose:
                print("-" * 100)
                print("curr step ", n)
                print("start pos (x_n_i, y_n_i)=(%.2f, %.2f)" % (x_n_i, y_n_i))
                print("start vel (xd_n_i, yd_n_i)=(%.2f, %.2f)" % (xd_n_i, yd_n_i))
                print("foot place (px_star, py_star)=(%.2f, %.2f)" % (px_star, py_star))
                print("simulate for Tsup %.2f sec..." % (self.Tsup))

            ts = np.linspace(0.0, self.Tsup_remain, num=1200) #todo: for two steps, this is correct, but not for multiple steps

            # x_n_f, xd_n_f = self.one_step(x=x_n_i, xd=xd_n_i, px=px_star, t=self.Tsup)
            # Xs = self.one_step(x=x_n_i, xd=xd_n_i, px=px_star, t=ts)
            #
            # y_n_f, yd_n_f = self.one_step(x=y_n_i, xd=yd_n_i, px=py_star, t=self.Tsup)
            # Ys = self.one_step(x=y_n_i, xd=yd_n_i, px=py_star, t=ts)

            x_n_f, xd_n_f, xdd_n_f = self.one_step_with_acc(x=x_n_i, xd=xd_n_i, px=px_star, t=self.Tsup_remain)
            Xs = self.one_step_with_acc(x=x_n_i, xd=xd_n_i, px=px_star, t=ts)

            y_n_f, yd_n_f, ydd_n_f = self.one_step_with_acc(x=y_n_i, xd=yd_n_i, px=py_star, t=self.Tsup_remain)
            Ys = self.one_step_with_acc(x=y_n_i, xd=yd_n_i, px=py_star, t=ts)

            com_pos = np.vstack((Xs[0], Ys[0], self.zc*np.ones(ts.shape)))
            com_vel = np.vstack((Xs[1], Ys[1], np.zeros(ts.shape)))
            com_acc = np.vstack((Xs[2], Ys[2], np.zeros(ts.shape)))

            this_step = {"n": n,
                         "support_foot": (-1) ** n,
                         "foot_placement": np.array([px_star, py_star]),
                         "foot_placement_xyz": np.array([px_star, py_star, 0.0]),
                         "ts": ts,
                         "Xs": Xs,
                         "Ys": Ys,
                         "com_pos": com_pos,
                         "com_vel": com_vel,
                         "com_acc": com_acc,
                         "com_pos_interp": interp1d(ts, com_pos),
                         "com_vel_interp": interp1d(ts, com_vel),
                         "com_acc_interp": interp1d(ts, com_acc),
                         "x_interp": interp1d(ts, Xs[0]),
                         "xd_interp": interp1d(ts, Xs[1]),
                         "y_interp": interp1d(ts, Ys[0]),
                         "yd_interp": interp1d(ts, Ys[1])}

            self.all_steps.append(this_step)

            if plot:
                plt.plot(Xs[0], Ys[0], 'k--')
                plt.plot(Xs[0][-1], Ys[0][-1], c='g', marker='*', markersize=10)

            if verbose:
                print("final pos (x_n_f, y_n_f)=(%.2f, %.2f)" % (x_n_f, y_n_f))
                print("final vel (xd_n_f, yd_n_f)=(%.2f, %.2f)" % (xd_n_f, yd_n_f))

            # step 4
            T = T + self.Tsup
            n = n + 1
            if verbose:
                print("next step:", n)

            x_n_i, xd_n_i = x_n_f, xd_n_f
            y_n_i, yd_n_i = y_n_f, yd_n_f

            # step 5
            if self.ini_support_leg == -1:
                px_n = px_n + self.sx[n - 1]
                py_n = py_n - (-1) ** n * self.sy[n - 1]
            elif self.ini_support_leg == 1:
                px_n = px_n + self.sx[n - 1]
                py_n = py_n + (-1) ** n * self.sy[n - 1]

            if verbose:
                print("next foot place (px_n, py_n) = (%.2f, %.2f) " % (px_n, py_n))
            if plot:
                plt.plot(px_n, py_n, c='k', marker='x', markersize=10, label='desired foot placement ' + str(n))

            # step 6
            if self.ini_support_leg == -1:
                x_bar_n = self.sx[n] / 2.0
                y_bar_n = (-1) ** n * self.sy[n] / 2.0
            elif self.ini_support_leg == 1:
                x_bar_n = self.sx[n] / 2.0
                y_bar_n = - (-1) ** n * self.sy[n] / 2.0

            print("x_bar_n ", x_bar_n)
            print("y_bar_n ", y_bar_n)

            xd_bar_n = (self.C + 1) / (self.Tc * self.S) * x_bar_n
            yd_bar_n = (self.C - 1) / (self.Tc * self.S) * y_bar_n
            if verbose:
                print("next walk primitive (x_bar_n, y_bar_n) = (%.2f, %.2f)" % (x_bar_n, y_bar_n))
                print("next walk primitive (xd_bar_n, yd_bar_n) = (%.2f, %.2f) " % (xd_bar_n, yd_bar_n))

            print("xd_bar_n ", xd_bar_n)
            print("yd_bar_n ", yd_bar_n)

            # step 7
            x_target, xd_target = px_n + x_bar_n, xd_bar_n
            y_target, yd_target = py_n + y_bar_n, yd_bar_n
            if verbose:
                print("target pos (x_target, y_target) = (%.2f, %.2f) " % (x_target, y_target))
                print("target vel (xd_target, yd_target) = (%.2f, %.2f) " % (xd_target, yd_target))
            # plt.plot(x_target, y_target, c='g', marker='*', markersize=10)

            print("x_target, xd_target ", x_target, xd_target)
            print("y_target, yd_target ", y_target, yd_target)


            # step 8
            a, b = 1.0, 10.0
            D = a * (self.C - 1) ** 2 + b * (self.S / self.Tc) ** 2

            px_star = - a * (self.C - 1) / D * (x_target - self.C * x_n_i - self.Tc * self.S * xd_n_i) - b * self.S / (
                    self.Tc * D) * (xd_target - self.S / self.Tc * x_n_i - self.C * xd_n_i)
            py_star = - a * (self.C - 1) / D * (y_target - self.C * y_n_i - self.Tc * self.S * yd_n_i) - b * self.S / (
                    self.Tc * D) * (yd_target - self.S / self.Tc * y_n_i - self.C * yd_n_i)
            if verbose:
                print("modified foot placement (px_star, py_star)=(%.2f, %.2f)" % (px_star, py_star))
            if plot:
                plt.plot(px_star, py_star, c='r', marker='x', markersize=10)



            if n == self.N:
                ts = np.linspace(0.0, self.Tsup, num=1200)
                # Xs = self.one_step(x=x_n_i, xd=xd_n_i, px=px_star, t=ts)
                # Ys = self.one_step(x=y_n_i, xd=yd_n_i, px=py_star, t=ts)
                Xs = self.one_step_with_acc(x=x_n_i, xd=xd_n_i, px=px_star, t=ts)
                Ys = self.one_step_with_acc(x=y_n_i, xd=yd_n_i, px=py_star, t=ts)


                com_pos = np.vstack((Xs[0], Ys[0], self.zc * np.ones(ts.shape)))
                com_vel = np.vstack((Xs[1], Ys[1], np.zeros(ts.shape)))
                com_acc = np.vstack((Xs[2], Ys[2], np.zeros(ts.shape)))

                this_step = {"n": n,
                             "support_foot": (-1) ** n,
                             "foot_placement": np.array([px_star, py_star]),
                             "foot_placement_xyz": np.array([px_star, py_star, 0.0]),
                             "ts": ts,
                             "Xs": Xs,
                             "Ys": Ys,
                             "com_pos": com_pos,
                             "com_vel": com_vel,
                             "com_acc": com_acc,
                             "com_pos_interp": interp1d(ts, com_pos),
                             "com_vel_interp": interp1d(ts, com_vel),
                             "com_acc_interp": interp1d(ts, com_acc),
                             "x_interp": interp1d(ts, Xs[0]),
                             "xd_interp": interp1d(ts, Xs[1]),
                             "y_interp": interp1d(ts, Ys[0]),
                             "yd_interp": interp1d(ts, Ys[1])}

                self.all_steps.append(this_step)

                if plot:
                    plt.plot(Xs[0], Ys[0], 'k--')
                    plt.plot(Xs[0][-1], Ys[0][-1], c='g', marker='*', markersize=10)

        if plot:
            plt.show()

        return self.all_steps

    def plan_analytical(self):
        """
        Remove print and plot.
        Chapter 4.3.3 -> Fig. 4.25 Algorithm of walking pattern generation based on 3D-LIP
        """


        x_n_i, xd_n_i, px_n = self.x0, self.xd0, self.px0
        y_n_i, yd_n_i, py_n = self.y0, self.yd0, self.py0

        px_star = self.px0
        py_star = self.py0

        # saving all trajectory and foot placement
        self.all_steps = []

        T, n = 0, 0
        while n < self.N:

            ts = np.linspace(0.0, self.Tsup_remain, num=1200)

            x_n_f, xd_n_f = self.one_step(x=x_n_i, xd=xd_n_i, px=px_star, t=self.Tsup_remain)
            Xs = self.one_step(x=x_n_i, xd=xd_n_i, px=px_star, t=ts)

            y_n_f, yd_n_f = self.one_step(x=y_n_i, xd=yd_n_i, px=py_star, t=self.Tsup_remain)
            Ys = self.one_step(x=y_n_i, xd=yd_n_i, px=py_star, t=ts)

            com_pos = np.vstack((Xs[0], Ys[0], self.zc*np.ones(ts.shape)))
            com_vel = np.vstack((Xs[1], Ys[1], np.zeros(ts.shape)))


            this_step = {"n": n,
                         "support_foot": (-1) ** n,
                         "foot_placement": np.array([px_star, py_star]),
                         "foot_placement_xyz": np.array([px_star, py_star, 0.0]),
                         "ts": ts,
                         "Xs": Xs,
                         "Ys": Ys,
                         "com_pos": com_pos,
                         "com_vel": com_vel,
                         "com_pos_interp": interp1d(ts, com_pos),
                         "com_vel_interp": interp1d(ts, com_vel),
                         "x_interp": interp1d(ts, Xs[0]),
                         "xd_interp": interp1d(ts, Xs[1]),
                         "y_interp": interp1d(ts, Ys[0]),
                         "yd_interp": interp1d(ts, Ys[1])}

            self.all_steps.append(this_step)


            # step 4
            T = T + self.Tsup
            n = n + 1


            x_n_i, xd_n_i = x_n_f, xd_n_f
            y_n_i, yd_n_i = y_n_f, yd_n_f

            # step 5
            px_n = px_n + self.sx[n - 1]
            py_n = py_n - (-1) ** n * self.sy[n - 1] # todo: make this general, if start from left support?


            # step 6
            x_bar_n = self.sx[n] / 2.0
            y_bar_n = (-1) ** n * self.sy[n] / 2.0 # todo: make this general, if start from left support?

            xd_bar_n = (self.C + 1) / (self.Tc * self.S) * x_bar_n
            yd_bar_n = (self.C - 1) / (self.Tc * self.S) * y_bar_n


            # step 7
            x_target, xd_target = px_n + x_bar_n, xd_bar_n
            y_target, yd_target = py_n + y_bar_n, yd_bar_n


            # step 8
            a, b = 10.0, 1.0
            D = a * (self.C - 1) ** 2 + b * (self.S / self.Tc) ** 2

            px_star = - a * (self.C - 1) / D * (x_target - self.C * x_n_i - self.Tc * self.S * xd_n_i) - b * self.S / (
                    self.Tc * D) * (xd_target - self.S / self.Tc * x_n_i - self.C * xd_n_i)
            py_star = - a * (self.C - 1) / D * (y_target - self.C * y_n_i - self.Tc * self.S * yd_n_i) - b * self.S / (
                    self.Tc * D) * (yd_target - self.S / self.Tc * y_n_i - self.C * yd_n_i)

            if n == self.N:
                ts = np.linspace(0.0, self.Tsup, num=1200)
                Xs = self.one_step(x=x_n_i, xd=xd_n_i, px=px_star, t=ts)
                Ys = self.one_step(x=y_n_i, xd=yd_n_i, px=py_star, t=ts)

                com_pos = np.vstack((Xs[0], Ys[0], self.zc * np.ones(ts.shape)))
                com_vel = np.vstack((Xs[1], Ys[1], np.zeros(ts.shape)))

                this_step = {"n": n,
                             "support_foot": (-1) ** n,
                             "foot_placement": np.array([px_star, py_star]),
                             "foot_placement_xyz": np.array([px_star, py_star, 0.0]),
                             "ts": ts,
                             "Xs": Xs,
                             "Ys": Ys,
                             "com_pos": com_pos,
                             "com_vel": com_vel,
                             "com_pos_interp": interp1d(ts, com_pos),
                             "com_vel_interp": interp1d(ts, com_vel),
                             "x_interp": interp1d(ts, Xs[0]),
                             "xd_interp": interp1d(ts, Xs[1]),
                             "y_interp": interp1d(ts, Ys[0]),
                             "yd_interp": interp1d(ts, Ys[1])}
                self.all_steps.append(this_step)



        return self.all_steps


    def obj_fun_x(self, opt_x, *args):
        px_star = opt_x
        x_n_i, xd_n_i, x_target, xd_target = args

        x_n_f, xd_n_f = self.one_step(x=x_n_i, xd=xd_n_i, px=px_star, t=self.Tsup)

        a, b = 1.0, 10.0
        goal = a*(x_target - x_n_f)**2 + b*(xd_target-xd_n_f)**2

        return goal

    def obj_fun_xy(self, opt_var, *args):
        px_star, py_star = opt_var
        x_n_i, xd_n_i, x_target, xd_target, y_n_i, yd_n_i, y_target, yd_target = args

        x_n_f, xd_n_f = self.one_step(x=x_n_i, xd=xd_n_i, px=px_star, t=self.Tsup)
        y_n_f, yd_n_f = self.one_step(x=y_n_i, xd=yd_n_i, px=py_star, t=self.Tsup)

        a, b = 1.0, 100.0
        goal = a * (x_target - x_n_f) ** 2 + b * (xd_target - xd_n_f) ** 2 + \
               a * (y_target - y_n_f) ** 2 + b * (yd_target - yd_n_f) ** 2

        return goal

    def obj_fun_xy_Tsup(self, opt_var, *args):
        px_star, py_star, Tsup = opt_var
        x_n_i, xd_n_i, x_target, xd_target, y_n_i, yd_n_i, y_target, yd_target = args

        x_n_f, xd_n_f = self.one_step(x=x_n_i, xd=xd_n_i, px=px_star, t=Tsup)
        y_n_f, yd_n_f = self.one_step(x=y_n_i, xd=yd_n_i, px=py_star, t=Tsup)

        a, b = 1.0, 10.0
        goal = a * (x_target - x_n_f) ** 2 + b * (xd_target - xd_n_f) ** 2 + \
               a * (y_target - y_n_f) ** 2 + b * (yd_target - yd_n_f) ** 2



        return goal

    def plan_numerical(self):

        x_n_i, xd_n_i, px_n = self.x0, self.xd0, self.px0
        y_n_i, yd_n_i, py_n = self.y0, self.yd0, self.py0

        px_star = self.px0
        py_star = self.py0

        # saving all trajectory and foot placement
        self.all_steps = []

        T, n = 0, 0
        while n < self.N:

            ts = np.linspace(0.0, self.Tsup_remain, num=1200)

            x_n_f, xd_n_f, xdd_n_f = self.one_step_with_acc(x=x_n_i, xd=xd_n_i, px=px_star, t=self.Tsup_remain)
            Xs = self.one_step_with_acc(x=x_n_i, xd=xd_n_i, px=px_star, t=ts)

            y_n_f, yd_n_f, ydd_n_f = self.one_step_with_acc(x=y_n_i, xd=yd_n_i, px=py_star, t=self.Tsup_remain)
            Ys = self.one_step_with_acc(x=y_n_i, xd=yd_n_i, px=py_star, t=ts)

            com_pos = np.vstack((Xs[0], Ys[0], self.zc * np.ones(ts.shape)))
            com_vel = np.vstack((Xs[1], Ys[1], np.zeros(ts.shape)))
            com_acc = np.vstack((Xs[2], Ys[2], np.zeros(ts.shape)))

            this_step = {"n": n,
                         "Tsup": self.Tsup,
                         "support_foot": (-1) ** n,
                         "foot_placement": np.array([px_star, py_star]),
                         "foot_placement_xyz": np.array([px_star, py_star, 0.0]),
                         "ts": ts,
                         "Xs": Xs,
                         "Ys": Ys,
                         "com_pos": com_pos,
                         "com_vel": com_vel,
                         "com_acc": com_acc,
                         "com_pos_interp": interp1d(ts, com_pos),
                         "com_vel_interp": interp1d(ts, com_vel),
                         "com_acc_interp": interp1d(ts, com_acc),
                         "x_interp": interp1d(ts, Xs[0]),
                         "xd_interp": interp1d(ts, Xs[1]),
                         "y_interp": interp1d(ts, Ys[0]),
                         "yd_interp": interp1d(ts, Ys[1])}
            self.all_steps.append(this_step)

            # step 4
            T = T + self.Tsup
            n = n + 1

            x_n_i, xd_n_i = x_n_f, xd_n_f
            y_n_i, yd_n_i = y_n_f, yd_n_f

            # step 5
            if self.ini_support_leg == -1:
                px_n = px_n + self.sx[n - 1]
                py_n = py_n - (-1) ** n * self.sy[n - 1]
            elif self.ini_support_leg == 1:
                px_n = px_n + self.sx[n - 1]
                py_n = py_n + (-1) ** n * self.sy[n - 1]


            # step 6
            if self.ini_support_leg == -1:
                x_bar_n = self.sx[n] / 2.0
                y_bar_n = (-1) ** n * self.sy[n] / 2.0
            elif self.ini_support_leg == 1:
                x_bar_n = self.sx[n] / 2.0
                y_bar_n = - (-1) ** n * self.sy[n] / 2.0


            xd_bar_n = (self.C + 1) / (self.Tc * self.S) * x_bar_n
            yd_bar_n = (self.C - 1) / (self.Tc * self.S) * y_bar_n

            print("xd_bar: ", xd_bar_n)
            print("yd_bar: ", yd_bar_n)

            # step 7
            x_target, xd_target = px_n + x_bar_n, xd_bar_n
            y_target, yd_target = py_n + y_bar_n, yd_bar_n

            # Analytical way
            # a, b = 10.0, 1.0
            # D = a * (self.C - 1) ** 2 + b * (self.S / self.Tc) ** 2
            #
            # px_star = - a * (self.C - 1) / D * (x_target - self.C * x_n_i - self.Tc * self.S * xd_n_i) - b * self.S / (
            #         self.Tc * D) * (xd_target - self.S / self.Tc * x_n_i - self.C * xd_n_i)
            # py_star = - a * (self.C - 1) / D * (y_target - self.C * y_n_i - self.Tc * self.S * yd_n_i) - b * self.S / (
            #         self.Tc * D) * (yd_target - self.S / self.Tc * y_n_i - self.C * yd_n_i)

            # # Numerical way: x,y separated
            # opt_x0 = np.array([px_n])
            # args_x = (x_n_i, xd_n_i, x_target, xd_target)
            # res_x = minimize(self.obj_fun_x, opt_x0, args=args_x, method='SLSQP')
            # print("res_x: ", res_x)
            # px_star = res_x.x
            #
            # opt_y0 = np.array([py_n])
            # args_y = (y_n_i, yd_n_i, y_target, yd_target)
            # res_y = minimize(self.obj_fun_x, opt_y0, args=args_y, method='SLSQP')
            # print("res_y: ", res_y)
            # py_star = res_y.x

            # Numerical way: x, y together
            sx_bnds = [-0.3, 0.3]
            sy_bnds = [-0.3, 0.3] # todo: better self collision avoidance
            px_bnds = px_n + sx_bnds
            py_bnds = py_n + sy_bnds
            opt_var0 = np.array([px_n, py_n])
            args = (x_n_i, xd_n_i, x_target, xd_target, y_n_i, yd_n_i, y_target, yd_target)
            res = minimize(self.obj_fun_xy, opt_var0, args=args, method='SLSQP')
            print("res: ", res.x)
            px_star, py_star = res.x

            # # Numerical way: x, y together plus Tsup
            # opt_var0 = np.array([px_n, py_n, self.Tsup])
            # sx_bnds = [-0.4, 0.4]
            # sy_bnds = [-0.4, 0.4]
            # px_bnds = px_n + sx_bnds
            # py_bnds = py_n + sy_bnds
            # Tsup_bnds = [0.399, 0.401]
            # bnds = (px_bnds, py_bnds, Tsup_bnds)
            # args = (x_n_i, xd_n_i, x_target, xd_target, y_n_i, yd_n_i, y_target, yd_target)
            # res = minimize(self.obj_fun_xy_Tsup, opt_var0, args=args, method='SLSQP', bounds=bnds)
            # print("px_star, py_star, nextTsup: ", res.x)
            # px_star, py_star, self.Tsup = res.x


            if n == self.N:
                ts = np.linspace(0.0, self.Tsup, num=1200)
                # Xs = self.one_step(x=x_n_i, xd=xd_n_i, px=px_star, t=ts)
                # Ys = self.one_step(x=y_n_i, xd=yd_n_i, px=py_star, t=ts)
                Xs = self.one_step_with_acc(x=x_n_i, xd=xd_n_i, px=px_star, t=ts)
                Ys = self.one_step_with_acc(x=y_n_i, xd=yd_n_i, px=py_star, t=ts)

                com_pos = np.vstack((Xs[0], Ys[0], self.zc * np.ones(ts.shape)))
                com_vel = np.vstack((Xs[1], Ys[1], np.zeros(ts.shape)))
                com_acc = np.vstack((Xs[2], Ys[2], np.zeros(ts.shape)))

                this_step = {"n": n,
                             "Tsup": self.Tsup,
                             "support_foot": (-1) ** n,
                             "foot_placement": np.array([px_star, py_star]),
                             "foot_placement_xyz": np.array([px_star, py_star, 0.0]),
                             "ts": ts,
                             "Xs": Xs,
                             "Ys": Ys,
                             "com_pos": com_pos,
                             "com_vel": com_vel,
                             "com_acc": com_acc,
                             "com_pos_interp": interp1d(ts, com_pos),
                             "com_vel_interp": interp1d(ts, com_vel),
                             "com_acc_interp": interp1d(ts, com_acc),
                             "x_interp": interp1d(ts, Xs[0]),
                             "xd_interp": interp1d(ts, Xs[1]),
                             "y_interp": interp1d(ts, Ys[0]),
                             "yd_interp": interp1d(ts, Ys[1])}
                self.all_steps.append(this_step)

        return self.all_steps




def start_with_right_support():
    walk_parameters = np.array([[0.0, 0.2, 0.2, 0.2, 0.0, 0.0],
                                [0.1, 0.1, 0.1, 0.1, 0.1, 0.0]])

    ini_foot_placement = np.array([0.0, -0.1])
    ini_com_pos = ini_foot_placement + np.array([0.0, 0.0125])
    ini_com_vel = np.array([0.0, 0.03])
    Tsup = 0.4

    ini_support_leg = -1  # right foot

    walking_pattern_generator = WalkingPatternGenerator(walk_parameters,
                                                        ini_com_pos,
                                                        ini_com_vel,
                                                        ini_foot_placement,
                                                        ini_support_leg=ini_support_leg,
                                                        Tsup=Tsup, zc=0.45)

    # walking_pattern_generator.plan(verbose=True, plot=True)
    # walking_pattern_generator.plan_analytical()
    # walking_pattern_generator.plot_plan()
    walking_pattern_generator.plan_numerical()
    # walking_pattern_generator.plot_plan()

    plt.show()
    print(walking_pattern_generator.all_steps)

def start_with_left_support():
    walk_parameters = np.array([[0.0, 0.0],
                                [0.1, 0.1]])

    ini_foot_placement = np.array([ 0.08258,  0.03864])
    ini_com_pos = np.array([ 0.07914, -0.01963])
    ini_com_vel = np.array([ 0.04718,  0.13622])
    Tsup = 0.4

    ini_support_leg = 1  # left foot

    walking_pattern_generator = WalkingPatternGenerator(walk_parameters,
                                                        ini_com_pos,
                                                        ini_com_vel,
                                                        ini_foot_placement,
                                                        ini_support_leg=ini_support_leg,
                                                        Tsup=Tsup, zc=0.45, g=9.81,
                                                        Tsup_remain=0.26)

    walking_pattern_generator.plan(verbose=True, plot=True)
    # walking_pattern_generator.plan_analytical()
    # walking_pattern_generator.plot_plan()
    # walking_pattern_generator.plan_numerical()
    # walking_pattern_generator.plot_plan()

    plt.show()
    print(walking_pattern_generator.all_steps)

if __name__ == "__main__":
    print("[main]")
    # start_with_right_support()
    start_with_left_support()