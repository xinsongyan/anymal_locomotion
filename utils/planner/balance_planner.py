import numpy as np
from utils.yaml_parser import load_yaml

import utils.transformations as tf
from copy import deepcopy

class BalancePlanner:
    def __init__(self, yaml_path):
        self.configs = load_yaml(yaml_path)
        self.ini_recorded = False


    def __call__(self, robot_state):

        if self.ini_recorded is False:
            foot_positions = []
            for foot_name in self.configs["foot_names"]:
                foot_positions.append(robot_state.end_effector[foot_name].pos)
            self.ref_com_pos = np.mean(foot_positions, 0)
            self.ref_com_pos[0] = 0
            self.ref_com_pos[1] = 0
            self.ref_com_pos[2] = self.configs["com_height"]
            self.ini_recorded = True



        from utils.command import HighLevelCommand
        import utils.transformations as tf
        high_level_command = HighLevelCommand()

        high_level_command.set_com_target(com_pos=self.ref_com_pos)
        high_level_command.set_base_target(base_quat=tf.quaternion_from_euler(0,-np.pi/2,0))
        high_level_command.set_support_legs(self.configs["foot_names"])
        high_level_command.set_swing_legs(swing_legs=None)
        high_level_command.set_task_hierarchy(task_hierarchy=deepcopy(self.configs["balance_task_hierarchy"]))


        return high_level_command