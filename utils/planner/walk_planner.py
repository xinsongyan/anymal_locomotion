import numpy as np
from utils.yaml_parser import load_yaml
from utils.command import HighLevelCommand
from utils.trajectory import SwingTrajectory
from utils.planner.LIPM.foot_step_optimization_MPC import FootstepOptimizer
from utils.planner.LIPM.foot_step_optimization_MPC import FootstepPlanner


class WalkPlanner:
    def __init__(self, yaml_path):
        self.configs = load_yaml(yaml_path)
        self.end_effector_home_pos_xy = self.configs['end_effector_home_pos_xy']
        self.end_effector_home_pos_x = self.configs['end_effector_home_pos_x']
        self.robot_type = self.configs['robot_type']
        self.end_effector_types = self.configs["end_effector_types"]
        self.ini_recorded = False

        self.footstep_planner = FootstepPlanner(yaml_path)

    def __call__(self, mpc_state_machine, step_state_machine, leg_coordinator, state_estimator):

        if step_state_machine.transition == 1:  # plan a trajectory for swing feet at transition moment
            print("step_transition")

            support_leg_positions = []
            for support_leg in leg_coordinator.support_legs:
                support_leg_positions.append(state_estimator.end_effector[support_leg].pos)
            virtual_support_foot_pos = np.mean(support_leg_positions, 0)
            self.virtual_support_foot_pos = virtual_support_foot_pos

            swing_leg_positions = []
            for swing_leg in leg_coordinator.swing_legs:
                swing_leg_positions.append(state_estimator.end_effector[swing_leg].pos)
            virtual_swing_ini_pos = np.mean(swing_leg_positions, 0)

            opt_footsteps = self.footstep_planner.plan(cur_com_pos=state_estimator.com_pos,
                                                        cur_com_vel=state_estimator.com_vel,
                                                        cur_sup_foot = leg_coordinator.support_leg,
                                                        cur_sup_foot_pos=virtual_support_foot_pos,
                                                        T_remain=step_state_machine.time_remain)


            self.swing_trajectories = {}
            if self.robot_type == "quadruped":
                if leg_coordinator.gait == "trot":
                    offset = self.end_effector_home_pos_xy
                elif leg_coordinator.gait == "pace":
                    offset = self.end_effector_home_pos_x

                for swing_leg in leg_coordinator.swing_legs:
                    swing_trajectory = SwingTrajectory(ini_pos=state_estimator.end_effector[swing_leg].pos,
                                                       end_pos=opt_footsteps[0, :] + offset[swing_leg],
                                                       mid_height=self.configs["swing_height"],
                                                       duration=self.configs["step_duration"])
                    self.swing_trajectories[swing_leg] = swing_trajectory

            elif self.robot_type == "biped":
                for swing_leg in leg_coordinator.swing_legs:
                    swing_trajectory = SwingTrajectory(ini_pos=state_estimator.end_effector[swing_leg].pos,
                                                       end_pos=opt_footsteps[0, :],
                                                       mid_height=self.configs["swing_height"],
                                                       duration=self.configs["step_duration"])
                    self.swing_trajectories[swing_leg] = swing_trajectory


            step_tt = np.arange(0, self.configs["step_duration"], 1.0/self.configs["sim_rate"])
            self.com_trajectory_nominal = self.footstep_planner.get_com_states(step_tt)

        if mpc_state_machine.transition == 1:
            print("mpc_transition")


            # support_leg_positions = []
            # for support_leg in leg_coordinator.support_legs:
            #     support_leg_positions.append(state_estimator.end_effector[support_leg].pos)
            # mpc_virtual_support_foot_pos = np.mean(support_leg_positions, 0)

            self.mpc_footsteps = self.footstep_planner.plan(cur_com_pos=state_estimator.com_pos,
                                                        cur_com_vel=state_estimator.com_vel,
                                                        cur_sup_foot =leg_coordinator.support_leg,
                                                        cur_sup_foot_pos=self.virtual_support_foot_pos,
                                                        T_remain=step_state_machine.time_remain)
            mpc_tt = np.arange(0, self.configs["mpc_duration"], 1.0/self.configs["sim_rate"])
            self.com_trajectory_mpc = self.footstep_planner.get_com_states(mpc_tt)

        high_level_command = HighLevelCommand()
        # high_level_command.set_com_target(self.footstep_planner.get_com_state(step_state_machine.time_past))
        high_level_command.set_com_target(com_pos=self.com_trajectory_nominal[0][:, step_state_machine.loops_past],
                                          com_vel=self.com_trajectory_nominal[1][:, step_state_machine.loops_past],
                                          com_acc=self.com_trajectory_nominal[2][:, step_state_machine.loops_past])
        high_level_command.set_base_target(base_quat=np.array([0, 0, 0, 1]))
        high_level_command.set_support_legs(leg_coordinator.support_legs)
        high_level_command.set_swing_legs(leg_coordinator.swing_legs)
        swing_leg_target_positions = {}
        swing_leg_target_quaternions = {}
        for swing_leg in leg_coordinator.swing_legs:
            swing_leg_target_positions[swing_leg] = self.swing_trajectories[swing_leg].interpolate(step_state_machine.time_past)
            swing_leg_target_quaternions[swing_leg] = np.array([0, 0, 0, 1])

        alpha = step_state_machine.time_past/step_state_machine.duration
        for swing_leg in leg_coordinator.swing_legs:
            swing_leg_target_positions[swing_leg]["pos"] += alpha * (self.mpc_footsteps[0,:] + self.end_effector_home_pos_xy[swing_leg] - self.swing_trajectories[swing_leg].pos(step_state_machine.duration))
            swing_leg_target_quaternions[swing_leg] = np.array([0, 0, 0, 1])


        high_level_command.set_swing_leg_positions(swing_leg_target_positions)
        high_level_command.set_swing_leg_quaternions(swing_leg_target_quaternions)
        high_level_command.set_task_hierarchy(task_hierarchy=self.configs["walk_task_hierarchy"])

        return high_level_command
