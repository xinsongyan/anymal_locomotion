import numpy as np
from robots.utils.yaml_parser import load_yaml
from robots.utils.command import HighLevelCommand
from robots.utils.trajectory import SwingTrajectory


class StepInPlacePlanner:
    def __init__(self, yaml_path):
        self.configs = load_yaml(yaml_path)
        self.robot_type = self.configs['robot_type']
        self.end_effector_types = self.configs["end_effector_types"]
        self.ini_recorded = False


    def __call__(self, mpc_state_machine, step_state_machine, leg_coordinator, state_estimator):
        if step_state_machine.transition == 1:  # plan a trajectory for CoM at transition moment
            print("step_transition")

            support_leg_positions = []
            for support_leg in leg_coordinator.support_legs:
                support_leg_positions.append(state_estimator.end_effector[support_leg].pos)

            


