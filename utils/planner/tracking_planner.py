import numpy as np
from numpy import genfromtxt
from robots.utils.yaml_parser import load_yaml
from robots.utils.command import HighLevelCommand
from copy import deepcopy

class TrackingPlanner:
    def __init__(self, yaml_path):
        self.configs = load_yaml(yaml_path)



        # self.trajectory = genfromtxt(self.configs["trajectory_csv_path"])
        self.trajectory_with_com = genfromtxt(self.configs["trajectory_with_com_csv_path"])
        self.pose_trajectory = self.trajectory_with_com[:,:-3]
        self.com_trajectory = self.trajectory_with_com[:,-3:]

        self.num_of_frames, self.pose_dim = self.pose_trajectory.shape

        print("pose_trajectory: ", self.pose_trajectory.shape)
        print("com_trajectory: ", self.com_trajectory.shape)

    def __call__(self, sim_count, speed_ratio=1.0):
        frame = int(sim_count*speed_ratio)

        return self.pose_trajectory[np.mod(frame, self.num_of_frames), :]

        # if sim_count < self.num_of_frames:
        #     print(np.mod(sim_count, self.num_of_frames))
        #     return self.pose_trajectory[np.mod(sim_count, self.num_of_frames), :]
        # else:
        #     return self.pose_trajectory[-1,:]

    def high_level_command(self, sim_count, robot_state, speed_ratio=1.0):
        high_level_command = HighLevelCommand()

        frame = int(sim_count * speed_ratio)
        frame = np.mod(frame, self.num_of_frames)
        print("frame:", frame)
        pose = self.pose_trajectory[frame, :]
        com_wrt_lfoot = self.com_trajectory[frame, :]

        world_T_lfoot = robot_state.end_effector["leftFoot"].transform_matrix
        com_wrt_world = world_T_lfoot.dot(np.concatenate((com_wrt_lfoot,[1])))

        high_level_command.set_com_target(com_pos=com_wrt_world[:3])
        high_level_command.set_whole_body_config(config=pose)
        high_level_command.set_support_legs(self.configs["foot_names"])
        high_level_command.set_swing_legs(swing_legs=None)
        high_level_command.set_task_hierarchy(task_hierarchy=deepcopy(self.configs["balance_task_hierarchy"]))

        return high_level_command
