from qpsolvers import solve_qp
from scipy.linalg import null_space
import numpy as np

class Task:
    def __init__(self, A, b, w=1):
        '''
        :param A: task matrix
        :param b: task vector
        :param w: task weight
        '''
        self.A = A
        self.b = b
        self.w = w

    def set_matrix(self, A):
        self.A = A

    def set_vector(self, b):
        self.b = b

    def set_weight(self, w):
        self.w = w

    def __str__(self):
        return "Task:" + ", w:" + str(self.w)

    def print(self):
        print("Task:",)
        print("A", self.A.shape, ":", "\n", self.A)
        print("b", self.b.shape, ":", "\n", self.b)
        print("w:", self.w)


class HierarchicalQP:
    def __init__(self, slack_variable=1e-10):
        self.slack_variable = slack_variable

    # def compress_tasks_in_each_level(self):
    #     for level in range(len(self.tasks)):
    #         if isinstance(self.tasks[level], list):
    #             level_task_list = self.tasks[level]
    #             A = np.empty((0, self.x_dim))
    #             b = np.empty(0)
    #             for task in level_task_list:
    #                 A = np.vstack((A, task.w * task.A))
    #                 b = np.hstack((b, task.w * task.b))
    #             level_task = Task(A,b)
    #             self.tasks[level] = level_task
    #
    # def solve(self, slack_variable=1e-10):
    #     self.compress_tasks_in_each_level()
    #     D0, f0 = self.ineq_cons_matrix, self.ineq_cons_vector
    #     C_bar, d_bar = np.eye(self.x_dim), np.zeros(self.x_dim)
    #     for i in range(len(self.tasks)):
    #         Ai_bar = self.tasks[i].A.dot(C_bar)
    #         bi_bar = self.tasks[i].b - self.tasks[i].A.dot(d_bar)
    #         P = Ai_bar.T.dot(Ai_bar)
    #         P += np.eye(P.shape[0]) * float(slack_variable)
    #         di = solve_qp(P=P,
    #                       q=-Ai_bar.T.dot(bi_bar),
    #                       G=D0.dot(C_bar),
    #                       h=f0-D0.dot(d_bar),
    #                       A=None,
    #                       b=None,
    #                       solver='quadprog')
    #         d_bar = d_bar + C_bar.dot(di)
    #         C_bar = C_bar.dot(null_space(Ai_bar))
    #     return d_bar

    def null_space_projector(self, M):
        from scipy.linalg import pinv
        null_space_projector = np.eye(M.shape[1]) - pinv(M).dot(M)
        return null_space_projector


    def solve(self, x_dim, task_hierarchy, ineq_cons_matrix, ineq_cons_vector):

        # extract uniq and sorted priority list
        priority_list = []
        for task_name, task_description in task_hierarchy.items():
            priority_list.append(task_description["priority"])
        priority_list = list(set(priority_list))  # remove repeated priority
        priority_list.sort()

        # compress tasks with same priority
        task_hierarchy_compressed = {}
        for priority in priority_list:
            A, b = [], []
            for task_name, task_description in task_hierarchy.items():
                if task_description["priority"] == priority:
                    A.append(task_description["A"]*task_description["weight"])
                    b.append(task_description["b"]*task_description["weight"])
            A = np.vstack(A)
            b = np.hstack(b)
            task_hierarchy_compressed[priority] = {"A":A, "b":b}


        # solve hierarchical QP based on priority
        D0, f0 = ineq_cons_matrix, ineq_cons_vector
        C_bar, d_bar = np.eye(x_dim), np.zeros(x_dim)
        for priority, task in task_hierarchy_compressed.items():
            Ai_bar = task["A"].dot(C_bar)
            bi_bar = task["b"] - task["A"].dot(d_bar)
            P = Ai_bar.T.dot(Ai_bar)
            P += np.eye(P.shape[0]) * float(self.slack_variable)
            di = solve_qp(P=P,
                          q=-Ai_bar.T.dot(bi_bar),
                          G=D0.dot(C_bar),
                          h=f0 - D0.dot(d_bar),
                          A=None,
                          b=None,
                          solver='quadprog')
            d_bar = d_bar + C_bar.dot(di)
            C_bar = C_bar.dot(self.null_space_projector(Ai_bar))
        return d_bar















