import numpy as np
from scipy.interpolate import CubicSpline


class SwingTrajectory:
    def __init__(self, ini_pos, end_pos, mid_height=0.2, duration=0.3,):
        self.ini_pos = np.array(ini_pos)
        self.end_pos = np.array(end_pos)
        self.mid_height = mid_height
        self.duration = duration

        self.mid_pos = 0.5 * (self.ini_pos + self.end_pos)
        self.mid_pos[2] = mid_height

        self.ini_vel = np.zeros(3)
        self.end_vel = np.zeros(3)

        self.cubic_spline = CubicSpline(x=[0.0, self.duration / 2.0, self.duration],
                                            y=np.array([self.ini_pos, self.mid_pos, self.end_pos]),
                                            axis=0,
                                            bc_type=((1, self.ini_vel), (1, self.end_vel)))

    def pos(self, t):
        return self.cubic_spline(t)

    def vel(self, t):
        return self.cubic_spline(t, 1)

    def acc(self, t):
        return self.cubic_spline(t, 2)

    def interpolate(self, t):
        return {"pos": self.cubic_spline(t),
                "vel": self.cubic_spline(t, 1),
                "acc": self.cubic_spline(t, 2)}
