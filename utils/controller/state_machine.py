from utils.yaml_parser import load_yaml

class LegCoordinator:
    def __init__(self, yaml_path):
        self.configs = load_yaml(yaml_path)
        self.robot_type = self.configs["robot_type"]
        self.end_effector_names = self.configs["end_effector_names"]
        self.ini_support_leg = self.configs["ini_support_leg"]
        self.gait = self.configs["gait"]

    def __call__(self, step_state_machine):
        # contact state
        self.support_leg = (-1) ** step_state_machine.state_count * self.ini_support_leg
        if self.support_leg == 1:
            self.contact_state = "leftSupport"
        else:
            self.contact_state = "rightSupport"


        if self.robot_type == "biped":
            self.support_legs, self.swing_legs = self.biped(self.contact_state)
        elif self.robot_type == "quadruped":
            self.support_legs, self.swing_legs = self.quadruped(self.contact_state, self.gait)

    def gait_transition(self, step_state_machine):
        if step_state_machine.loops_past == 0:
            if divmod(step_state_machine.state_count, 4)[1] == 0:
                if self.gait == "trot":
                    self.gait = "pace"
                elif self.gait == "pace":
                    self.gait = "trot"


    def biped(self, contact_state):
        support_legs = self.configs["contact_support_map"][contact_state]
        swing_legs = self.end_effector_names.copy()
        for i in support_legs:
            swing_legs.remove(i)
        return support_legs, swing_legs

    def quadruped(self, contact_state, gait):
        support_legs = self.configs["contact_support_map"][gait][contact_state]
        swing_legs = self.end_effector_names.copy()
        for i in support_legs:
            swing_legs.remove(i)
        return support_legs, swing_legs




class StateMachine:
    def __init__(self, duration, sim_rate):
        self.duration = duration
        self.time_step = 1/sim_rate
        self.loops_per_state = int(self.duration / self.time_step)

    def __call__(self, loop_count):
        # state loops and time
        self.state_count, self.loops_past = divmod(loop_count, self.loops_per_state)
        self.loops_remain = self.loops_per_state - self.loops_past
        self.time_past = self.loops_past * self.time_step
        self.time_remain = self.loops_remain * self.time_step

        if self.loops_past == 0:
            self.transition = True
        else:
            self.transition = False



