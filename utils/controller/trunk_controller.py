def trunk_control(self, high_level_cmd, loop_count):
    step_count, step_past_n, step_remain_n, step_time_past, step_time_remain = self.state_machine(loop_count)
    swing_legs, support_legs = self.get_swing_support_leg_indexes(high_level_cmd.contact_state)

    num_of_contacts = len(support_legs)
    GRF_dim = num_of_contacts * 3

    J_contact = []
    for i in support_legs:
        J_contact.append(self.robot_state.EE[i].J[-3:, :])
    J_contact = np.vstack(J_contact)

    from robots.utils.task import Task
    # least square task: minimize qdd and GRFs
    ls_A = np.identity(GRF_dim)
    ls_b = np.zeros(GRF_dim)
    ls_task = Task(ls_A, ls_b)

    # # equal distribution task
    # eq_A = np.hstack((np.eye(3), -np.eye(3)))
    # eq_b = np.zeros(3)
    # eq_task = Task(eq_A, eq_b)

    # com position task
    kp_linear = 600
    kd_linear = 30
    des_linear_momentum = self.robot_state.mass * (
            kp_linear * (high_level_cmd.com_pos - self.robot_state.com) +
            kd_linear * (high_level_cmd.com_vel_linear - self.robot_state.com_velocity) +
            high_level_cmd.com_acc_linear)
    linear_momentum_b = des_linear_momentum + self.robot_state.mass * np.array([0, 0, 9.81])

    linear_momentum_A = np.hstack([np.eye(3)] * num_of_contacts)
    linear_momentum_task = Task(linear_momentum_A, linear_momentum_b)

    # base orientation task
    from robots.utils.geometry import quaternionPD, rotationPD

    from scipy.linalg import block_diag
    I_base = block_diag(pybullet.getDynamicsInfo(self.id, -1)[2])
    # I_base = np.array([[base_inertia.ixx, base_inertia.ixy, base_inertia.ixz],
    #                    [base_inertia.ixy, base_inertia.iyy, base_inertia.iyz],
    #                    [base_inertia.ixz, base_inertia.iyz, base_inertia.izz]])
    # I_base = np.array([[base_inertia.ixx, 0.0, 0.0],
    #                    [0.0, base_inertia.iyy, 0.0],
    #                    [0.0, 0.0, base_inertia.izz]])

    R_base = self.robot_state.base.rotation_matrix
    I_G = R_base.dot(I_base).dot(R_base.T)
    base_orientation_b = quaternionPD(quat_des=high_level_cmd.com_quat,
                                      quat_cur=self.robot_state.base.quaternion,
                                      omega_des=np.zeros(3),
                                      omega_cur=self.robot_state.base.linear_velocity,
                                      kp=5000, kd=500)
    base_orientation_b = rotationPD(
        rot_des=np.eye(3),
        # rot_des=quaternion_matrix(high_level_cmd.com_quat)[:3,:3],
        rot_cur=self.robot_state.base.rotation_matrix,
        omega_des=np.zeros(3),
        omega_cur=self.robot_state.base.angular_velocity,
        kp=5000, kd=500)

    base_orientation_b = I_G.dot(base_orientation_b)

    skew = lambda V: np.array([[0, -V[2], V[1]], [V[2], 0, -V[0]], [-V[1], V[0], 0]])
    com_P_ee = []
    # for ee in self.robot_state.EE:
    #     com_P_ee.append(skew(ee.pos - self.robot_state.com_pos))
    for i in support_legs:
        com_P_ee.append(skew(self.robot_state.EE[i].pos - self.robot_state.com_pos))

    base_orientation_A = np.hstack(com_P_ee)
    base_orientation_task = Task(base_orientation_A, base_orientation_b)

    linear_momentum_task.set_weight(1)
    base_orientation_task.set_weight(1)
    tasks = [linear_momentum_task, base_orientation_task]

    # combine all task A and b matrices
    A = np.empty((0, GRF_dim))
    b = np.empty(0)
    for task in tasks:
        A = np.vstack((A, task.w * task.A))
        b = np.hstack((b, task.w * task.b))

    ############################
    #  Inequality constraints  #
    ############################
    # friction constraints
    mu = 0.7
    frictionCons = np.array([[1, 0, -mu],
                             [-1, 0, -mu],
                             [0, 1, -mu],
                             [0, -1, -mu]])

    # unilateral constraints
    unilateralCons = np.array([[0, 0, -1]])

    # GRF constraints: friction constraints + unilateral constraints
    forceCons = np.vstack((frictionCons,
                           unilateralCons))

    from scipy import linalg
    GRFCons = []
    for i in range(num_of_contacts):
        GRFCons = linalg.block_diag(GRFCons, forceCons)

    inequalityConsMatrix = GRFCons
    inequalityConsVector = np.zeros(GRFCons.shape[0])

    ########################
    # Equality constraints #
    ########################
    # J_contact = np.vstack((self.robot_state.EE[0].J[-3:, :],
    #                        self.robot_state.EE[1].J[-3:, :],
    #                        self.robot_state.EE[2].J[-3:, :],
    #                        self.robot_state.EE[3].J[-3:, :]))

    dynamicConsMatrix = np.hstack((self.robot_state.inertia_matrix[:6, :], -J_contact.T[:6, :]))  # 6*(N+12)
    dynamicConsVector = - self.robot_state.nonlinear_effects[:6]  # 6*1

    equalityConsMatrix = None
    equalityConsVector = None

    ##########################
    #  Solve the QP problem  #
    ##########################
    from qpsolvers import solve_qp
    P = A.T.dot(A)
    P = P + np.eye(P.shape[0]) * 1e-10
    X = solve_qp(P=P, q=-A.T.dot(b),
                 G=inequalityConsMatrix, h=inequalityConsVector,
                 A=equalityConsMatrix, b=equalityConsVector,
                 solver='quadprog')

    GRF = X[-GRF_dim:]
    # print("X.shape:", X.shape)
    # print("qdd.shape:", qdd.shape)
    # print("GRF.shape:", GRF.shape)
    # print("GRF:", GRF)

    ###################
    # Inverse Dynamic #
    ###################
    tau_N = - J_contact.T.dot(GRF)
    tau_n = tau_N[6:]

    # swing leg torque
    if high_level_cmd.contact_state is "leftSupport" or high_level_cmd.contact_state is "rightSupport":
        # gravity compensation for all legs
        gravity_compensation_torques = self.robot_state.nonlinear_effects[6:]
        tau_n = tau_n + gravity_compensation_torques

        # swing track PD torques
        Kp = 3000
        Kd = 10
        for swing_leg in swing_legs:
            swing_leg_full_Jacobian = self.robot_state.EE[swing_leg].J
            swing_leg_full_Jacobian_linear = swing_leg_full_Jacobian[3:6, :]
            swing_leg_Jacobian = self.robot_state.EE[swing_leg].J[3:6,
                                 6 + swing_leg * 3:6 + swing_leg * 3 + 3]
            swing_leg_Jdqd = self.robot_state.EE[swing_leg].Jdqd[3:]
            swing_leg_trajectory = self.swing_trajectories[swing_leg]
            swing_leg_PD_torques = swing_leg_Jacobian.T.dot(
                Kp * (swing_leg_trajectory.pos(step_time_past) - self.robot_state.EE[swing_leg].pos)
                + Kd * (swing_leg_trajectory.vel(step_time_past) - self.robot_state.EE[
                    swing_leg].linear_velocity))

            openrational_space_inertia_matrix = np.linalg.inv(
                swing_leg_full_Jacobian_linear.dot(self.robot_state.inertia_matrix).dot(
                    swing_leg_full_Jacobian_linear.T))
            swing_leg_acc_comp_torques = swing_leg_Jacobian.T.dot(openrational_space_inertia_matrix).dot(
                swing_leg_trajectory.acc(step_time_past) - swing_leg_Jdqd)

            tau_swing_leg = np.zeros_like(tau_n)
            tau_swing_leg[swing_leg * 3:swing_leg * 3 + 3] = swing_leg_PD_torques + swing_leg_acc_comp_torques
            tau_n += tau_swing_leg

    self.setJointTorques(tau_n)


def balance_with_trunk_control(self, loop_count):
    if self.ini_recorded is False:
        # self.ref_com_pos = (self.robot_state.lf_foot.pos + self.robot_state.rf_foot.pos + self.robot_state.lh_foot.pos + self.robot_state.rh_foot.pos)/4.0
        self.ref_com_pos = np.zeros(3)
        self.ref_com_pos[0] = 0.0
        self.ref_com_pos[1] = 0.0
        self.ref_com_pos[2] = 0.45
        self.ini_recorded = True

    print("self.robot_state.lf_foot.pos: ", self.robot_state.EE[0].pos)
    print("self.robot_state.rf_foot.pos: ", self.robot_state.EE[1].pos)
    print("self.ref_com_pos: ", self.ref_com_pos)
    print("self.robot_state.com : ", self.robot_state.com)

    from robots.utils.command import HighLevelCommand
    high_level_cmd = HighLevelCommand(contact_state="doubleSupport",
                                      com_pos=self.ref_com_pos)
    self.trunk_control(high_level_cmd, loop_count)


def raise_one_leg_with_trunk_control(self, swing_leg_index=0, com_height=0.45, raise_height=0.1):
    support_leg_indexes = [0, 1, 2, 3]
    support_leg_indexes.remove(swing_leg_index)

    print("support_leg_indexes: ", support_leg_indexes)

    if self.ini_recorded is False:
        # self.ref_com_pos = (self.robot_state.lf_foot.pos + self.robot_state.rf_foot.pos + self.robot_state.lh_foot.pos + self.robot_state.rh_foot.pos)/4.0
        self.ref_com_pos = (self.robot_state.EE[0].pos + self.robot_state.EE[1].pos + self.robot_state.EE[2].pos +
                            self.robot_state.EE[3].pos) / 4.0
        # self.ref_com_pos = (self.robot_state.EE[0].pos + self.robot_state.EE[3].pos) / 2.0
        # self.ref_com_pos[0] -= 0.02
        # self.ref_com_pos[1] -= 0.02
        self.ref_com_pos[2] = com_height
        self.ini_recorded = True

    # step state machine
    self.state_machine(loop_count)
    step_count, step_past_n, step_remain_n = self.state_machine(loop_count)
    support_state = self.state_machine.support_leg(loop_count)
    print(round(loop_count * time_step * 100) / 100, step_count, step_past_n, step_remain_n, support_state)

    from robots.utils.command import HighLevelCommand

    if step_count == 0:
        high_level_cmd = HighLevelCommand(contact_state="doubleSupport",
                                          com_pos=self.ref_com_pos)
        self.trunk_control(high_level_cmd, loop_count)
    else:
        if step_count == 1 and step_past_n == 0:  # plan a trajectory for rh foot at transition moment
            print("plan raise trajectory for once!")
            ini_pos = self.robot_state.EE[swing_leg_index].pos
            end_pos = ini_pos
            mid_pos = 0.5 * (ini_pos + end_pos)
            mid_pos[2] = raise_height
            from scipy.interpolate import CubicSpline
            self.swing_trajectory = CubicSpline([0.0, self.step_duration / 2.0, self.step_duration],
                                                np.array([ini_pos, mid_pos, end_pos]),
                                                axis=0, bc_type=((1, [0, 0, 0.0]), (1, [0, 0, 0])))

        high_level_cmd = HighLevelCommand(contact_state="doubleSupport",
                                          com_pos=self.ref_com_pos)

        # trunk_control
        num_of_contacts = len(support_leg_indexes)
        GRF_dim = num_of_contacts * 3

        J_contact = []
        for i in support_leg_indexes:
            J_contact.append(self.robot_state.EE[i].J[-3:, :])
        J_contact = np.vstack(J_contact)

        from robots.utils.task import Task
        # least square task: minimize qdd and GRFs
        ls_A = np.identity(GRF_dim)
        ls_b = np.zeros(GRF_dim)
        ls_task = Task(ls_A, ls_b)

        # com position task
        kp_linear = 100
        kd_linear = 10
        des_linear_momentum = self.robot_state.mass * (
                kp_linear * (high_level_cmd.com_pos - self.robot_state.com) +
                kd_linear * (high_level_cmd.com_vel_linear - self.robot_state.com_velocity) +
                high_level_cmd.com_acc_linear)
        linear_momentum_b = des_linear_momentum + self.robot_state.mass * np.array([0, 0, 9.81])

        linear_momentum_A = np.hstack([np.eye(3)] * num_of_contacts)
        linear_momentum_task = Task(linear_momentum_A, linear_momentum_b)

        # base orientation task
        from robots.utils.geometry import quaternionPD, rotationPD

        from scipy.linalg import block_diag
        I_base = block_diag(pybullet.getDynamicsInfo(self.id, -1)[2])

        R_base = self.robot_state.base.rotation_matrix
        I_G = R_base.dot(I_base).dot(R_base.T)
        base_orientation_b = quaternionPD(quat_des=high_level_cmd.com_quat,
                                          quat_cur=self.robot_state.base.quaternion,
                                          omega_des=np.zeros(3),
                                          omega_cur=self.robot_state.base.spatial_velocity[:3],
                                          kp=1000, kd=100)
        base_orientation_b = rotationPD(
            rot_des=np.eye(3),
            # rot_des=quaternion_matrix(high_level_cmd.com_quat)[:3,:3],
            rot_cur=self.robot_state.base.rotation_matrix,
            omega_des=np.zeros(3),
            omega_cur=self.robot_state.base.spatial_velocity[:3],
            kp=1000, kd=100)

        base_orientation_b = I_G.dot(base_orientation_b)

        skew = lambda V: np.array([[0, -V[2], V[1]], [V[2], 0, -V[0]], [-V[1], V[0], 0]])
        com_P_ee = []
        # for ee in self.robot_state.EE:
        #     com_P_ee.append(skew(ee.pos - self.robot_state.com_pos))
        for i in support_leg_indexes:
            com_P_ee.append(skew(self.robot_state.EE[i].pos - self.robot_state.com_pos))

        base_orientation_A = np.hstack(com_P_ee)
        base_orientation_task = Task(base_orientation_A, base_orientation_b)

        ls_task.set_weight(1)
        linear_momentum_task.set_weight(10)
        base_orientation_task.set_weight(10)
        tasks = [ls_task, linear_momentum_task, base_orientation_task]

        # combine all task A and b matrices
        A = np.empty((0, GRF_dim))
        b = np.empty(0)
        for task in tasks:
            A = np.vstack((A, task.w * task.A))
            b = np.hstack((b, task.w * task.b))

        ############################
        #  Inequality constraints  #
        ############################
        # friction constraints
        mu = 0.7
        frictionCons = np.array([[1, 0, -mu],
                                 [-1, 0, -mu],
                                 [0, 1, -mu],
                                 [0, -1, -mu]])

        # unilateral constraints
        unilateralCons = np.array([[0, 0, -1]])

        # GRF constraints: friction constraints + unilateral constraints
        forceCons = np.vstack((frictionCons,
                               unilateralCons))

        from scipy import linalg
        GRFCons = []
        for i in range(num_of_contacts):
            GRFCons = linalg.block_diag(GRFCons, forceCons)

        inequalityConsMatrix = GRFCons
        inequalityConsVector = np.zeros(GRFCons.shape[0])

        ########################
        # Equality constraints #
        # ########################
        # J_contact = np.vstack((self.robot_state.EE[0].J[-3:, :],
        #                        self.robot_state.EE[1].J[-3:, :],
        #                        self.robot_state.EE[2].J[-3:, :],
        #                        self.robot_state.EE[3].J[-3:, :]))
        #
        # dynamicConsMatrix = np.hstack((self.robot_state.inertia_matrix[:6, :], -J_contact.T[:6, :]))  # 6*(N+12)
        # dynamicConsVector = - self.robot_state.nonlinear_effects[:6]  # 6*1

        equalityConsMatrix = None
        equalityConsVector = None

        ##########################
        #  Solve the QP problem  #
        ##########################
        from qpsolvers import solve_qp
        X = solve_qp(P=A.T.dot(A), q=-A.T.dot(b),
                     G=inequalityConsMatrix, h=inequalityConsVector,
                     A=equalityConsMatrix, b=equalityConsVector,
                     solver='quadprog')

        GRF = X[-GRF_dim:]
        # print("X.shape:", X.shape)
        # print("qdd.shape:", qdd.shape)
        # print("GRF.shape:", GRF.shape)
        # print("GRF:", GRF)

        ###################
        # Inverse Dynamic #
        ###################
        tau_N = - J_contact.T.dot(GRF)
        tau_n = tau_N[6:]

        ## swing leg PD
        swing_leg_full_Jacobian = self.robot_state.EE[swing_leg_index].J
        swing_leg_full_Jacobian_linear = swing_leg_full_Jacobian[3:6, :]
        swing_leg_Jacobian = swing_leg_full_Jacobian_linear[:, 6 + swing_leg_index * 3:6 + swing_leg_index * 3 + 3]
        openrational_space_inertia_matrix = np.linalg.inv(
            swing_leg_full_Jacobian_linear.dot(self.robot_state.inertia_matrix).dot(swing_leg_full_Jacobian_linear.T))

        ## swing leg gravity compensation
        gravity_compensation_torques = self.robot_state.nonlinear_effects[6:]
        swing_leg_gravity_comp_torques = gravity_compensation_torques[swing_leg_index * 3:swing_leg_index * 3 + 3]

        ## swing leg acc compensation
        swing_leg_Jdqd = self.robot_state.EE[swing_leg_index].Jdqd[3:]
        swing_leg_acc_comp_torques = swing_leg_Jacobian.T.dot(openrational_space_inertia_matrix).dot(
            self.swing_trajectory(step_past_n * time_step, 2) - swing_leg_Jdqd)

        ## swing track PD torques
        Kp = 3000
        Kd = 10
        swing_leg_PD_torques = swing_leg_Jacobian.T.dot(
            Kp * (self.swing_trajectory(step_past_n * time_step) - self.robot_state.EE[swing_leg_index].pos)
            + Kd * (self.swing_trajectory(step_past_n * time_step, 1) - self.robot_state.EE[
                swing_leg_index].linear_velocity))

        tau_swing = np.zeros_like(tau_n)
        tau_swing[swing_leg_index * 3:swing_leg_index * 3 + 3] = swing_leg_PD_torques \
                                                                 + swing_leg_gravity_comp_torques \
                                                                 + swing_leg_acc_comp_torques
        # tau_swing[swing_leg_index*3:swing_leg_index*3+3] = swing_leg_PD_torques \
        #                                                    + swing_leg_gravity_comp_torques

        self.setJointTorques(tau_n + tau_swing)


def get_swing_support_leg_indexes(self, contact_state):
    all_legs = [0, 1, 2, 3]
    if contact_state is "leftSupport":
        support_legs = [0, 3]
    elif contact_state is "rightSupport":
        support_legs = [1, 2]
    elif contact_state is "doubleSupport":
        support_legs = all_legs

    # get swing leg index
    temp = all_legs.copy()
    for i in support_legs:
        temp.remove(i)
    swing_legs = temp

    return swing_legs, support_legs


def raise_two_leg_with_trunk_control(self, com_height=0.45, raise_height=0.05):
    # step state machine
    self.state_machine(loop_count)
    step_count, step_past_n, step_remain_n = self.state_machine(loop_count)
    support_state = self.state_machine.support_leg(loop_count)
    # print(round(loop_count*time_step*100)/100, step_count, step_past_n, step_remain_n, support_state)
    swing_legs, support_legs = self.get_swing_support_leg_indexes(support_state[1])

    if self.ini_recorded is False:
        self.ref_com_pos = (self.robot_state.EE[0].pos + self.robot_state.EE[1].pos + self.robot_state.EE[2].pos +
                            self.robot_state.EE[3].pos) / 4.0
        # self.ref_com_pos[0] -= 0.01
        # self.ref_com_pos[1] = -0.0
        self.ref_com_pos[2] = com_height
        self.ini_recorded = True

    from robots.utils.command import HighLevelCommand

    if step_count == 0:
        high_level_cmd = HighLevelCommand(contact_state="doubleSupport",
                                          com_pos=self.ref_com_pos)
        self.trunk_control(high_level_cmd, loop_count)
    else:
        if step_past_n == 0:

            support_leg_positions = []
            for support_leg in support_legs:
                support_leg_positions.append(self.robot_state.EE[support_leg].pos)
            self.ref_com_pos = np.mean(support_leg_positions, 0)
            self.ref_com_pos[2] = com_height

            from scipy.interpolate import CubicSpline
            from robots.utils.trajectory import SwingTrajectory
            self.swing_trajectories = {}
            for swing_leg in swing_legs:
                swing_trajectory = SwingTrajectory(self.robot_state.EE[swing_leg].pos,
                                                   self.robot_state.EE[swing_leg].pos,
                                                   mid_height=raise_height, duration=self.step_duration)
                self.swing_trajectories[swing_leg] = swing_trajectory

        high_level_cmd = HighLevelCommand(contact_state=support_state[1],
                                          com_pos=self.ref_com_pos)

        self.trunk_control(high_level_cmd, loop_count)


def trot_with_trunk_control(self, loop_count, des_walking_velocity=np.array([0.0, 0.0, 0.0]), com_height=0.48,
                            raise_height=0.05):
    step_count, step_past_n, step_remain_n, step_time_past, step_time_remain = self.state_machine(loop_count)
    support_state = self.state_machine.support_leg(loop_count)
    swing_legs, support_legs = self.get_swing_support_leg_indexes(support_state[1])
    print("step_count, step_past_n, step_remain_n: ", step_count, step_past_n, step_remain_n, support_state)

    from robots.utils.trajectory import SwingTrajectory
    from robots.utils.planner.LIPM import FootstepOptimizer

    if step_past_n == 0:  # plan a trajectory for swing feet at transition moment
        virtual_support_leg = support_state[0]

        support_leg_positions = []
        for support_leg in support_legs:
            support_leg_positions.append(self.robot_state.EE[support_leg].pos)
        virtual_support_foot_pos = np.mean(support_leg_positions, 0)

        swing_leg_positions = []
        for swing_leg in swing_legs:
            swing_leg_positions.append(self.robot_state.EE[swing_leg].pos)
        virtual_swing_ini_pos = np.mean(swing_leg_positions, 0)

        self.footstep_optmizer = FootstepOptimizer(ref_com_vel=des_walking_velocity,
                                                   cur_com_pos=self.robot_state.com_pos,
                                                   cur_com_vel=self.robot_state.com_vel,
                                                   cur_sup_foot=virtual_support_leg,
                                                   cur_sup_foot_pos=virtual_support_foot_pos,
                                                   remain_swing_duration=step_time_remain,
                                                   norminal_swing_duration=self.step_duration,
                                                   norminal_com_height=com_height,
                                                   optimize_steps=3,
                                                   inter_feet_distance=0.15,
                                                   g=self.gravity)
        footsteps = self.footstep_optmizer.plan_xyz()
        virtual_swing_end_pos = footsteps[0, :]
        virtual_swing_shift = virtual_swing_end_pos - virtual_swing_ini_pos
        virtual_swing_shift[2] = 0

        self.swing_trajectories = {}
        for swing_leg in swing_legs:
            ini_pos = self.robot_state.EE[swing_leg].pos
            swing_trajectory = SwingTrajectory(self.robot_state.EE[swing_leg].pos,
                                               virtual_swing_ini_pos + self.end_effector_home_pos_xy[
                                                   swing_leg] + virtual_swing_shift,
                                               mid_height=raise_height, duration=self.step_duration)
            self.swing_trajectories[swing_leg] = swing_trajectory

    from robots.utils.command import HighLevelCommand
    high_level_cmd = HighLevelCommand(contact_state=support_state[1],
                                      com_pos=self.footstep_optmizer.get_com_state(step_time_past)[0],
                                      com_vel_linear=self.footstep_optmizer.get_com_state(step_time_past)[1],
                                      com_acc_linear=self.footstep_optmizer.get_com_state(step_time_past)[2])

    self.trunk_control(high_level_cmd, loop_count)


