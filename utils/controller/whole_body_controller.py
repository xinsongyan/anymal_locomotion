import numpy as np
import scipy as sp
from utils.yaml_parser import load_yaml
from utils.hierarchical_quadratic_programming import HierarchicalQP
from utils.geometry import quaternionPD, positionPD, posePD, simplePD
from utils.task import Task

class WholeBodyController:
    def __init__(self, yaml_path):
        self.configs = load_yaml(yaml_path)
        self.kp_com = self.configs["controller_gains"]["com"]["kp"]
        self.kd_com = self.configs["controller_gains"]["com"]["kd"]
        self.kd_angular_momentum = self.configs["controller_gains"]["angular_momentum"]["kd"]
        self.kp_base_orientation = self.configs["controller_gains"]["base_orientation"]["kp"]
        self.kd_base_orientation = self.configs["controller_gains"]["base_orientation"]["kd"]
        self.end_effector_types = self.configs["end_effector_types"]
        self.hierachical_QP_solver = HierarchicalQP(slack_variable=self.configs["slack_variable"])

    def get_contact_jacobian(self, robot_state, support_legs):
        J_contact = []

        for support_leg in support_legs:
            if self.end_effector_types[support_leg] == "point":
                J_contact.append(robot_state.end_effector[support_leg].J_linear)
            elif self.end_effector_types[support_leg] == "line" or self.end_effector_types[support_leg] == "planar":
                for vetex_state in robot_state.end_effector_vertices_state[support_leg]:
                    J_contact.append(vetex_state.J_linear)
        J_contact = np.vstack(J_contact)
        return J_contact


    def inverse_dynamic_controller(self, robot_state, cmd):
        # cmd.show()

        lfoot_name, rfoot_name = self.configs['foot_names']

        num_of_contacts = 2
        J_contact = np.vstack((robot_state.end_effector[lfoot_name].J, robot_state.end_effector[rfoot_name].J))

        ls_A = np.identity(robot_state.N + num_of_contacts * 6)
        ls_b = np.zeros(robot_state.N + num_of_contacts * 6)
        ls_task = Task(ls_A, ls_b)

        # centroidal dynamic task
        kp_linear = 1000
        kp_angular = 10
        des_linear_momentum = robot_state.mass * (
                kp_linear * (cmd.com_pos - robot_state.com_pos) +
                2 * np.sqrt(kp_linear) * (cmd.com_vel - robot_state.com_vel) +
                cmd.com_acc)
        des_angular_momentum = - 2 * np.sqrt(kp_angular) * robot_state.angular_momentum

        # angular momentum task
        angular_momentum_A = np.hstack((robot_state.CMM[:3, :], np.zeros((3, num_of_contacts * 6))))
        angular_momentum_b = des_angular_momentum - robot_state.CMM_bias_force[:3]
        angular_momentum_task = Task(angular_momentum_A, angular_momentum_b)

        # linear momentum task
        linear_momentum_A = np.hstack((robot_state.CMM[-3:, :], np.zeros((3, num_of_contacts * 6))))
        linear_momentum_b = des_linear_momentum - robot_state.CMM_bias_force[-3:]
        linear_momentum_task = Task(linear_momentum_A, linear_momentum_b)

        # pelvis orientation task
        pelvis_orientation_A = np.hstack((robot_state.base.J[:3, :], np.zeros((3, num_of_contacts * 6))))
        pelvis_orientation_b = quaternionPD(quat_des=cmd.base_quat,
                                            quat_cur=robot_state.base.quaternion,
                                            omega_des=np.zeros(3),
                                            omega_cur=robot_state.base.angular_velocity,
                                            kp=1000, kd=2.0 * np.sqrt(500))
        pelvis_orientation_task = Task(pelvis_orientation_A, pelvis_orientation_b)

        ls_task.set_weight(1)  # = min_qdd + min_GRF
        pelvis_orientation_task.set_weight(10)
        linear_momentum_task.set_weight(30)
        angular_momentum_task.set_weight(10)
        tasks = [ls_task, linear_momentum_task, pelvis_orientation_task]

        # combine all task A and b matrices
        A = np.empty((0, robot_state.N + num_of_contacts * 6))
        b = np.empty(0)
        for task in tasks:
            A = np.vstack((A, task.weight * task.matrix))
            b = np.hstack((b, task.weight * task.vector))

        ############################
        #  Inequality constraints  #
        ############################
        lGRFCons = self.get_spatial_force_cons().dot(
            sp.linalg.block_diag(robot_state.end_effector[lfoot_name].rot.T, robot_state.end_effector[lfoot_name].rot.T))
        rGRFCons = self.get_spatial_force_cons().dot(
            sp.linalg.block_diag(robot_state.end_effector[rfoot_name].rot.T, robot_state.end_effector[rfoot_name].rot.T))
        GRFCons = np.vstack((np.hstack((self.get_spatial_force_cons(), np.zeros((9, 6)))),
                             np.hstack((np.zeros((9, 6)), self.get_spatial_force_cons()))))
        inequalityConsMatrix = np.hstack((np.zeros((18, robot_state.N)), GRFCons))
        inequalityConsVector = np.zeros(18)

        ########################
        # Equality constraints #
        ########################
        dynamicConsMatrix = np.hstack((robot_state.inertia_matrix[:6, :], -J_contact.T[:6, :]))  # 6*(N+12)
        dynamicConsVector = - robot_state.nonlinear_effects[:6]  # 6*1
        equalityConsMatrix = dynamicConsMatrix
        equalityConsVector = dynamicConsVector

        ##########################
        #  Solve the QP problem  #
        ##########################
        from qpsolvers import solve_qp
        X = solve_qp(P=A.T.dot(A), q=-A.T.dot(b),
                     G=inequalityConsMatrix, h=inequalityConsVector,
                     A=equalityConsMatrix, b=equalityConsVector,
                     solver='quadprog')
        # print("X.shape:", X.shape)
        qdd = X[:robot_state.N]
        GRF = X[robot_state.N:]

        np.set_printoptions(precision=4)
        print("GRF: ")
        print(GRF[:6])
        print(GRF[-6:])

        ###################
        # Inverse Dynamic #
        ###################
        tau_N = robot_state.inertia_matrix.dot(qdd) + robot_state.nonlinear_effects - J_contact.T.dot(GRF)
        tau_n = tau_N[6:]


        return dict(zip(robot_state.actuated_body_names, tau_n))


    def get_spatial_force_cons(self):
        foot_size = (0.227, 0.133887, 0.05)
        # foot_size = (0.2, 0.1, 0.015)

        # zmp constraints
        dx_min, dx_max = -foot_size[0] / 2.0, foot_size[0] / 2.0
        dy_min, dy_max = -foot_size[1] / 2.0, foot_size[1] / 2.0
        zmpConsSpatialForce = np.array([[-1, 0, 0, 0, 0, dy_min],
                                             [1, 0, 0, 0, 0, -dy_max],
                                             [0, 1, 0, 0, 0, dx_min],
                                             [0, -1, 0, 0, 0, -dx_max]])

        # friction constraints
        mu = 0.7
        frictionConsSpatialForce = np.array([[0, 0, 0, 1, 0, -mu],
                                                  [0, 0, 0, -1, 0, -mu],
                                                  [0, 0, 0, 0, 1, -mu],
                                                  [0, 0, 0, 0, -1, -mu]])

        # unilateral constraints
        unilateralConsSpatialForce = np.array([[0, 0, 0, 0, 0, -1]])

        # GRF constraints: zmp constraints + friction constraints + unilateral constraints
        SpatialForceCons = np.vstack((zmpConsSpatialForce,
                                           frictionConsSpatialForce,
                                           unilateralConsSpatialForce))

        return SpatialForceCons

    def compute_joint_torques(self, robot_state, high_level_cmd):


        num_of_supports = 0
        num_of_GRFs = 0
        for support_leg in high_level_cmd.support_legs:
            if self.end_effector_types[support_leg] == "point":
                num_of_supports += 1
                num_of_GRFs += 1
            elif self.end_effector_types[support_leg] == "line":
                num_of_supports += 1
                num_of_GRFs += 2
            elif self.end_effector_types[support_leg] == "planar":
                num_of_supports += 1
                num_of_GRFs += 4

        # print("num_of_supports", num_of_supports)
        # print("num_of_GRFs", num_of_GRFs)

        GRF_dim = num_of_GRFs * 3
        qdd_dim = robot_state.N
        x_dim = qdd_dim + GRF_dim


        # print("qdd_dim", qdd_dim, "GRF_dim", GRF_dim)

        J_contact = self.get_contact_jacobian(robot_state, high_level_cmd.support_legs)

        inequalityConsMatrix, inequalityConsVector = self.get_inequality_constraints(qdd_dim=qdd_dim, num_of_GRFs=num_of_GRFs)

        whole_body_control_task_hierarchy = high_level_cmd.task_hierarchy

        if "dynamic_cons_task" in whole_body_control_task_hierarchy.keys():
            dynamicConsMatrix, dynamicConsVector = self.get_dynamic_constraints(robot_state.inertia_matrix, robot_state.nonlinear_effects, J_contact)
            whole_body_control_task_hierarchy["dynamic_cons_task"]["A"] = dynamicConsMatrix
            whole_body_control_task_hierarchy["dynamic_cons_task"]["b"] = dynamicConsVector


        if "least_square_task" in whole_body_control_task_hierarchy.keys():
            least_square_task_A = np.eye(x_dim)
            least_square_task_b = np.zeros(x_dim)
            whole_body_control_task_hierarchy["least_square_task"]["A"] = least_square_task_A
            whole_body_control_task_hierarchy["least_square_task"]["b"] = least_square_task_b


        if "linear_momentum_task" in whole_body_control_task_hierarchy.keys():
            # linear momentum task
            des_linear_momentum = robot_state.mass * (
                    self.kp_com * (high_level_cmd.com_pos - robot_state.com_pos) +
                    self.kd_com * (high_level_cmd.com_vel - robot_state.com_vel) +
                                   high_level_cmd.com_acc)
            linear_momentum_A = np.hstack((robot_state.CMM_linear, np.zeros((3, GRF_dim))))
            linear_momentum_b = des_linear_momentum - robot_state.CMM_bias_force_linear
            whole_body_control_task_hierarchy["linear_momentum_task"]["A"] = linear_momentum_A
            whole_body_control_task_hierarchy["linear_momentum_task"]["b"] = linear_momentum_b


        if "angular_momentum_task" in whole_body_control_task_hierarchy.keys():
            # angular momentum task
            des_angular_momentum = - self.kd_angular_momentum * robot_state.angular_momentum
            angular_momentum_A = np.hstack((robot_state.CMM_angular, np.zeros((3, GRF_dim))))
            angular_momentum_b = des_angular_momentum - robot_state.CMM_bias_force_angular
            whole_body_control_task_hierarchy["angular_momentum_task"]["A"] = angular_momentum_A
            whole_body_control_task_hierarchy["angular_momentum_task"]["b"] = angular_momentum_b

        if "whole_body_config_task" in whole_body_control_task_hierarchy.keys():
            base_dim = 6
            config_dim = qdd_dim - base_dim
            whole_body_config_A = np.hstack((np.zeros((config_dim,base_dim)), np.eye(config_dim), np.zeros((config_dim, GRF_dim))))
            whole_body_config_b = simplePD(pos_des=high_level_cmd.whole_body_config,
                                           pos_cur=robot_state.q[base_dim:qdd_dim],
                                           vel_des=np.zeros(config_dim),
                                           vel_cur=robot_state.qd[base_dim:qdd_dim],
                                           acc_des=np.zeros(config_dim),
                                           kp=self.configs["controller_gains"]["whole_body_config_task"]["kp"],
                                           kd=self.configs["controller_gains"]["whole_body_config_task"]["kd"])
            whole_body_control_task_hierarchy["whole_body_config_task"]["A"] = whole_body_config_A
            whole_body_control_task_hierarchy["whole_body_config_task"]["b"] = whole_body_config_b

        if "arm_config_task" in whole_body_control_task_hierarchy.keys():
            base_dim = 6
            leg_dim = len(self.configs["leg_home_config"])
            arm_dim = len(self.configs["arm_home_config"])

            arm_config_A = np.hstack((np.zeros((arm_dim, base_dim)), np.zeros((arm_dim, leg_dim)), np.eye(arm_dim), np.zeros((arm_dim, GRF_dim))))
            arm_config_b = simplePD(pos_des=self.configs["arm_home_config"],
                                           pos_cur=robot_state.q[base_dim+leg_dim:base_dim+leg_dim+arm_dim],
                                           vel_des=np.zeros(arm_dim),
                                           vel_cur=robot_state.qd[base_dim+leg_dim:base_dim+leg_dim+arm_dim],
                                           acc_des=np.zeros(arm_dim),
                                           kp=self.configs["controller_gains"]["arm_config_task"]["kp"],
                                           kd=self.configs["controller_gains"]["arm_config_task"]["kd"])

            whole_body_control_task_hierarchy["arm_config_task"]["A"] = arm_config_A
            whole_body_control_task_hierarchy["arm_config_task"]["b"] = arm_config_b

        if "torso_orientation_task" in whole_body_control_task_hierarchy.keys():
            torso_orientation_A = np.hstack((robot_state.torso.J_angular, np.zeros((3, GRF_dim))))
            torso_orientation_b = quaternionPD(quat_des=np.array([0, 0, 0, 1]),
                                              quat_cur=robot_state.torso.quaternion,
                                              omega_des=np.zeros(3),
                                              omega_cur=robot_state.torso.angular_velocity,
                                              kp=self.configs["controller_gains"]["torso_orientation"]["kp"],
                                              kd=self.configs["controller_gains"]["torso_orientation"]["kd"])
            whole_body_control_task_hierarchy["torso_orientation_task"]["A"] = torso_orientation_A
            whole_body_control_task_hierarchy["torso_orientation_task"]["b"] = torso_orientation_b

        if "base_orientation_task" in whole_body_control_task_hierarchy.keys():
            # base orientation task
            base_orientation_A = np.hstack((robot_state.base.J_angular, np.zeros((3, GRF_dim))))
            base_orientation_b = quaternionPD(quat_des=high_level_cmd.base_quat,
                                              quat_cur=robot_state.base.quaternion,
                                              omega_des=np.zeros(3),
                                              omega_cur=robot_state.base.angular_velocity,
                                              kp=self.kp_base_orientation,
                                              kd=self.kd_base_orientation)
            whole_body_control_task_hierarchy["base_orientation_task"]["A"] = base_orientation_A
            whole_body_control_task_hierarchy["base_orientation_task"]["b"] = base_orientation_b

        if "swing_leg_tasks" in whole_body_control_task_hierarchy.keys():
            swing_leg_A_all = []
            swing_leg_b_all = []
            for swing_leg in high_level_cmd.swing_legs:
                if self.end_effector_types[swing_leg] == "point":
                    swing_leg_acc = positionPD(pos_des=high_level_cmd.swing_leg_positions[swing_leg]['pos'], pos_cur=robot_state.end_effector[swing_leg].position,
                                               vel_des=high_level_cmd.swing_leg_positions[swing_leg]['vel'], vel_cur=robot_state.end_effector[swing_leg].linear_velocity,
                                               acc_des=high_level_cmd.swing_leg_positions[swing_leg]['acc'],
                                               kp=self.configs["controller_gains"]["swing_leg_position"]["kp"],
                                               kd=self.configs["controller_gains"]["swing_leg_position"]["kd"])
                    swing_leg_A = np.hstack((robot_state.end_effector[swing_leg].J_linear, np.zeros((3, GRF_dim))))
                    swing_leg_b = swing_leg_acc - robot_state.end_effector[swing_leg].Jdqd_linear
                    swing_leg_A_all.append(swing_leg_A)
                    swing_leg_b_all.append(swing_leg_b)
                elif self.end_effector_types[swing_leg] == "line" or self.end_effector_types[swing_leg] == "planar":
                    swing_leg_pos_acc = positionPD(pos_des=high_level_cmd.swing_leg_positions[swing_leg]['pos'],
                                                   pos_cur=robot_state.end_effector[swing_leg].position,
                                                   vel_des=high_level_cmd.swing_leg_positions[swing_leg]['vel'],
                                                   vel_cur=robot_state.end_effector[swing_leg].linear_velocity,
                                                   acc_des=high_level_cmd.swing_leg_positions[swing_leg]['acc'],
                                                   kp=self.configs["controller_gains"]["swing_leg_position"]["kp"],
                                                   kd=self.configs["controller_gains"]["swing_leg_position"]["kd"])
                    swing_leg_ori_acc = quaternionPD(quat_des=high_level_cmd.swing_leg_quaternions[swing_leg],
                                                     quat_cur=robot_state.end_effector[swing_leg].quaternion,
                                                     omega_des=np.zeros(3),
                                                     omega_cur=robot_state.end_effector[swing_leg].angular_velocity,
                                                     kp=self.configs["controller_gains"]["swing_leg_orientation"][
                                                         "kp"],
                                                     kd=self.configs["controller_gains"]["swing_leg_orientation"][
                                                         "kd"])
                    swing_leg_acc = np.hstack((swing_leg_ori_acc, swing_leg_pos_acc))
                    swing_leg_A = np.hstack((robot_state.end_effector[swing_leg].J, np.zeros((6, GRF_dim))))
                    swing_leg_b = swing_leg_acc - robot_state.end_effector[swing_leg].Jdqd
                    swing_leg_A_all.append(swing_leg_A)
                    swing_leg_b_all.append(swing_leg_b)
            whole_body_control_task_hierarchy["swing_leg_tasks"]["A"] = np.vstack(swing_leg_A_all)
            whole_body_control_task_hierarchy["swing_leg_tasks"]["b"] = np.hstack(swing_leg_b_all)




        x = self.hierachical_QP_solver.solve(x_dim=x_dim,
                                             task_hierarchy=whole_body_control_task_hierarchy,
                                             ineq_cons_matrix=inequalityConsMatrix,
                                             ineq_cons_vector=inequalityConsVector)


        qdd, GRF = x[:qdd_dim], x[-GRF_dim:]

        ###################
        # Inverse Dynamic #
        ###################
        tau_N = robot_state.inertia_matrix.dot(qdd) + robot_state.nonlinear_effects - J_contact.T.dot(GRF)
        tau_n = tau_N[6:]

        return dict(zip(robot_state.actuated_body_names, tau_n))


    def get_dynamic_constraints(self, inertia_matrix, nonlinear_effects, contact_jacobian):
        dynamicConsMatrix = np.hstack((inertia_matrix[:6, :], -contact_jacobian.T[:6, :]))  # 6*(N+12)
        dynamicConsVector = - nonlinear_effects[:6]  # 6*1
        return (dynamicConsMatrix, dynamicConsVector)

    def get_equality_constraints(self, inertia_matrix, nonlinear_effects, contact_jacobian):
        return self.get_dynamic_constraints(inertia_matrix, nonlinear_effects, contact_jacobian)

    def get_inequality_constraints(self, qdd_dim, num_of_GRFs):
        # friction constraints
        mu = 0.7
        frictionCons = np.array([[1, 0, -mu],
                                 [-1, 0, -mu],
                                 [0, 1, -mu],
                                 [0, -1, -mu]])

        # unilateral constraints
        unilateralCons = np.array([[0, 0, -1]])

        # GRF constraints: friction constraints + unilateral constraints
        forceCons = np.vstack((frictionCons, unilateralCons))

        from scipy.linalg import block_diag
        GRFCons = []
        for i in range(num_of_GRFs):
            GRFCons = block_diag(GRFCons, forceCons)

        inequalityConsMatrix = np.hstack((np.zeros((GRFCons.shape[0], qdd_dim)), GRFCons))
        inequalityConsVector = np.zeros(GRFCons.shape[0])

        return (inequalityConsMatrix, inequalityConsVector)

