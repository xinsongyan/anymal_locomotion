import pybullet
import numpy as np
import rbdl
from utils.yaml_parser import load_yaml
from utils.urdf_parser_py.urdf import URDF


class Robot:
    def __init__(self, robot_config_path):
        self.robot_configs = load_yaml(robot_config_path)


        self.gravity = self.robot_configs["gravity"]

        print("pybullet load robot from:", self.robot_configs["urdf_path"], "<"*100)
        self.id = pybullet.loadURDF(fileName=self.robot_configs["urdf_path"],
                                    basePosition=self.robot_configs["robot_ini_pos"],
                                    baseOrientation=pybullet.getQuaternionFromEuler(self.robot_configs["robot_ini_rpy"]),
                                    useFixedBase=self.robot_configs["fixed_base"],
                                    # flags=pybullet.URDF_USE_SELF_COLLISION or pybullet.URDF_USE_SELF_COLLISION_EXCLUDE_ALL_PARENTS
                                    flags=pybullet.URDF_USE_INERTIA_FROM_FILE
                                    )
        print("Robot loaded! id=", self.id, "<"*100)

        self.urdf_model = URDF.from_xml_file(self.robot_configs["urdf_path"])

        self.robot_info = self.getRobotInfo()


        self.joint_ids = self.robot_info["joint_ids"]
        self.joint_names = self.robot_info["joint_names"]
        self.link_ids = self.robot_info["link_ids"]
        self.link_names = self.robot_info["link_names"]
        self.num_of_joints = self.robot_info["num_of_joints"]
        self.num_of_actuated_joints = self.robot_info["num_of_actuated_joints"]

        self.link_name_id_map = dict(zip(self.link_names, self.link_ids))
        print("self.link_name_id_map: ", self.link_name_id_map)




        # self.updateRobotDynamics()

        self.homing(self.robot_configs["homing_config"])

        self.addDebugParameter()





        self.end_effector_home_pos_xy = self.robot_configs['end_effector_home_pos_xy']
        self.end_effector_names = list(self.robot_configs['end_effector_tips'].keys())
        self.end_effector_offsets = list(self.robot_configs['end_effector_tips'].values())
        print("end_effector_names:", self.end_effector_names)
        print("end_effector_offsets:", self.end_effector_offsets)



        self.updateRobotDynamics()
        # self.enable_torque_control()
        self.torque_control_enabled = None
        self.ini_recorded = False
        self.debug_enable = self.robot_configs["debug"]["enable"]


    def addDebugParameter(self):
        self.debug_param_ids = []
        for joint_name, joint_lower_limit, joint_upper_limit, joint_home_pos in zip(self.robot_info["joint_names"], self.robot_info["joint_lower_limits"], self.robot_info["joint_upper_limits"], self.robot_configs["homing_config"]):
            self.debug_param_ids.append(pybullet.addUserDebugParameter(joint_name, joint_lower_limit, joint_upper_limit, joint_home_pos))

    def removeDebugParameter(self):
        for debug_param_id in self.debug_param_ids:
            pybullet.removeUserDebugItem(debug_param_id)

    def readDebugParameter(self):
        debug_param_values = []
        for debug_param_id in self.debug_param_ids:
            debug_param_values.append(pybullet.readUserDebugParameter(debug_param_id))
        return debug_param_values

    def printRawRobotInfor(self):
        print("printRawRobotInfor", "<"*100)

        print("joint_info:")
        print("joint_index, joint_name, joint_type")
        joint_type_dict = {0:"REVOLUTE", 1:"PRISMATIC", 2:"SPHERICAL", 3:"PLANAR", 4:"FIXED"}
        for j in range(pybullet.getNumJoints(self.id)):
            joint_info = pybullet.getJointInfo(self.id, j)
            joint_index = joint_info[0]
            joint_name = joint_info[1]
            joint_type = joint_info[2]
            print(joint_index, joint_name, joint_type_dict[joint_type])
            print(joint_info)

        print("link_info:")
        print("linkIndex, mass, lateral_friction, local inertia diagonal")
        base_dynamics = pybullet.getDynamicsInfo(self.id, -1)
        print(-1, base_dynamics[0], base_dynamics[1], base_dynamics[2])
        print(base_dynamics)
        for j in range(pybullet.getNumJoints(self.id)):
            link_dynamics = pybullet.getDynamicsInfo(self.id, j)
            print(j, link_dynamics[0], link_dynamics[1], link_dynamics[2])
            print(link_dynamics)

    def updateRobotDynamics(self):
        # change lateral friction
        for j in range(-1,pybullet.getNumJoints(self.id)):
            pybullet.changeDynamics(bodyUniqueId=self.id, linkIndex=j, lateralFriction=1.0, spinningFriction=1.0, rollingFriction=0.0)

        print("updated link dynamics:")
        link_dynamics = pybullet.getDynamicsInfo(self.id, -1)
        print(-1, link_dynamics)
        for j in range(pybullet.getNumJoints(self.id)):
            link_dynamics = pybullet.getDynamicsInfo(self.id, j)
            print(j, link_dynamics)

    def homing(self, home_config=None):
        for index, angle in zip(self.joint_ids, home_config):
            pybullet.resetJointState(self.id, index, angle, 0.0)



    def enable_torque_control(self):
        # disable motor in order to use direct torque control
        pybullet.setJointMotorControlArray(self.id, self.joint_ids, pybullet.VELOCITY_CONTROL, forces=[0.0]*len(self.joint_ids))
        self.torque_control_enabled = True
        print("torque control enabled!")

    # get param
    def getRobotName(self):
        return pybullet.getBodyInfo(self.id)[1].decode()

    def getBaseLinkName(self):
        return pybullet.getBodyInfo(self.id)[0].decode()

    def getRobotInfo(self, verbose=True):
        robot_info = {}
        robot_info["robot_name"] = pybullet.getBodyInfo(self.id)[1].decode()
        robot_info["base_link_name"] = pybullet.getBodyInfo(self.id)[0].decode()
        robot_info["num_of_joints"] = pybullet.getNumJoints(self.id)
        joint_ids, joint_names = [], []
        link_ids, link_names = [], []
        joint_lower_limits, joint_upper_limits = [], []
        for joint_id in range(pybullet.getNumJoints(self.id)):
            joint_info = pybullet.getJointInfo(self.id, joint_id)
            print(joint_info)
            link_id = joint_info[0]
            joint_name = joint_info[1]
            joint_type = joint_info[2]
            joint_lower_limit = joint_info[8]
            joint_upper_limit = joint_info[9]
            link_name = joint_info[12]
            if (joint_type == pybullet.JOINT_PRISMATIC or joint_type == pybullet.JOINT_REVOLUTE):
                joint_ids.append(joint_id)
                joint_names.append(joint_name.decode())
                link_ids.append(link_id)
                link_names.append(link_name.decode())
                joint_lower_limits.append(joint_lower_limit)
                joint_upper_limits.append(joint_upper_limit)
        robot_info["num_of_actuated_joints"] = len(joint_ids)
        robot_info["joint_ids"] = joint_ids
        robot_info["joint_names"] = joint_names
        robot_info["joint_lower_limits"] = joint_lower_limits
        robot_info["joint_upper_limits"] = joint_upper_limits
        robot_info["link_ids"] = link_ids
        robot_info["link_names"] = link_names
        if verbose:
            print("-"*100)
            print("Robot information extracted from pybullet simulator:")
            for key in robot_info:
                print(key,":",robot_info[key])
            print("-" * 100)
        return robot_info





    def getBaseCoMPosition(self):
        return np.array(pybullet.getBasePositionAndOrientation(self.id)[0])

    def getBaseCoMQuaternion(self):
        return np.array(pybullet.getBasePositionAndOrientation(self.id)[1])

    def getBaseLinkPosition(self):
        base_com_pos = self.getBaseCoMPosition()
        base_com_quat = self.getBaseCoMQuaternion()
        base_com_pose = np.concatenate((base_com_pos, base_com_quat))
        from utils.geometry import pose2transform, homogeneous_vector
        base_com_trans = pose2transform(base_com_pose)
        base_link_com_offset = np.array(pybullet.getDynamicsInfo(self.id, -1)[3])
        base_link_pos = base_com_trans.dot(homogeneous_vector(-base_link_com_offset))[:3]
        return base_link_pos

    def getBaseLinkQuaternion(self):
        return np.array(pybullet.getBasePositionAndOrientation(self.id)[1])

    def getBaseVelocityLinear(self):
        return pybullet.getBaseVelocity(self.id)[0]

    def getBaseVelocityAngular(self):
        return pybullet.getBaseVelocity(self.id)[1]

    def getBaseTwist(self):
        return np.concatenate((self.getBaseVelocityLinear(), self.getBaseVelocityAngular()))

    def getBaseSpatialVelocity(self):
        return np.concatenate((self.getBaseVelocityAngular(), self.getBaseVelocityLinear()))

    # def getJointPositions(self, jointId=None):
    #     """
    #     Get the position of the given joint(s).
    #
    #     Args:
    #         jointId (int, int[N], None): joint id, or list of joint ids. If None, get the position of all (actuated)
    #             joints.
    #
    #     Returns:
    #         if 1 joint:
    #             float: joint position [rad]
    #         if multiple joints:
    #             np.float[N]: joint positions [rad]
    #     """
    #     if isinstance(jointId, int):
    #         return pybullet.getJointState(self.id, jointId)[0]
    #     if jointId is None:
    #         jointId = self.joint_ids
    #     return np.array([state[0] for state in pybullet.getJointStates(self.id, jointId)])

    def getActuatedJointPositions(self):
        joint_positions = np.array([state[0] for state in pybullet.getJointStates(self.id, self.joint_ids)])
        return dict(zip(self.link_names, joint_positions))

    # def getJointVelocities(self, jointId=None):
    #     """
    #     Get the velocity of the given joint(s).
    #
    #     Args:
    #         jointId (int, int[N], None): joint id, or list of joint ids. If None, get the velocity of all (actuated)
    #             joints.
    #
    #     Returns:
    #         if 1 joint:
    #             float: joint velocity [rad/s]
    #         if multiple joints:
    #             np.float[N]: joint velocities [rad/s]
    #     """
    #     if isinstance(jointId, int):
    #         return pybullet.getJointState(self.id, jointId)[1]
    #     if jointId is None:
    #         jointId = self.joint_ids
    #     return np.array([state[1] for state in pybullet.getJointStates(self.id, jointId)])

    def getActuatedJointVelocities(self):
        joint_velocities = np.array([state[1] for state in pybullet.getJointStates(self.id, self.joint_ids)])
        return dict(zip(self.link_names, joint_velocities))


    def getObservation(self):
        return {"base_position": self.getBaseLinkPosition(),
                "base_quaternion": self.getBaseLinkQuaternion(),
                "base_velocity_linear": self.getBaseVelocityLinear(),
                "base_velocity_angular": self.getBaseVelocityAngular(),
                "joint_positions": self.getJointPositions(),
                "joint_velocities": self.getJointVelocities(),
                "joint_names": self.joint_names}




    # set control command
    def setJointPosition(self, position=None, jointId=None):
        pybullet.setJointMotorControl2(self.id, jointId, pybullet.POSITION_CONTROL, position)

    def setJointPositions(self, positions=None, jointIds=None):
        if jointIds is None:
            jointIds = self.joint_ids
        pybullet.setJointMotorControlArray(self.id, jointIds, pybullet.POSITION_CONTROL, positions)

    def setJointTorque(self, torque=None, jointId=None):
        pybullet.setJointMotorControl2(self.id, jointId, pybullet.TORQUE_CONTROL, force=torque)

    def setJointTorques(self, torques=None, jointIds=None):
        if jointIds is None:
            jointIds = self.joint_ids
        pybullet.setJointMotorControlArray(self.id, jointIds, pybullet.TORQUE_CONTROL, forces=torques)

    def setActuatedJointTorques(self, link_name_torque_dict):
        if self.torque_control_enabled is None:
            self.enable_torque_control()
        torques = [link_name_torque_dict[link_name] for link_name in self.link_names]
        pybullet.setJointMotorControlArray(self.id, self.joint_ids, pybullet.TORQUE_CONTROL, forces=torques)


    # def setJointPositions(self, position=None, jointId=None, torque_limit=None):
    #     """
    #     Set the position to the given joint(s).
    #
    #     Args:
    #         jointId (int, int[N], None): joint id, or list of joint ids. If None, it will set the joint positions to
    #             all (actuated) joints.
    #         position (float, float[N], None): desired position(s) for joint(s) [N]. If None, it will set 0 to the given joint(s).
    #
    #     Returns:
    #         None
    #     """
    #     if isinstance(jointId, int):
    #         if position is None:
    #             position = 0
    #         pybullet.setJointMotorControl2(self.id, jointId, pybullet.POSITION_CONTROL, position)#, force=torque_limit)
    #
    #     else:
    #         if jointId is None:
    #             jointId = self.joint_ids
    #         if position is None:
    #             position = [0] * len(self.joint_ids)
    #         elif isinstance(position, (int, float)):
    #             position = [position] * len(self.joint_ids)
    #         pybullet.setJointMotorControlArray(self.id, jointId, pybullet.POSITION_CONTROL, position)#, forces=[torque_limit]*len(jointId))


    # def setJointTorques(self, torque=None, jointId=None):
    #     """
    #     Set the torque to the given joint(s) (using force/torque control).
    #
    #     Args:
    #         jointId (int, int[N], None): joint id, or list of joint ids. If None, it will set the joint torques to
    #             all (actuated) joints.
    #         torque (float, float[N], None): desired torque(s) to apply to the joint(s) [N]. If None, it will apply
    #             a torque of 0 to the given joint(s).
    #
    #     Returns:
    #         None
    #     """
    #     if isinstance(jointId, int):
    #         if torque is None:
    #             torque = 0
    #         pybullet.setJointMotorControl2(self.id, jointId, pybullet.TORQUE_CONTROL, force=torque)
    #     else:
    #         if jointId is None:
    #             jointId = self.joint_ids
    #         if torque is None:
    #             torque = [0]*len(self.joint_ids)
    #         elif isinstance(torque, (int, float)):
    #             torque = [torque]*len(self.joint_ids)
    #         pybullet.setJointMotorControlArray(self.id, jointId, pybullet.TORQUE_CONTROL, forces=torque)




    def print_joint_info(self):
        for j in range(pybullet.getNumJoints(self.id)):
            print(pybullet.getJointInfo(self.id, j))


    def parse_joint_info(self):
        self.jointIds = []
        self.paramIds = []
        self.jointOffsets = []
        self.jointDirections = [-1, 1, 1, 1, 1, 1, -1, 1, 1, 1, 1, 1]
        self.jointAngles = [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0]

        for i in range(4):
            self.jointOffsets.append(0)
            self.jointOffsets.append(-0.7)
            self.jointOffsets.append(0.7)

        self.maxForceId = pybullet.addUserDebugParameter("maxForce", 0, 100, 20)

        for j in range(pybullet.getNumJoints(self.id)):
            pybullet.changeDynamics(self.id, j, linearDamping=0, angularDamping=0)
            info = pybullet.getJointInfo(self.id, j)
            # print(info)
            jointName = info[1]
            jointType = info[2]
            if (jointType == pybullet.JOINT_PRISMATIC or jointType == pybullet.JOINT_REVOLUTE):
                self.jointIds.append(j)


    def enable_collision_between_lower_legs(self):
        # 2,5,8 and 11 are the lower legs
        self.lower_legs = [2, 5, 8, 11]
        for l0 in self.lower_legs:
            for l1 in self.lower_legs:
                if (l1 > l0):
                    enableCollision = 1
                    print("collision for pair", l0, l1, pybullet.getJointInfo(self.id, l0)[12],
                          pybullet.getJointInfo(self.id, l1)[12],
                          "enabled=", enableCollision)
                    pybullet.setCollisionFilterPair(self.id, self.id, 2, 5, enableCollision)






    def homing_control(self):
        homing_config = self.robot_configs["homing_config"]

        self.setJointPositions(homing_config)

    def debug_control(self):
        homing_config = self.readDebugParameter()
        self.setJointPositions(homing_config)

    def chain_ik(self, base_link, end_link, pos, rpy, q_guess):
        import utils.transformations as tf
        from utils.pykdl_utils.kdl_kinematics import KDLKinematics
        kinematics = KDLKinematics(urdf=self.urdf_model, base_link=base_link, end_link=end_link)
        transformation = tf.euler_matrix(rpy[0], rpy[1], rpy[2], 'sxyz').dot(tf.translation_matrix(pos))
        q = kinematics.inverse(transformation, q_guess=q_guess)
        return q

    def inverse_kinematic_control(self):
        joint_positions = self.robot_configs["homing_config"]


        # lhand_pos, lhand_rpy = [0.6, 0.2,  0.2], [0.0, 0.0,  1.57]
        # rhand_pos, rhand_rpy = [0.14, -0.78,  0.2], np.deg2rad([0, 0, 0])
        # larm_q = self.chain_ik(base_link="utorso", end_link="l_hand", pos=lhand_pos, rpy=lhand_rpy)
        # rarm_q = self.chain_ik(base_link="utorso", end_link="r_hand", pos=rhand_pos, rpy=rhand_rpy)
        # joint_positions[3:3+7] = larm_q
        # joint_positions[3+7:3+7+7] = rarm_q


        lfoot_pos, lfoot_rpy = [-0.0,  0.15, -1], np.deg2rad([0, 0, 0])
        rfoot_pos, rfoot_rpy = [-0.0, -0.15, -1], np.deg2rad([0, 0, 0])
        lleg_q_guess = np.array([2.72670334e-02, -9.02065960e-03, -5.08771857e-01,  8.43414962e-01, -3.34639467e-01,  9.01986114e-03])
        rleg_q_guess = np.array([6.87330720e-03, -9.09410209e-05, -5.02649076e-01,  8.44101532e-01, -3.41448953e-01,  9.17156084e-05])
        lleg_q = self.chain_ik(base_link="pelvis", end_link="leftFoot", pos=lfoot_pos, rpy=lfoot_rpy, q_guess=lleg_q_guess)
        rleg_q = self.chain_ik(base_link="pelvis", end_link="rightFoot", pos=rfoot_pos, rpy=rfoot_rpy, q_guess=rleg_q_guess)

        print("lleg_q: ", lleg_q)
        print("rleg_q: ", rleg_q)

        joint_positions[:6] = lleg_q
        joint_positions[6:12] = rleg_q

        self.setJointPositions(joint_positions)

    def zero_torque_control(self):
        if self.torque_control_enabled is None:
            self.enable_torque_control()
        zero_torques = np.zeros(self.num_of_actuated_joints)
        self.setJointTorques(zero_torques)

    def gravity_compensation_control(self, state_estimator):
        if self.torque_control_enabled is None:
            self.enable_torque_control()
        gravity_compensation_torques = state_estimator.nonlinear_effects[6:]
        self.setJointTorques(gravity_compensation_torques)

    def gravity_compensation_PD(self, state_estimator):
        if self.torque_control_enabled is None:
            self.enable_torque_control()

        gravity_compensation_torques = state_estimator.nonlinear_effects[6:]

        des_pos = self.robot_configs["homing_config"]
        des_vel = np.zeros_like(des_pos)
        cur_pos_dict = self.getActuatedJointPositions()
        cur_pos = [cur_pos_dict[link_name] for link_name in self.link_names]
        cur_pos = np.array(cur_pos)

        cur_vel_dict = self.getActuatedJointVelocities()
        cur_vel = [cur_vel_dict[link_name] for link_name in self.link_names]
        cur_vel = np.array(cur_vel)

        Kp = 1000
        Kd = 1

        PD_torques = Kp*(des_pos-cur_pos) - Kd*cur_vel

        cmd_torques = gravity_compensation_torques + PD_torques

        self.setJointTorques(cmd_torques)



    def step(self, loop_count):



        self.homing_control()
        # self.zero_torque_control()
        # self.gravity_compensation_control()
        # self.gravity_compensation_PD()
        # self.balance_with_trunk_control(loop_count)
        # self.raise_one_leg_with_trunk_control(swing_leg_index=0)
        # self.raise_two_leg_with_trunk_control()
        # self.trot_with_trunk_control(loop_count)
        # self.balance_control()
        # self.trot_in_place()
        # self.raise_two_leg()
        # self.trot()
        # self.pace()
        # self.bound()



    def debug_apply_external_force(self, keys, force=1000):

        if pybullet.B3G_UP_ARROW in keys and keys[pybullet.B3G_UP_ARROW] & pybullet.KEY_WAS_TRIGGERED:
            print("upper key was triggered!")
            pybullet.applyExternalForce(self.id, -1, forceObj=[force, 0, 0], posObj=[0, 0, 0],
                                        flags=pybullet.LINK_FRAME)
        if pybullet.B3G_DOWN_ARROW in keys and keys[pybullet.B3G_DOWN_ARROW] & pybullet.KEY_WAS_TRIGGERED:
            print("down key was triggered!")
            pybullet.applyExternalForce(self.id, -1, forceObj=[-force, 0, 0], posObj=[0, 0, 0],
                                        flags=pybullet.LINK_FRAME)
        if pybullet.B3G_LEFT_ARROW in keys and keys[pybullet.B3G_LEFT_ARROW] & pybullet.KEY_WAS_TRIGGERED:
            print("left key was triggered!")
            pybullet.applyExternalForce(self.id, -1, forceObj=[0, force, 0], posObj=[0, 0, 0],
                                        flags=pybullet.LINK_FRAME)
        if pybullet.B3G_RIGHT_ARROW in keys and keys[pybullet.B3G_RIGHT_ARROW] & pybullet.KEY_WAS_TRIGGERED:
            print("right key was triggered!")
            pybullet.applyExternalForce(self.id, -1, forceObj=[0, -force, 0], posObj=[0, 0, 0],
                                        flags=pybullet.LINK_FRAME)



    def debug(self, keys):
        self.debug_apply_external_force(keys, force=1000)
        # print(keys, keys.keys(), keys.values())



        # if options["key_frame"] is True:
        #     # plot com frame
        #     line_length = 0.1
        #     line_width = 1
        #     red = [1, 0, 0]
        #     green = [0, 1, 0]
        #     blue = [0, 0, 1]
        #
        #     frame_pos = self.robot.robot_state.com_pos
        #     frame_rot = self.robot.robot_state.base.rotation_matrix
        #     frame_life_time = 0.2
        #     pybullet.addUserDebugLine(frame_pos, frame_pos + line_length * frame_rot[:, 0],
        #                               lineColorRGB=red,
        #                               lineWidth=line_width,
        #                               lifeTime=frame_life_time)
        #     pybullet.addUserDebugLine(frame_pos, frame_pos + line_length * frame_rot[:, 1],
        #                               lineColorRGB=green,
        #                               lineWidth=line_width,
        #                               lifeTime=frame_life_time)
        #     pybullet.addUserDebugLine(frame_pos, frame_pos + line_length * frame_rot[:, 2],
        #                               lineColorRGB=blue,
        #                               lineWidth=line_width,
        #                               lifeTime=frame_life_time)
        #
        # if options["plot_trails"] is True:
        #     if loop_count % 8 == 0:
        #         com_pos = self.robot.robot_state.com_pos
        #         lf_foot_pos = self.robot.robot_state.EE[0].pos
        #         lh_foot_pos = self.robot.robot_state.EE[1].pos
        #         rf_foot_pos = self.robot.robot_state.EE[2].pos
        #         rh_foot_pos = self.robot.robot_state.EE[3].pos
        #
        #         line_width = 2
        #         trail_duration = 0
        #         com_trail_color = [1, 0, 0]
        #
        #         gait = "trot"
        #         # gait = "pace"
        #         # gait = "bound"
        #         if gait is "trot":
        #             lf_foot_trail_color = [0, 1, 0]
        #             lh_foot_trail_color = [0, 0, 1]
        #             rf_foot_trail_color = [0, 0, 1]
        #             rh_foot_trail_color = [0, 1, 0]
        #         elif gait is "pace":
        #             lf_foot_trail_color = [0, 1, 0]
        #             lh_foot_trail_color = [0, 1, 0]
        #             rf_foot_trail_color = [0, 0, 1]
        #             rh_foot_trail_color = [0, 0, 1]
        #         elif gait is "bound":
        #             lf_foot_trail_color = [0, 1, 0]
        #             lh_foot_trail_color = [0, 0, 1]
        #             rf_foot_trail_color = [0, 1, 0]
        #             rh_foot_trail_color = [0, 0, 1]
        #
        #         if (self.has_pre):
        #             pybullet.addUserDebugLine(self.pre_com_pos, com_pos,
        #                                       lineColorRGB=com_trail_color,
        #                                       lineWidth=line_width,
        #                                       lifeTime=trail_duration)
        #             pybullet.addUserDebugLine(self.pre_lf_foot_pos, lf_foot_pos,
        #                                       lineColorRGB=lf_foot_trail_color,
        #                                       lineWidth=line_width,
        #                                       lifeTime=trail_duration)
        #             pybullet.addUserDebugLine(self.pre_lh_foot_pos, lh_foot_pos,
        #                                       lineColorRGB=lh_foot_trail_color,
        #                                       lineWidth=line_width,
        #                                       lifeTime=trail_duration)
        #             pybullet.addUserDebugLine(self.pre_rf_foot_pos, rf_foot_pos,
        #                                       lineColorRGB=rf_foot_trail_color,
        #                                       lineWidth=line_width,
        #                                       lifeTime=trail_duration)
        #             pybullet.addUserDebugLine(self.pre_rh_foot_pos, rh_foot_pos,
        #                                       lineColorRGB=rh_foot_trail_color,
        #                                       lineWidth=line_width,
        #                                       lifeTime=trail_duration)
        #         self.pre_com_pos = com_pos
        #         self.pre_lf_foot_pos = lf_foot_pos
        #         self.pre_lh_foot_pos = lh_foot_pos
        #         self.pre_rf_foot_pos = rf_foot_pos
        #         self.pre_rh_foot_pos = rh_foot_pos
        #         self.has_pre = True
        #
        # if options["plot_virtual_leg_pair"] is True:
        #
        #     # get foot tip position through contacts
        #     lowerleg_ids = [2, 5, 8, 11]
        #     contact_points = []
        #     for lowerleg_id in lowerleg_ids:
        #         contact = pybullet.getContactPoints(self.floor, self.id, -1, lowerleg_id)
        #         if contact:
        #             contact_points.append(contact[0][5])
        #
        #     gait = "trot"
        #     gait = "pace"
        #     gait = "bound"
        #     if contact_points:
        #         if gait is "pace":
        #             pybullet.addUserDebugLine(lineFromXYZ=contact_points[0],
        #                                       lineToXYZ=contact_points[1],
        #                                       lineColorRGB=[0, 1, 0],
        #                                       lineWidth=5,
        #                                       lifeTime=0)
        #
        #             pybullet.addUserDebugLine(lineFromXYZ=contact_points[2],
        #                                       lineToXYZ=contact_points[3],
        #                                       lineColorRGB=[0, 1, 0],
        #                                       lineWidth=5,
        #                                       lifeTime=0)
        #
        #         if gait is "trot":
        #             if len(contact_points) == 4:
        #                 pybullet.addUserDebugLine(lineFromXYZ=contact_points[0],
        #                                           lineToXYZ=contact_points[3],
        #                                           lineColorRGB=[0, 1, 0],
        #                                           lineWidth=2,
        #                                           lifeTime=0.1)
        #
        #                 pybullet.addUserDebugLine(lineFromXYZ=contact_points[1],
        #                                           lineToXYZ=contact_points[2],
        #                                           lineColorRGB=[0, 1, 0],
        #                                           lineWidth=2,
        #                                           lifeTime=0.1)
        #             elif len(contact_points) == 2:
        #                 pybullet.addUserDebugLine(lineFromXYZ=contact_points[0],
        #                                           lineToXYZ=contact_points[1],
        #                                           lineColorRGB=[1, 0, 0],
        #                                           lineWidth=2,
        #                                           lifeTime=0.1)
        #
        #         if gait is "bound":
        #             pybullet.addUserDebugLine(lineFromXYZ=contact_points[1],
        #                                       lineToXYZ=contact_points[3],
        #                                       lineColorRGB=[0, 1, 0],
        #                                       lineWidth=5,
        #                                       lifeTime=0)
        #
        #             pybullet.addUserDebugLine(lineFromXYZ=contact_points[0],
        #                                       lineToXYZ=contact_points[2],
        #                                       lineColorRGB=[0, 1, 0],
        #                                       lineWidth=5,
        #                                       lifeTime=0)
        #
        #     if self.debug:
        #         pybullet.addUserDebugText(text=str(round(t, 2)) + "sec",
        #                                   textPosition=[0, 0, 0], textColorRGB=[1, 0, 0], textSize=1.5, lifeTime=0.1)
        #
        #         # plot ref frame
        #         line_length = 0.1
        #         line_width = 1
        #         red = [1, 0, 0]
        #         green = [0, 1, 0]
        #         blue = [0, 0, 1]
        #         pink = [1, 0.75, 0.79]
        #
        #         frame_pos = self.ref_com_pos
        #         frame_rot = np.eye(3)
        #         frame_life_time = 0.2
        #         pybullet.addUserDebugLine(frame_pos, frame_pos + line_length * frame_rot[:, 0],
        #                                   lineColorRGB=pink,
        #                                   lineWidth=line_width,
        #                                   lifeTime=frame_life_time)
        #         pybullet.addUserDebugLine(frame_pos, frame_pos + line_length * frame_rot[:, 1],
        #                                   lineColorRGB=pink,
        #                                   lineWidth=line_width,
        #                                   lifeTime=frame_life_time)
        #         pybullet.addUserDebugLine(frame_pos, frame_pos + line_length * frame_rot[:, 2],
        #                                   lineColorRGB=pink,
        #                                   lineWidth=line_width,
        #                                   lifeTime=frame_life_time)
        #
        #         frame_pos = self.robot_state.com_pos
        #         frame_rot = self.robot_state.base.rotation_matrix
        #         frame_life_time = 0.2
        #         pybullet.addUserDebugLine(frame_pos, frame_pos + line_length * frame_rot[:, 0],
        #                                   lineColorRGB=red,
        #                                   lineWidth=line_width,
        #                                   lifeTime=frame_life_time)
        #         pybullet.addUserDebugLine(frame_pos, frame_pos + line_length * frame_rot[:, 1],
        #                                   lineColorRGB=green,
        #                                   lineWidth=line_width,
        #                                   lifeTime=frame_life_time)
        #         pybullet.addUserDebugLine(frame_pos, frame_pos + line_length * frame_rot[:, 2],
        #                                   lineColorRGB=blue,
        #                                   lineWidth=line_width,
        #                                   lifeTime=frame_life_time)
