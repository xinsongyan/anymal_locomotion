import rbdl
import numpy as np

import utils.transformations as tf
from utils.yaml_parser import load_yaml


class StateEstimator:
    def __init__(self, yaml_path):
        self.configs = load_yaml(yaml_path)
        self.rbdl_model = rbdl.loadModel(self.configs["urdf_path"].encode('utf-8'), verbose=False, floating_base=True)
        self.base_link_name = self.configs["base_link_name"]

        self.end_effector_names = self.configs["end_effector_names"]
        self.end_effector_tips = self.configs["end_effector_tips"]
        self.end_effector_types = self.configs["end_effector_types"]

        self.end_effector_vertices = self.configs["end_effector_vertices"]

        # dof information
        self.N = self.rbdl_model.dof_count
        self.n = self.rbdl_model.dof_count - 6

        self.actuated_body_names = self.get_actuated_body_names()


        self.pre_com_pos = None
        self.print_rbdl_model_info()

    def print_rbdl_model_info(self):
        print("-" * 100)
        print("rbdl model information")
        print("-" * 100)
        print("rbdl model q_size:", self.rbdl_model.q_size)
        print("rbdl model qdot_size:", self.rbdl_model.qdot_size)
        print("rbdl model dof_count:", self.rbdl_model.dof_count)
        print("rbdl model full_body_names: ", self.get_full_body_names())
        print("rbdl model actuated_body_names: ", self.get_actuated_body_names())
        print("-" * 100)
        print("rbdl model information")
        print("-" * 100)

    def print_state_info(self):
        print("mass: ", self.mass)
        print("com_position: ", self.com_position)
        print("com_velocity: ", self.com_velocity)
        print("com_acceleration: ", self.com_acceleration)
        print("linear_momentum: ", self.linear_momentum)
        print("angular_momentum: ", self.angular_momentum)
        print("change_of_linear_momentum: ", self.change_of_linear_momentum)  # since qdd=0, this is the bias term
        print("change_of_angular_momentum: ", self.change_of_angular_momentum)  # since qdd=0, this is the bias term

        print("<Test CMM calculation>")
        print("rbdl linear momentum:  ", self.linear_momentum)
        print("CMM_linear*qd:         ", self.CMM_linear.dot(self.qd))
        print("rbdl angular_momentum: ", self.angular_momentum)
        print("CMM_angular*qd:        ", self.CMM_angular.dot(self.qd))
        print("rbdl change_of_linear_momentum: ", self.change_of_linear_momentum)  # since qdd=0, this is the bias term
        print("CMM_bias_force_linear:        ", self.CMM_bias_force_linear)
        print("rbdl change_of_angular_momentum: ", self.change_of_angular_momentum)  # since qdd=0, this is the bias term
        print("CMM_bias_force_angular:        ", self.CMM_bias_force_angular)

    def get_full_body_names(self):
        full_body_names = []
        full_body_names.append(self.rbdl_model.GetBodyName(0))
        body_id = 1
        while self.rbdl_model.IsBodyId(body_id):
            full_body_names.append(self.rbdl_model.GetBodyName(body_id).decode())
            body_id += 1
        return full_body_names

    def get_actuated_body_names(self):
        body_names = []
        body_id = 3
        while self.rbdl_model.IsBodyId(body_id):
            body_name = self.rbdl_model.GetBodyName(body_id).decode()
            body_names.append(body_name)
            body_id += 1
        return body_names




    def estimate_from_observation(self, base_position, base_quaternion, actuated_joint_position, base_twist_linear,
                                  base_twist_angular, actuated_joint_velocity, verbose=False):
        # print(actuated_joint_position)
        # print(actuated_joint_velocity)

        actuated_q, actuated_qd = [], []
        for body_name in self.actuated_body_names:
            actuated_q.append(actuated_joint_position[body_name])
            actuated_qd.append(actuated_joint_velocity[body_name])

        generalized_q = np.concatenate((base_position, base_quaternion[:3], actuated_q, [base_quaternion[-1]]))
        base_omega_base = tf.quaternion_matrix(base_quaternion)[:3, :3].T.dot(base_twist_angular)
        generalized_qd = np.concatenate((base_twist_linear, base_omega_base, actuated_qd))

        self.q = generalized_q
        self.qd = generalized_qd
        self.qdd = np.zeros(self.rbdl_model.dof_count)



        self.estimate()

        if verbose:
            self.print_state_info()

    def estimate(self):

        self.com_position = np.zeros(3)
        self.com_velocity = np.zeros(3)
        self.com_acceleration = np.zeros(3)  # depending on qdd
        self.angular_momentum = np.zeros(3)
        self.change_of_angular_momentum = np.zeros(3)  # depending on qdd
        self.mass = rbdl.CalcCenterOfMass(self.rbdl_model, self.q, self.qd, self.qdd,
                                          self.com_position,
                                          self.com_velocity,
                                          self.com_acceleration,
                                          self.angular_momentum,
                                          self.change_of_angular_momentum)
        self.linear_momentum = self.mass * self.com_velocity
        self.change_of_linear_momentum = self.mass * self.com_acceleration

        self.kinetic_energy = 0.5 * self.mass * (np.linalg.norm(self.com_velocity) ** 2)
        self.potential_energy = self.mass * 9.81 * self.com_position[2]
        self.energy = self.kinetic_energy + self.potential_energy

        self.inertia_matrix = np.zeros((self.N, self.N))
        rbdl.CompositeRigidBodyAlgorithm(self.rbdl_model, self.q, self.inertia_matrix)

        self.nonlinear_effects = np.zeros(self.N)
        rbdl.NonlinearEffects(self.rbdl_model, self.q, self.qd, self.nonlinear_effects)




        self.CMM, self.CMM_bias_force = self.centroidal_dynamics(self.rbdl_model, self.q, self.qd, self.base_link_name)
        self.CMM_angular = self.CMM[:3, :]
        self.CMM_linear = self.CMM[-3:, :]
        self.CMM_bias_force_angular = self.CMM_bias_force[:3]
        self.CMM_bias_force_linear = self.CMM_bias_force[-3:]


        # base body state
        self.base = BodyState(self.rbdl_model, self.q, self.qd, self.base_link_name, np.zeros(3))


        # end effector tip states
        self.end_effector = {}
        for end_effector_name, end_effector_offset in self.end_effector_tips.items():
            self.end_effector[end_effector_name] = BodyState(self.rbdl_model,
                                                             self.q,
                                                             self.qd,
                                                             end_effector_name,
                                                             end_effector_offset)

        # end effector vertices states
        self.end_effector_vertices_state = {}
        for end_effector_name, end_effector_vertex_offsets in self.end_effector_vertices.items():
            temp = []
            for end_effector_vertex_offset in end_effector_vertex_offsets:
                temp.append(BodyState(self.rbdl_model,
                                      self.q,
                                      self.qd,
                                      end_effector_name,
                                      end_effector_vertex_offset))
            self.end_effector_vertices_state[end_effector_name] = temp

        self.create_alias()

    def create_alias(self):
        self.com_pos = self.com_position
        self.com_vel = self.com_velocity
        self.com_acc = self.com_acceleration

    def centroidal_dynamics(self, rbdl_model, q, qd, base_link_name, update_kinematics=True):
        """
        Cite: Wensing, Patrick M., and David E. Orin. "Improved computation of the humanoid centroidal dynamics and application for whole-body control." International Journal of Humanoid Robotics 13.01 (2016): 1550039.
        """
        if rbdl_model.IsBodyId(rbdl_model.GetBodyId(base_link_name.encode())) == False:
            print("[Error] StateEstimator -> centroidal_dynamics(): Body '", self.base_link_name, "' not found in the model!")


        base_link_name = base_link_name.encode()
        N, n = rbdl_model.dof_count, rbdl_model.dof_count - 6

        H = np.zeros((N, N))
        rbdl.CompositeRigidBodyAlgorithm(rbdl_model, q, H, update_kinematics)

        # nonlinear_effects = Coriolis term: C(q, qd) * qd + gravity term G(q)
        nonlinear_effects = np.zeros(N)
        gravity_term = np.zeros(N)
        rbdl.NonlinearEffects(rbdl_model, q, qd, nonlinear_effects)
        rbdl.NonlinearEffects(rbdl_model, q, np.zeros(N), gravity_term)
        Cqdot = nonlinear_effects - gravity_term

        # U_1
        U_1 = np.hstack((np.identity(6), np.zeros((6, n))))

        # R_01(q_1)
        # R_01 = rbdl_model.X_base[2].E.T
        R_01 = rbdl.CalcBodyWorldOrientation(rbdl_model, q, rbdl_model.GetBodyId(base_link_name), update_kinematics).T

        # Phi_1: this is a matrix transfer gereralized velocity of floating base to spatial velocity defined in local frame
        Phi_1 = np.vstack((np.hstack((np.zeros((3, 3)), np.identity(3))), np.hstack((R_01.T, np.zeros((3, 3))))))

        # Psi_1
        Psi_1 = np.linalg.inv(Phi_1)

        # Computations:
        H11 = U_1.dot(H).dot(U_1.T)
        I1C = Psi_1.T.dot(H11).dot(Psi_1)  # 6*6
        M = I1C[6 - 1, 6 - 1]
        p1G = (1.0 / M) * np.array([I1C[3 - 1, 5 - 1], I1C[1 - 1, 6 - 1], I1C[2 - 1, 4 - 1]])
        skew = lambda V: np.array([[0, -V[2], V[1]], [V[2], 0, -V[0]], [-V[1], V[0], 0]])  # inline skew function
        X_iG_T = np.vstack((np.hstack((R_01, R_01.dot(skew(p1G).T))), np.hstack((np.zeros((3, 3)), R_01))))

        A_G = X_iG_T.dot(Psi_1.T).dot(U_1).dot(H)
        A_Gd_qd = X_iG_T.dot(Psi_1.T).dot(U_1).dot(Cqdot)

        return A_G, A_Gd_qd

    def centroidal_momentum_singular_values(self, A_G):
        u, s, vh = np.linalg.svd(A_G, full_matrices=True)
        return s

    def centroidal_momentum_ellipsoid(self, A_G):
        u, s, vh = np.linalg.svd(A_G, full_matrices=True)
        position = self.com
        homogeneous_matrix = lambda rot=np.identity(3), pos=np.zeros(3): np.vstack((np.append(rot[0, :], pos[0]),
                                                                                    np.append(rot[1, :], pos[1]),
                                                                                    np.append(rot[2, :], pos[2]),
                                                                                    np.array([0, 0, 0, 1])))
        quaternion = tf.quaternion_from_matrix(homogeneous_matrix(rot=u))
        scale = s
        return position, quaternion, scale

    def debug(self):
        import pybullet


        line_width = 2
        trail_duration = 0
        com_trail_color = [1, 0, 0]

        if (self.pre_com_pos is not None):
            pybullet.addUserDebugLine(self.pre_com_pos, self.com_pos,
                                      lineColorRGB=com_trail_color,
                                      lineWidth=line_width,
                                      lifeTime=trail_duration)

            for end_effector_name, end_effector_state in self.end_effector.items():
                pybullet.addUserDebugLine(self.pre_end_effector[end_effector_name].pos, end_effector_state.pos,
                                          lineColorRGB=com_trail_color,
                                          lineWidth=line_width,
                                          lifeTime=trail_duration)

            for end_effector_name, end_effector_vertices_state in self.end_effector_vertices_state.items():
                for index, end_effector_vertex_state in enumerate(end_effector_vertices_state):
                    pybullet.addUserDebugLine(self.pre_end_effector_vertices_state[end_effector_name][index].pos, end_effector_vertex_state.pos,
                                              lineColorRGB=com_trail_color,
                                              lineWidth=line_width,
                                              lifeTime=trail_duration)

        self.pre_com_pos = self.com_pos
        self.pre_end_effector = self.end_effector
        self.pre_end_effector_vertices_state = self.end_effector_vertices_state




class BodyState:
    def __init__(self, rbdl_model, q, qd, body_name, body_point_position, update_kinematics=True):

        if rbdl_model.IsBodyId(rbdl_model.GetBodyId(body_name.encode())) == False:
            print("[Error]:  StateEstimator -> BodyState: Body '", body_name, "' not found in the model!")


        body_id = rbdl_model.GetBodyId(body_name.encode())
        body_point_position = np.array(body_point_position, dtype=np.double)

        # homogeneous_matrix = lambda rot=np.identity(3), pos=np.zeros(3): np.vstack((np.append(rot[0, :], pos[0]),
        #                                                                             np.append(rot[1, :], pos[1]),
        #                                                                             np.append(rot[2, :], pos[2]),
        #                                                                             np.array([0, 0, 0, 1])))

        self.position = rbdl.CalcBodyToBaseCoordinates(rbdl_model, q, body_id, body_point_position, update_kinematics)
        self.rotation_matrix = rbdl.CalcBodyWorldOrientation(rbdl_model, q, body_id, update_kinematics).T
        self.transform_matrix = self.homogeneous_matrix(rot=self.rotation_matrix, pos=self.position)
        self.rpy = np.array(tf.euler_from_matrix(self.homogeneous_matrix(rot=self.rotation_matrix), axes='sxyz'))
        self.quaternion = tf.quaternion_from_matrix(self.homogeneous_matrix(rot=self.rotation_matrix))
        # self.pose = np.concatenate((self.position, self.quaternion))

        self.spatial_velocity = rbdl.CalcPointVelocity6D(rbdl_model, q, qd, body_id, body_point_position,
                                                         update_kinematics)
        self.angular_velocity = self.spatial_velocity[:3]
        self.linear_velocity = self.spatial_velocity[-3:]
        # self.twist = np.concatenate((self.linear_velocity, self.angular_velocity))

        self.J = np.zeros((6, rbdl_model.dof_count))
        rbdl.CalcPointJacobian6D(rbdl_model, q, body_id, body_point_position, self.J, update_kinematics)
        self.Jdqd = rbdl.CalcPointAcceleration6D(rbdl_model, q, qd, np.zeros(rbdl_model.dof_count), body_id,
                                                 body_point_position,
                                                 update_kinematics)

        self.create_alias()

    def homogeneous_matrix(self, rot=np.identity(3), pos=np.zeros(3)):
        T = np.identity(4)
        T[:3, :3] = rot[:3, :3]
        T[:3, -1] = pos[:3]
        return T

    def create_alias(self):
        self.pos = self.position
        self.rot = self.rotation_matrix

        self.pose = {"positon": self.position, "quaternion": self.quaternion}
        self.twist = {"linear": self.linear_velocity, "angular": self.angular_velocity}

        self.J_angular = self.J[:3, :]
        self.J_linear = self.J[-3:, :]

        self.Jdqd_angular = self.Jdqd[:3]
        self.Jdqd_linear = self.Jdqd[-3:]

    def show(self):
        print("body_id            \t:", self.body_id)
        print("body_point_position\t:", self.body_point_position)
        print("position           \t:", self.position)
        print("rotation_matrix    \t:", "\n", self.rotation_matrix)
        print("quaternion         \t:", self.quaternion)
        print("rpy                \t:", self.rpy)
        print("pose               \t:", self.pose)
        print("twist              \t:", self.twist)
        print("spatial_velocity   \t:", self.spatial_velocity)
        print("Jdqd               \t:", self.Jdqd)
        print("J                  \t:", "\n", self.J)
