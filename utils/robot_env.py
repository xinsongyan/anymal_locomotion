import time
import pybullet
import pybullet_data
from utils.yaml_parser import load_yaml

class Floor:
    def __init__(self):
        pybullet.setAdditionalSearchPath(pybullet_data.getDataPath())
        self.id = pybullet.loadURDF('plane.urdf')


    def changeFriction(self, lateralFriction=1.0, spinningFriction=1.0, rollingFriction=0.0):
        print("Current floor dynamic: ", pybullet.getDynamicsInfo(self.id, -1))
        pybullet.changeDynamics(bodyUniqueId=self.id, linkIndex=-1, lateralFriction=lateralFriction, spinningFriction=spinningFriction, rollingFriction=rollingFriction)
        print("Updated floor dynamic: ", pybullet.getDynamicsInfo(self.id, -1))

class Table:
    def __init__(self, pos, rpy, scale=1.0):
        pybullet.setAdditionalSearchPath(pybullet_data.getDataPath())
        table_urdf_path = pybullet_data.getDataPath() + "/table/table.urdf"
        self.id = pybullet.loadURDF(table_urdf_path,
                                    basePosition=pos,
                                    baseOrientation=pybullet.getQuaternionFromEuler(rpy),
                                    useMaximalCoordinates=0,
                                    useFixedBase=0,
                                    flags=0,
                                    globalScaling=scale,
                                    physicsClientId=0)



    def changeFriction(self, lateralFriction=1.0, spinningFriction=1.0, rollingFriction=0.0):
        print("Current floor dynamic: ", pybullet.getDynamicsInfo(self.id, -1))
        pybullet.changeDynamics(bodyUniqueId=self.id, linkIndex=-1, lateralFriction=lateralFriction, spinningFriction=spinningFriction, rollingFriction=rollingFriction)
        print("Updated floor dynamic: ", pybullet.getDynamicsInfo(self.id, -1))

class RobotEnv:
    def __init__(self, config_path=None):
        self.configs = load_yaml(config_path)
        self.sim_rate = self.configs["sim_rate"]
        self.sim_time_step = 1.0 / self.sim_rate
        self.real_time_sim = self.configs["real_time_sim"]

        self.physics_client = pybullet.connect(pybullet.GUI)
        self.configureGUI()
        pybullet.setTimeStep(self.sim_time_step)
        pybullet.setGravity(0, 0, -self.configs["gravity"])

        self.floor = Floor()

        try:
            print("[RobotEnv] Try to load table...")
            self.table = Table(pos=self.configs["table"]["pos"], rpy=self.configs["table"]["rpy"], scale=self.configs["table"]["scale"])
            print("Table loaded succesfully! Table id: ", self.table.id)
        except:
            print("[RobotEnv] Fail to load table! To load a table, please add following instructions into config file: \n table: {pos: [0, 0, 0], rpy: [0, 0, 0]}")


        self.sim_count = 0
        self.sim_time = 0.0



    def configureGUI(self):
        pybullet.configureDebugVisualizer(pybullet.COV_ENABLE_GUI, self.configs["COV_ENABLE_GUI"])
        pybullet.configureDebugVisualizer(pybullet.COV_ENABLE_RGB_BUFFER_PREVIEW, self.configs["COV_ENABLE_RGB_BUFFER_PREVIEW"])
        pybullet.configureDebugVisualizer(pybullet.COV_ENABLE_DEPTH_BUFFER_PREVIEW, self.configs["COV_ENABLE_DEPTH_BUFFER_PREVIEW"])
        pybullet.configureDebugVisualizer(pybullet.COV_ENABLE_SEGMENTATION_MARK_PREVIEW, self.configs["COV_ENABLE_SEGMENTATION_MARK_PREVIEW"])
        pybullet.resetDebugVisualizerCamera(cameraDistance=self.configs["cameraDistance"],
                                            cameraYaw=self.configs["cameraYaw"],
                                            cameraPitch=self.configs["cameraPitch"],
                                            cameraTargetPosition=self.configs["cameraTargetPosition"])



    def reset(self):
        pass

    def render(self):
        pass

    def debug(self):
        self.keys = pybullet.getKeyboardEvents()

        # pause simulation by space key event
        space_key = ord(' ')
        if space_key in self.keys and self.keys[space_key] & pybullet.KEY_WAS_TRIGGERED:
            print("Simulation Paused!")
            print("Press Space key to start again!")
            while True:
                keys = pybullet.getKeyboardEvents()
                if space_key in keys and keys[space_key] & pybullet.KEY_WAS_TRIGGERED:
                    break


    def step(self):

        pybullet.stepSimulation()
        if self.real_time_sim:
            time.sleep(self.sim_time_step)
        self.sim_count += 1
        self.sim_time = self.sim_count * self.sim_time_step


if __name__ == '__main__':

    import os
    yaml_path = os.getcwd() + "/config.yaml"
    env = RobotEnv(config_path=yaml_path)

    env.reset()

    while True:
        env.render()

        observation = env.step()

        env.debug()