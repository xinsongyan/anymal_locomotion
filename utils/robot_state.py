import rbdl
import numpy as np
from . import transformations

class RobotState:
    def __init__(self, rbdl_model, q, qd, base_link_name, ee_names, ee_offsets, verbose=False):
        self.q = q
        self.qd = qd

        # dof information
        self.N = rbdl_model.dof_count
        self.n = rbdl_model.dof_count - 6

        self.qdd = np.zeros(rbdl_model.dof_count)
        self.com_position = np.zeros(3)
        self.com_velocity = np.zeros(3)
        self.com_acceleration = np.zeros(3)  # depending on qdd
        self.angular_momentum = np.zeros(3)
        self.change_of_angular_momentum = np.zeros(3)  # depending on qdd
        self.mass = rbdl.CalcCenterOfMass(rbdl_model, self.q, self.qd, self.qdd,
                                          self.com_position,
                                          self.com_velocity,
                                          self.com_acceleration,
                                          self.angular_momentum,
                                          self.change_of_angular_momentum)
        self.linear_momentum = self.mass * self.com_velocity
        self.change_of_linear_momentum = self.mass * self.com_acceleration


        # energy
        self.kinetic_energy = 0.5 * self.mass * (np.linalg.norm(self.com_velocity) ** 2)
        self.potential_energy = self.mass * 9.81 * self.com_position[2]
        self.energy = self.kinetic_energy + self.potential_energy



        # print("debug---------------------------->")
        # FL_calf = BodyState(rbdl_model, self.q, np.zeros(rbdl_model.dof_count), rbdl_model.GetBodyId("FL_calf".encode()), np.zeros(3))
        # print("calc FL_calf position: ", FL_calf.pos, FL_calf.quaternion)
        #
        # base_com = BodyState(rbdl_model, self.q, self.qd, rbdl_model.GetBodyId("trunk".encode()), np.array([0.002284, -4.1e-05, 0.025165]))
        # print("calc base_com position: ", base_com.pos, base_com.quaternion)
        #
        #
        # FL_hip_com = BodyState(rbdl_model, self.q, self.qd, rbdl_model.GetBodyId("FL_hip".encode()), np.array([-0.001568, -0.008134, 0.000864]))
        # print("calc FL_hip_com position: ", FL_hip_com.pos, FL_hip_com.quaternion)



        if verbose:
            print("mass: ", self.mass)
            print("com_position: ", self.com_position)
            print("com_velocity: ", self.com_velocity)
            print("com_acceleration: ", self.com_acceleration)
            print("linear_momentum: ", self.linear_momentum)
            print("angular_momentum: ", self.angular_momentum)
            print("change_of_linear_momentum: ", self.change_of_linear_momentum)  # since qdd=0, this is the bias term
            print("change_of_angular_momentum: ", self.change_of_angular_momentum)  # since qdd=0, this is the bias term




        self.inertia_matrix = np.zeros((self.N, self.N))
        rbdl.CompositeRigidBodyAlgorithm(rbdl_model, self.q, self.inertia_matrix)

        self.nonlinear_effects = np.zeros(self.N)
        rbdl.NonlinearEffects(rbdl_model, self.q, self.qd, self.nonlinear_effects)

        self.CMM, self.CMM_bias_force = self.centroidal_dynamics(rbdl_model, self.q, self.qd, base_link_name)
        self.CMM_angular = self.CMM[:3, :]
        self.CMM_linear = self.CMM[-3:, :]



        self.base = BodyState(rbdl_model, self.q, self.qd, rbdl_model.GetBodyId(base_link_name.encode()), np.zeros(3))
        # self.lf_foot = BodyState(rbdl_model, self.q, self.qd, rbdl_model.GetBodyId(ee_names[0].encode()), np.asarray(ee_offsets[0]))
        # self.lh_foot = BodyState(rbdl_model, self.q, self.qd, rbdl_model.GetBodyId(ee_names[1].encode()), np.asarray(ee_offsets[1]))
        # self.rf_foot = BodyState(rbdl_model, self.q, self.qd, rbdl_model.GetBodyId(ee_names[2].encode()), np.asarray(ee_offsets[2]))
        # self.rh_foot = BodyState(rbdl_model, self.q, self.qd, rbdl_model.GetBodyId(ee_names[3].encode()), np.asarray(ee_offsets[3]))
        # self.ee0 = BodyState(rbdl_model, self.q, self.qd, rbdl_model.GetBodyId(ee_names[0].encode()), np.asarray(ee_offsets[0]))
        # self.ee1 = BodyState(rbdl_model, self.q, self.qd, rbdl_model.GetBodyId(ee_names[1].encode()), np.asarray(ee_offsets[1]))
        # self.ee2 = BodyState(rbdl_model, self.q, self.qd, rbdl_model.GetBodyId(ee_names[2].encode()), np.asarray(ee_offsets[2]))
        # self.ee3 = BodyState(rbdl_model, self.q, self.qd, rbdl_model.GetBodyId(ee_names[3].encode()), np.asarray(ee_offsets[3]))
        #

        self.EE = []
        for ee_name, ee_offset in zip(ee_names, ee_offsets):
            self.EE.append(BodyState(rbdl_model, self.q, self.qd, rbdl_model.GetBodyId(ee_name.encode()), np.array(ee_offset, dtype=np.double)))

        # for walking
        self.lf_foot = BodyState(rbdl_model, self.q, self.qd, rbdl_model.GetBodyId(ee_names[0].encode()), np.asarray(ee_offsets[0]))
        self.rf_foot = BodyState(rbdl_model, self.q, self.qd, rbdl_model.GetBodyId(ee_names[1].encode()), np.asarray(ee_offsets[1]))
        self.lh_foot = BodyState(rbdl_model, self.q, self.qd, rbdl_model.GetBodyId(ee_names[2].encode()), np.asarray(ee_offsets[2]))
        self.rh_foot = BodyState(rbdl_model, self.q, self.qd, rbdl_model.GetBodyId(ee_names[3].encode()), np.asarray(ee_offsets[3]))





        # self.ee_states = {}
        # for ee_name, ee_offset in zip(ee_names, ee_offsets):
        #     self.ee_states[ee_name] = BodyState(rbdl_model, self.q, self.qd, rbdl_model.GetBodyId(ee_name.encode()), np.array(ee_offset, dtype=np.double))




        # self.waist = BodyState(rbdl_model, self.q, self.qd, rbdl_model.GetBodyId(b"Waist"), np.zeros(3))
        # self.lankle = BodyState(rbdl_model, self.q, self.qd, rbdl_model.GetBodyId(b"LFoot"), np.zeros(3))
        # self.rankle = BodyState(rbdl_model, self.q, self.qd, rbdl_model.GetBodyId(b"RFoot"), np.zeros(3))
        # self.lsole = BodyState(rbdl_model, self.q, self.qd, rbdl_model.GetBodyId(b"LFoot"), param.foot_sole_position)
        # self.rsole = BodyState(rbdl_model, self.q, self.qd, rbdl_model.GetBodyId(b"RFoot"), param.foot_sole_position)

        # # contact jacobian
        # self.Jc = np.vstack((self.lsole.J, self.rsole.J))

        # print("custom change_of_angular_momentum: ", self.CMM.dot(self.qdd)+self.CMM_bias_force)

        # self.CMM_singular_value_angular = self.centroidal_momentum_singular_values(self.CMM_angular)
        # self.CMM_singular_value_linear = self.centroidal_momentum_singular_values(self.CMM_linear)

        # self.CMM_manipulability_angular = np.linalg.det(self.CMM_angular[:, 6:].dot(self.CMM_angular[:, 6:].T))
        # self.CMM_manipulability_linear = np.linalg.det(self.CMM_linear[:, 6:].dot(self.CMM_linear[:, 6:].T))

        # self.CMM_manipulability_angular = np.linalg.det(self.CMM_angular.dot(self.CMM_angular.T))
        # self.CMM_manipulability_linear = np.linalg.det(self.CMM_linear.dot(self.CMM_linear.T))

        self.create_alias()

    def create_alias(self):
        self.com = self.com_position
        self.com_pos = self.com_position
        self.com_vel = self.com_velocity
        self.com_acc = self.com_acceleration


    def centroidal_dynamics(self, rbdl_model, q, qd, base_link_name, update_kinematics=True):
        """
        Cite: Wensing, Patrick M., and David E. Orin. "Improved computation of the humanoid centroidal dynamics and application for whole-body control." International Journal of Humanoid Robotics 13.01 (2016): 1550039.
        """
        base_link_name = base_link_name.encode('utf-8')  # python3
        N, n = rbdl_model.dof_count, rbdl_model.dof_count - 6

        H = np.zeros((N, N))
        rbdl.CompositeRigidBodyAlgorithm(rbdl_model, q, H, update_kinematics)

        # nonlinear_effects = Coriolis term: C(q, qd) * qd + gravity term G(q)
        nonlinear_effects = np.zeros(N)
        gravity_term = np.zeros(N)
        rbdl.NonlinearEffects(rbdl_model, q, qd, nonlinear_effects)
        rbdl.NonlinearEffects(rbdl_model, q, np.zeros(N), gravity_term)
        Cqdot = nonlinear_effects - gravity_term

        # U_1
        U_1 = np.hstack((np.identity(6), np.zeros((6, n))))

        # R_01(q_1)
        # R_01 = rbdl_model.X_base[2].E.T
        R_01 = rbdl.CalcBodyWorldOrientation(rbdl_model, q, rbdl_model.GetBodyId(base_link_name), update_kinematics).T

        # Phi_1: this is a matrix transfer gereralized velocity of floating base to spatial velocity defined in local frame
        Phi_1 = np.vstack((np.hstack((np.zeros((3, 3)), np.identity(3))), np.hstack((R_01.T, np.zeros((3, 3))))))

        # Psi_1
        Psi_1 = np.linalg.inv(Phi_1)

        # Computations:
        H11 = U_1.dot(H).dot(U_1.T)
        I1C = Psi_1.T.dot(H11).dot(Psi_1)  # 6*6
        M = I1C[6 - 1, 6 - 1]
        p1G = (1.0 / M) * np.array([I1C[3 - 1, 5 - 1], I1C[1 - 1, 6 - 1], I1C[2 - 1, 4 - 1]])
        skew = lambda V: np.array([[0, -V[2], V[1]], [V[2], 0, -V[0]], [-V[1], V[0], 0]])  # inline skew function
        X_iG_T = np.vstack((np.hstack((R_01, R_01.dot(skew(p1G).T))), np.hstack((np.zeros((3, 3)), R_01))))

        A_G = X_iG_T.dot(Psi_1.T).dot(U_1).dot(H)
        A_Gd_qd = X_iG_T.dot(Psi_1.T).dot(U_1).dot(Cqdot)

        return A_G, A_Gd_qd

    def centroidal_momentum_singular_values(self, A_G):
        u, s, vh = np.linalg.svd(A_G, full_matrices=True)

        return s

    def centroidal_momentum_ellipsoid(self, A_G):
        u, s, vh = np.linalg.svd(A_G, full_matrices=True)

        position = self.com
        homogeneous_matrix = lambda rot=np.identity(3), pos=np.zeros(3): np.vstack((np.append(rot[0, :], pos[0]),
                                                                                    np.append(rot[1, :], pos[1]),
                                                                                    np.append(rot[2, :], pos[2]),
                                                                                    np.array([0, 0, 0, 1])))

        quaternion = transformations.quaternion_from_matrix(homogeneous_matrix(rot=u))
        scale = s
        return position, quaternion, scale

    def show(self):
        print("RobotState:")
        print("com pos:", self.com)
        print("lsole pos:", self.lsole.pos)
        print("rsole pos:", self.rsole.pos)


class BodyState:
    def __init__(self, rbdl_model, q, qd, body_id, body_point_position, update_kinematics=True):
        self.body_id = body_id
        self.body_point_position = body_point_position

        # self.pos = rbdl.CalcBodyToBaseCoordinates(rbdl_model, q, body_id, body_point_position, update_kinematics)
        # self.rot = rbdl.CalcBodyWorldOrientation(rbdl_model, q, body_id, update_kinematics).T
        # self.transform = self.homogeneous_matrix(rot=self.rot, pos=self.pos)
        # self.quaternion = transformations.quaternion_from_matrix(self.homogeneous_matrix(rot=self.rot))
        # self.rpy = np.array(transformations.euler_from_matrix(self.homogeneous_matrix(rot=self.rot), axes='sxyz'))
        #
        # self.vel = rbdl.CalcPointVelocity6D(rbdl_model, q, qd, body_id, body_point_position, update_kinematics)
        # self.angular_vel = self.vel[:3]
        # self.linear_vel = self.vel[-3:]

        # standard notation
        # self.pose = Pose(self.pos, self.quaternion)
        # self.twist = Twist(self.linear_vel, self.angular_vel)
        # self.Pose = Pose(self.pos, self.quaternion)
        # self.Twist = Twist(self.linear_vel, self.angular_vel)

        homogeneous_matrix = lambda rot=np.identity(3), pos=np.zeros(3): np.vstack((np.append(rot[0, :], pos[0]),
                                                                                    np.append(rot[1, :], pos[1]),
                                                                                    np.append(rot[2, :], pos[2]),
                                                                                    np.array([0, 0, 0, 1])))

        self.position = rbdl.CalcBodyToBaseCoordinates(rbdl_model, q, body_id, body_point_position, update_kinematics)
        self.rotation_matrix = rbdl.CalcBodyWorldOrientation(rbdl_model, q, body_id, update_kinematics).T
        self.transform_matrix = homogeneous_matrix(rot=self.rotation_matrix, pos=self.position)
        self.rpy = np.array(transformations.euler_from_matrix(homogeneous_matrix(rot=self.rotation_matrix), axes='sxyz'))
        self.quaternion = transformations.quaternion_from_matrix(homogeneous_matrix(rot=self.rotation_matrix))
        self.pose = np.concatenate((self.position, self.quaternion))

        self.spatial_velocity = rbdl.CalcPointVelocity6D(rbdl_model, q, qd, body_id, body_point_position, update_kinematics)
        self.angular_velocity = self.spatial_velocity[:3]
        self.linear_velocity = self.spatial_velocity[-3:]
        self.twist = np.concatenate((self.linear_velocity, self.angular_velocity))

        self.J = np.zeros((6, rbdl_model.dof_count))
        rbdl.CalcPointJacobian6D(rbdl_model, q, body_id, body_point_position, self.J, update_kinematics)
        self.Jdqd = rbdl.CalcPointAcceleration6D(rbdl_model, q, qd, np.zeros(rbdl_model.dof_count), body_id, body_point_position,
                                                 update_kinematics)

    # def homogeneous_matrix(self, rot=np.identity(3), pos=np.zeros(3)):
    #     T = np.identity(4)
    #     T[:3, :3] = rot[:3, :3]
    #     T[:3, -1] = pos[:3]
    #     return T
        self.create_alias()

    def create_alias(self):
        self.pos = self.position
        self.rot = self.rotation_matrix

    def show(self):
        print("body_id            \t:", self.body_id)
        print("body_point_position\t:", self.body_point_position)
        print("position           \t:", self.position)
        print("rotation_matrix    \t:", "\n", self.rotation_matrix)
        print("quaternion         \t:", self.quaternion)
        print("rpy                \t:", self.rpy)
        print("pose               \t:", self.pose)
        print("twist              \t:", self.twist)
        print("spatial_velocity   \t:", self.spatial_velocity)
        print("Jdqd               \t:", self.Jdqd)
        print("J                  \t:", "\n", self.J)
