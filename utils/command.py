#  Copyright (c) 2018.  Songyan XIN,  Italian Institute of Technology.

import numpy as np

class HighLevelCommand:
    def __init__(self):
        pass

    def set_whole_body_config(self, config=None):
        self.whole_body_config = config

    def set_task_hierarchy(self, task_hierarchy=None):
        self.task_hierarchy = task_hierarchy

    def set_support_legs(self, support_legs=None):
        self.support_legs = support_legs

    def set_swing_legs(self, swing_legs=None):
        self.swing_legs = swing_legs

    def set_swing_leg_positions(self, swing_leg_positions):
        self.swing_leg_positions = swing_leg_positions

    def set_swing_leg_quaternions(self, swing_leg_quaternions):
        self.swing_leg_quaternions = swing_leg_quaternions


    def set_com_target(self, com_target=None, com_pos=np.zeros(3), com_vel=np.zeros(3), com_acc=np.zeros(3)):
        if com_target is None:
            self.com_pos = com_pos
            self.com_vel = com_vel
            self.com_acc = com_acc
        else:
            if isinstance(com_target, (list, tuple)):
                self.com_pos = com_target[0]
                self.com_vel = com_target[1]
                self.com_acc = com_target[2]
            elif isinstance(com_target, dict):
                self.com_pos = com_target["pos"]
                self.com_vel = com_target["vel"]
                self.com_acc = com_target["acc"]

    # def set_base_command(self, base_quat=np.array([0,0,0,1]), base_vel_angular=np.zeros(3), base_acc_angular=np.zeros(3)):
    #     self.base_quat = base_quat
    #     self.base_vel_angular = base_vel_angular
    #     self.base_acc_angular = base_acc_angular

    def set_base_target(self, base_target=None, base_quat=np.array([0,0,0,1]), base_vel_angular=np.zeros(3), base_acc_angular=np.zeros(3)):
        if base_target is None:
            self.base_quat = base_quat
            self.base_vel_angular = base_vel_angular
            self.base_acc_angular = base_acc_angular
        else:
            if isinstance(base_target, (list, tuple)):
                self.base_quat = base_target[0]
                self.base_vel_angular = base_target[1]
                self.base_acc_angular = base_target[2]
            elif isinstance(base_target, dict):
                self.base_quat = base_target["quat"]
                self.base_vel_angular = base_target["vel_angular"]
                self.base_acc_angular = base_target["acc_angular"]

    def show(self):
        print("-" * 30)
        print("[HighLevelCommand]")
        # print("task_hierarchy:", self.task_hierarchy)
        print("com_pos:", self.com_pos)
        print("support_legs:", self.support_legs)
        print("swing_legs:", self.swing_legs)
        print("-" * 30)




# class HighLevelCommand:
#     def __init__(self, com_pos=[0,0,0], com_quat=[0,0,0,1],
#                  lsole_pos=[0,0,0], lsole_quat=[0,0,0,1],
#                  rsole_pos=[0,0,0], rsole_quat=[0,0,0,1],
#                  com_vel_linear=np.zeros(3),
#                  com_vel_angular=np.zeros(3),
#                  com_acc_linear=np.zeros(3),
#                  com_acc_angular=np.zeros(3),
#                  lsole_vel_linear=np.zeros(3),
#                  lsole_vel_angular=np.zeros(3),
#                  lsole_acc_linear=np.zeros(3),
#                  lsole_acc_angular=np.zeros(3),
#                  rsole_vel_linear=np.zeros(3),
#                  rsole_vel_angular=np.zeros(3),
#                  rsole_acc_linear=np.zeros(3),
#                  rsole_acc_angular=np.zeros(3),
#                  angular_momentum=np.zeros(3),
#                  contact_state=""):
#
#         self.com_pos = np.array(com_pos)
#         self.com_quat = np.array(com_quat)
#         self.com_pose = np.concatenate((com_pos, com_quat))
#
#         self.com_vel_linear = com_vel_linear
#         self.com_vel_angular = com_vel_angular
#         self.com_spatial_velocity = np.concatenate((com_vel_angular, com_vel_linear))
#
#         self.com_acc_linear = com_acc_linear
#         self.com_acc_angular = com_acc_angular
#         self.com_spatial_acceleration = np.concatenate((com_acc_angular, com_acc_linear))
#
#         self.lsole_pos = np.array(lsole_pos)
#         self.lsole_quat = np.array(lsole_quat)
#         self.lsole_pose = np.concatenate((lsole_pos, lsole_quat))
#
#         self.lsole_vel_linear = lsole_vel_linear
#         self.lsole_vel_angular = lsole_vel_angular
#         self.lsole_spatial_velocity = np.concatenate((lsole_vel_angular, lsole_vel_linear))
#
#         self.lsole_acc_linear = lsole_acc_linear
#         self.lsole_acc_angular = lsole_acc_angular
#         self.lsole_spatial_acceleration = np.concatenate((lsole_acc_angular,lsole_acc_linear))
#
#         self.rsole_pos = np.array(rsole_pos)
#         self.rsole_quat = np.array(rsole_quat)
#         self.rsole_pose = np.concatenate((rsole_pos, rsole_quat))
#
#         self.rsole_vel_linear = rsole_vel_linear
#         self.rsole_vel_angular = rsole_vel_angular
#         self.rsole_spatial_velocity = np.concatenate((rsole_vel_angular, rsole_vel_linear))
#
#         self.rsole_acc_linear = rsole_acc_linear
#         self.rsole_acc_angular = rsole_acc_angular
#         self.rsole_spatial_acceleration = np.concatenate((rsole_acc_angular, rsole_acc_linear))
#
#         self.angular_momentum = angular_momentum
#
#         self.contact_state = contact_state
#
#         self.create_alias()
#
#
#
#
#     def create_alias(self):
#         self.base_quaternion = self.com_quat
#         self.base_quat = self.com_quat
#         self.com_vel = self.com_vel_linear
#         self.com_acc = self.com_acc_linear
#
#
#     def show(self):
#         print("-" * 30)
#         print("[HighLevelCommand]")
#         print("contact_state: ", self.contact_state)
#         print("com_pos:", self.com_pos)
#         print("lsole_pos:", self.lsole_pos)
#         print("rsole_pos:", self.rsole_pos)
#         print("-" * 30)
