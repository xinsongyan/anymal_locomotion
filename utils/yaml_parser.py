import yaml

def load_yaml(yaml_path):
    import yaml
    with open(yaml_path) as f:
        params = yaml.load(f)
    return params