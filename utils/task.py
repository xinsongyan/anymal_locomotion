#  Copyright (c) 2018.  Songyan XIN,  Italian Institute of Technology.

class Task:
    def __init__(self, matrix, vector, priority=1, weight=1):
        self.matrix = matrix  # matrix
        self.vector = vector  # vector
        self.priority = priority  # int
        self.weight = weight  # float

    def set_matrix(self, matrix):
        self.matrix = matrix

    def set_vector(self, vector):
        self.vector = vector
        
    def set_priority(self, priority):
        self.priority = priority

    def set_weight(self, weight):
        self.weight = weight


    def show(self):
        print("Task:")
        print("matrix", self.matrix.shape, ":", "\n", self.matrix)
        print("vector", self.vector.shape, ":", "\n", self.vector)
        print("priority", self.priority)
        print("weight:", self.weight)
