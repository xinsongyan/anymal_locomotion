/*!
 * @file     AnymalDescription.hpp
 * @author   Dario Bellicoso
 * @date     Nov 6, 2017
 */

#pragma once

// anymal description
#include "anymal_description/AnymalDefinitions.hpp"
#include "anymal_description/AnymalTopology.hpp"
#include "anymal_description/robot_description_additions.hpp"

// romo
#include <romo/common/RobotDescription.hpp>

namespace anymal_description {

using ConcreteAnymalDescription = romo::ConcreteDescription<AnymalDefinitions, AnymalTopology>;
using AnymalDescription = romo::RobotDescription<ConcreteAnymalDescription>;

}  // namespace anymal_description
